# TODO

## Général
- Fonctionnement à multi-processus séparé (Core/ GUI / Surface).
- Rapport de soutenance 2 — Markdown & Pandoc.
- Refaire des fichiers de test (guitare).
- Documentation à rédiger (version site internet & version LaTeX).

### Site internet
- Contenu du site web à compléter.
- Vidéo de démonstration des capacités du logiciel.

## GUI
- Interface en anglais uniquement (ou module de langue français avec option du choix de la langue).
- User presets — sauvegarde des préréglages dans un fichier dédié à chaque *preset*. Possibilité pour l’utilisateur d’en créer des personnalisés.
- Menu pour ouvrir un fichier au format audio compatible avec *libsndfile* (WAV, FLAC, AIFF, RAW, etc ...)
- Bouton *processing* permettant l’activation du traitement (thread processing) en temps réel. Et dans le cas d’une lecture de fichier l’arrêt de la lecture du fichier (pause) et un bouton permettant de recommencer la lecture depuis le début (utiliser des pictogrammes).
- Jolie interface graphique — facilement utilisable et rapidement. Travailler sur le *design* et l’ergonomie.
- Reproduire l’effet des LED de la surface de contrôle dans la GUI.
- Représentation graphique du son en temps réel — visualisation *Scope* dans VLC.
- Relier l’interface graphique au reste de l’application (via des évenements). Uniquement lorsque le fonctionnement multi-processus sera prêt.

## Surface
- Schéma électronique de la table de contrôle (Arduino). Pour permettre à d’autres utilisateurs d’Arduino d’utiliser *StarDSP*. :-p
- Ajout d’un écran LCD permettant d’afficher le preset en cours d’utilisation et d’afficher les valeurs des potentiomètre (en %) lorsque l’utilisateur les modifies.
- Ajouter des potentionmètres, LED, et boutons.

## Traitement
- Effet de delay.
- Effet de réverberation.
- Effet chorus.
- Effet de modulation.
- Effet de distorsion.
- Balance stéréo + effets stéréo.
- Compresseur (utile ?)
- Possibilité de mixer deux sons permettant au musicien d’avoir un rythme de batterie ou de métronome.

# Pièces à fournir

- Un plan de soutenance
- Un rapport de soutenance (20p. min)
- Un site web

## Soutenance finale

- Un rapport de projet
- Un dossier d’exploitation
- Le projet
- Une procédure d’installation/désinstallation

# Site web

- Présentation du projet (historique, membres, chronologie de la réalisation, les problèmes rencontrés, les solutions envisagées)
- Les liens sur les sites (membres, logiciels, images, sons, librairies, applets etc ...)
- Un téléchargement du rapport, projet et version lite.

# Améliorations (soutenance 1)
- Introduction & Conclusion à travailler (important).
- Manque de précision.
- Concision.
- Aller droit au but et expliquer (pas suffisemment de détails et attention aux phrases à rallonge).
- Ne pas se couper la parole.
- Être en avance sur le planning.
- Site internet trop banal, faire une vitrine du projet (être vendeur).
- Interface graphique trop banale.
- Assurer la compréhension des algos.
- Faire des soutenances blanches.
- Rajouter des détails dans le rapport (du contenu, images, schéma, etc ...)
