README
======

To ensure the compatibility between _Mac OS X_ and _OpenSUSE_ you have to configure two files.
The website is running on _Mac OS X_ using _MAMP_.

# cgi-bin/template.ml

## Mac OS X

`let template_dir = “../htdocs/templates/“`

## OpenSUSE

`let template_dir = “../templates/“`

# templates/base.html

You have to change the link of the __CSS__ file.

## Mac OS X

`<link rel="stylesheet" href="../design/style.css" />`

## OpenSUSE

`<link rel="stylesheet" href="design/style.css" />`

# Setup (Apache/Linux)

    # cd /var/www
    # ln -s /path/to/stardsp/website/ ./stardsp
    # cd ./stardsp/
    # make

The website will be available from this url `http://127.0.0.1/stardsp/`

# Usage

The template engine, programmed with OCaml, is designed to process web templates (situated in `/templates/`) and dynamic content (situated in `/cgi-bin`) to produce output web documents.

The template language uses the following syntax :

  - `[begin *part*]*html code*[end]` : defines a part of html code
  - `{ *part* }` : includes the content of the part *part* previously defined
  - `{% *function* %}` : includes the result of the OCaml function *function*. The function must be declared in the file `/cgi-bin/cgi_functions.ml`.
  - `{@ *template* @}` : Tells the template engine that the current template "extends" another template.

Once template processing is complete, the content of the part `base` is printed by the CGI.

## URL dispatcher

The URL is divided as follows :

    http://domain?arg0/arg1/arg2/...

arg0 represents the name of the page asked by the user.
You can link arg0 to a template in the file `/cgi-bin/url.ml`.
The others arguments can be gotten with the function `Env.get_url_arg n`.

## Example

### templates/base.html

    :::html
    [begin base]
    <!DOCTYPE html>
    <html>
      <head>
        <title>OCaml-WebFramework -{ title }</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      </head>
      <body>
        <nav>
          <ul>
            <li><a href="?index/">Home</a></li>
            <li><a href="?about/">About</a></li>
          </ul>
        </nav>
        { body }
      </body>
    </html>
    [end]


### templates/index.html

    :::html
    {@ base.html @}
    
    [begin title] Index [end]
    
    [begin body]
    <h1>Hello World ! </h1>
    <p>{% get_time %}</p>
    [end]

### templates/about.html

    :::html
    {@ base.html @}
    
    [begin title] About [end]
    
    [begin body]
    <h1>About</h1>
    [end]

### cgi-bin/url.ml

    :::ocaml
    ...
    let get_page_from_url () =
      match Env.get_url_arg 0 with
        | "about" -> "about.html"
        | _ -> "index.html"
    ...

### cgi-bin/cgi_functions.ml

    :::ocaml
    ...
    let get_function = function
      | "get_time" -> Date.get_date ()
      | f -> "Cannot find function : " ^ f
    ...
