(*
** url.ml for OCaml-WebFramework
**
** Copyright (C) 2012 Pierre Surply
** <pierre.surply@gmail.com>
**
** This file is part of OCaml-WebFramework.
**
**    OCaml-WebFramework is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    OCaml-WebFramework is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with OCaml-WebFramework.  If not, see <http://www.gnu.org/licenses/>.
**
** Started on  Sun Sep 16 11:31:15 2012 Pierre Surply
** Last update Mon Apr  8 21:53:40 2013 Pierre Pagnoux
*)

let get_page_from_url () =
  match Env.get_url_arg 0 with
    | "index"		-> "index.html"
    | "overview"	-> "overview.html"
    | "doc"		-> "doc.html"
    | "about"		-> "about.html"
    | "download"	-> "download.html"
    | _			-> "index.html"

let make_page_from_url () =
  Template.make_page (get_page_from_url ())
