(*
 ** com.ml for StarDSP
 **
 ** Made by Pierre Surply
 ** <pierre.surply@gmail.com>
 **
 ** Started on  Fri Apr  5 22:06:49 2013 Pierre Surply
** Last update Sun May 26 17:52:02 2013 Pierre Pagnoux
 *)

exception Unknown_order of char

type order =
  | Add		of char
  | Rm		of (char * int)
  | Amp_drive   of int
  | Amp_noise   of int
  | Eq          of (int * int * int)
  | Echo_delay  of int
  | Echo_decay  of int
  | Rev         of int
  | Balance     of int
  | Stereo      of int
  | Process
  | File        of string
  | Realtime
  | Quit

let order_to_char = function
  | Add _		-> 'a'
  | Rm _		-> 'b'
  | Amp_drive _         -> 's'
  | Amp_noise _         -> 't'
  | Eq _                -> 'u'
  | Echo_delay _        -> 'v'
  | Echo_decay _        -> 'w'
  | Rev _               -> 'x'
  | Balance _           -> 'y'
  | Stereo _            -> 'z'
  | Process             -> 'p'
  | File _              -> 'o'
  | Realtime            -> 'r'
  | Quit                -> 'q'

let char_to_order fd c =
  let arg1 () =
    let buff = String.create 2 in
    if Unix.read fd buff 0 2 > 0 then
      (int_of_char buff.[0],
       int_of_char buff.[1])
    else
      (0, 0)
  in
  let arg3 () =
    let buff = String.create 4 in
    if Unix.read fd buff 0 4 > 0 then
      (int_of_char buff.[0],
       int_of_char buff.[1],
       int_of_char buff.[2],
       int_of_char buff.[3])
    else
      (0, 0, 0, 0)
  in
  let arg_str () =
    let buff = String.create 256 in
    if Unix.read fd buff 0 1 > 0 then
      let length = int_of_char buff.[0] in
      if Unix.read fd buff 0 length = length then
        (length, String.sub buff 0 length)
      else
        failwith "Path is incomplete."
    else
      (0, "")
  in
  match c with
    | 'a' ->
      let buff = String.create 2 in
      if Unix.read fd buff 0 2 > 0 then
	(0, Add (buff.[1]))
      else
	failwith "Add effect failed."
    | 'b' ->
      let buff = String.create 3 in
      if Unix.read fd buff 0 3 > 0 then
	(int_of_char buff.[0], Rm (buff.[1], int_of_char buff.[2]))
      else
	failwith "Rm effect failed."
    | 's' ->
      let (pos, arg) = arg1 () in
      (pos, Amp_drive arg)
    | 't' ->
      let (pos, arg) = arg1 () in
      (pos, Amp_noise arg)
    | 'u' ->
      let (pos, lg, mg, hg) = arg3 () in
      (pos, Eq (lg, mg, hg))
    | 'v' ->
      let (pos, arg) = arg1 () in
      (pos, Echo_delay arg)
    | 'w' ->
      let (pos, arg) = arg1 () in
      (pos, Echo_decay arg)
    | 'x' ->
      let (pos, arg) = arg1 () in
      (pos, Rev arg)
    | 'y' ->
      let (pos, arg) = arg1 () in
      (pos, Balance arg)
    | 'z' ->
      let (pos, arg) = arg1 () in
      (pos, Stereo arg)
    | 'p' -> (0, Process)
    | 'o' ->
      let (len, str) = arg_str () in
      (len, File str)
    | 'r' -> (0, Realtime)
    | 'q' -> (0, Quit)
    | c   -> raise (Unknown_order c)

let send_order ?(pos = 0) fd order =
  let buff = String.create 5 in
  buff.[0] <- order_to_char order;
  buff.[1] <- char_of_int pos;
  let _ = match order with
    | Add c ->
      buff.[2] <- c;
      Unix.write fd buff 0 3
    | Rm (c,i) ->
      buff.[2] <- c;
      buff.[3] <- char_of_int i;
      Unix.write fd buff 0 4
    | Amp_drive i
    | Amp_noise i
    | Echo_delay i
    | Echo_decay i
    | Rev i
    | Balance i
    | Stereo i ->
      buff.[2] <- char_of_int i;
      Unix.write fd buff 0 3
    | Eq (lg, mg, hg) ->
      buff.[2] <- char_of_int lg;
      buff.[3] <- char_of_int mg;
      buff.[4] <- char_of_int hg;
      Unix.write fd buff 0 5
    | File str ->
      Unix.write fd buff 0 2;
      Unix.write fd str 0 pos
    | _ -> Unix.write fd buff 0 1
  in ()
