(*
 ** com.mli for StarDSP
 **
 ** Made by Pierre Surply
 ** <pierre.surply@gmail.com>
 **
 ** Started on  Tue Apr 16 19:03:18 2013 Pierre Surply
** Last update Sun May 26 17:51:45 2013 Pierre Pagnoux
 *)

exception Unknown_order of char

type order =
  | Add		of char
  | Rm		of (char * int)
  | Amp_drive   of int
  | Amp_noise   of int
  | Eq          of (int * int * int)
  | Echo_delay  of int
  | Echo_decay  of int
  | Rev         of int
  | Balance     of int
  | Stereo      of int
  | Process
  | File        of string
  | Realtime
  | Quit

val order_to_char       : order -> char
val char_to_order       : Unix.file_descr -> char -> (int * order)

val send_order          : ?pos:int -> Unix.file_descr -> order -> unit
