(*
** term.ml for StarDSP
** 
** Made by Pierre Surply
**   pierre.surply@gmail.com
** 
** Started on  Sun Jan 27 14:43:50 2013 Pierre Surply
**
*)

type t

external create 	: string -> t = "ocaml_term_create"
external delete 	: t -> unit = "ocaml_term_delete"
external send_int 	: t -> int -> int = "ocaml_term_sendi"
external send_char 	: t -> char -> unit = "ocaml_term_sendc"
external recv_int       : t -> int = "ocaml_term_recvi"
external recv_blk       : t -> int = "ocaml_term_recv_blk"
