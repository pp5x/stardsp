(*
** surface.ml for StarDSP
** 
** Made by Pierre Surply
**   pierre.surply@gmail.com
** 
** Started on  Sun Jan 27 14:43:50 2013
**
*)

type t =
{
  dev                   : string;
  com			: Term.t option;
  mutable lvl_led	: int;
}

let create dev =
  {
    dev = dev;
    com =
      if dev <> "" then
        Some (Term.create dev)
      else
        None;
    lvl_led = 0;
  }

let update =
  let i = ref 0 in
  fun surface lvl processing ->
    match surface.com with
    | Some com ->
      incr i;
      if !i > 10 then
        begin
          i := 0;
          let alvl = abs (int_of_float (lvl *. 10000.)) in
	  let llvl = int_of_float (log (float (alvl + 1))) in
	  let lvl = (surface.lvl_led + llvl) / 2 in
          surface.lvl_led <- lvl;
          let packet =
            ((if processing then 0 else 1) lsl 4) lor
              (lvl land 0xF)
          in
            begin
              let ret = Term.send_int com packet in
		(*Printf.printf "%X  \r%!" ret;*)
              let opcode = ret land 0b11 in
              let arg = ret lsr 2 in
              match opcode with
              | 0b01 -> Some (Com.Process)
              | 0b10 ->
                begin
                  let value = (Term.recv_blk com * 100) / 64 in
                  match arg with
                  | 0 ->
                    Some (Com.Amp_drive value)
                  | 1 ->
                    Some (Com.Stereo value)
                  | 2 ->
                    Some (Com.Echo_delay value)
                  | 3 ->
                    Some (Com.Echo_decay value)
                  | 4 ->
                    Some (Com.Rev value)
                  | _ -> None
                end
              | _ -> None
	    end
        end
      else None
    | None -> None

let minisleep t =
  ignore (Unix.select [] [] [] t)

let connect surface s =
  let regex = Str.regexp "\\(.*/\\)?\\([^/]*\\)\\..+" in
  let title =
    if Str.string_match regex s 0 then
      Str.matched_group 2 s
    else
      if s = "" then "\x03" else s
  in
  match surface.com with
  | Some com ->
    Printf.printf "Connecting the surface..%!";
    let _ = Term.send_int com 42 in
    minisleep 0.1;
    if Term.recv_int com = 42 then
      Printf.printf "\t\x1B[32mOK\x1B[0m\n%!"
    else
      begin
        minisleep 2.;
        let _ = Term.send_int com 42 in
        minisleep 0.1;
        if Term.recv_int com = 42 then
          Printf.printf ".\t\x1B[32mOK\x1B[0m\n%!"
        else
          begin
            Printf.printf "\t\x1B[31mFailure\x1B[0m\n%!";
            exit 2;
          end
      end;
    let len = String.length title in
    for i = 0 to if len > 16 then 15 else len - 1 do
      Term.send_char com title.[i];
      minisleep 0.05
    done;
    if len < 16 then
      let _ = Term.send_int com 0 in ();
  | None -> ()

let reset surface =
  surface.lvl_led <- 0;
  match surface.com with
  | Some com ->
    let _ = Term.send_int com 0 in ()
  | None -> ()
