/*
** Makefile for StarDSP
** 
** Made by Pierre Surply
**   pierre.surply@gmail.com
** 
** Started on  Wed Oct 24 18:38:55 2012
*/

#ifndef _TERM_H_
#define _TERM_H_

#define BAUDRATE	B9600

typedef struct s_term
{
  int			fd;
  struct termios	*cur, *org;
} *term;

term term_create(const char *dev);
void term_delete(term t);
void term_sendc(term t, const char c);
void term_sendi(term t, const unsigned char i);

#endif /* _TERM_H_ */
