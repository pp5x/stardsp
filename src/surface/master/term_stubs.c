/*
** term_stubs.c for StarDSP
** 
** Made by Pierre Surply
**   pierre.surply@gmail.com
** 
** Started on  Sun Jan 27 14:43:50 2013
**
*/

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <stdio.h>

#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>

#include "term.h"

CAMLprim
value ocaml_term_create(value dev)
{
  CAMLparam1(dev);

  term	t;
  value	ans;

  t = malloc(sizeof (struct s_term));
  t->cur = malloc(sizeof (struct termios));
  t->org = malloc(sizeof (struct termios));

  t->fd = open(String_val(dev), O_RDWR | O_NOCTTY | O_NDELAY);

  tcgetattr(t->fd, t->org);

  memset(t->cur, 0, sizeof(struct termios)); 

  t->cur->c_cflag = CLOCAL | CS8 | CREAD;
  t->cur->c_iflag = 0;
  t->cur->c_oflag = 0;
  t->cur->c_lflag = 0;
  t->cur->c_cc[VTIME] = 0;
  t->cur->c_cc[VMIN] = 1;

  cfsetospeed(t->cur, BAUDRATE);
  cfsetispeed(t->cur, BAUDRATE); 
  
  tcflush(t->fd, TCIOFLUSH);
  
  tcsetattr(t->fd, TCSANOW, t->cur);

  ans = caml_alloc_tuple(1);
  Store_field(ans, 0, (value) t);

  CAMLreturn((value) ans);
}

CAMLprim
value ocaml_term_delete(value _t)
{
  term	t;
  t = (term) Field(_t, 0);
  tcsetattr(t->fd, TCSANOW, t->org);
  close(t->fd);
  free(t->cur);
  free(t->org);
  free(t);
  Store_field(_t, 0, (value) NULL);

  return Val_unit;
}

CAMLprim
value ocaml_term_sendi(value _t, value _i)
{
  term		t = (term) Field(_t, 0);
  unsigned char i = (unsigned char) Int_val(_i);

  write(t->fd, &i, 1);
  tcdrain(t->fd);
  i = 0xFF;
  read(t->fd, &i, 1);
  return Val_int((int) i);
}

CAMLprim
value ocaml_term_sendc(value _t, value _c)
{
  term	t = (term) Field(_t, 0);
  char	c = (unsigned char) Int_val(_c);
  write(t->fd, &c, 1);

  return Val_unit;
}

CAMLprim
value ocaml_term_recvi(value _t, value _i)
{
  term		t = (term) Field(_t, 0);
  unsigned char i = (unsigned char) Int_val(_i);

  i = 0;
  read(t->fd, &i, 1);

  return Val_int((int) i);
}

CAMLprim
value ocaml_term_recv_blk(value _t, value _i)
{
  term		t = (term) Field(_t, 0);
  unsigned char i = (unsigned char) Int_val(_i);

  i = 0;
  while(read(t->fd, &i, 1) < 1);

  return Val_int((int) i);
}
