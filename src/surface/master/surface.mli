(*
** surface.mli for StarDSP
** 
** Made by Pierre Surply
**   pierre.surply@gmail.com
** 
** Started on  Sun Jan 27 14:43:50 2013
**
*)

type t

val create	: string -> t
val connect     : t -> string -> unit
val update	: t -> float -> bool -> Com.order option
val reset	: t -> unit
