(*
** term.mli for StarDSP
** 
** Made by Pierre Surply
**   pierre.surply@gmail.com
** 
** Started on  Sun Jan 27 14:43:50 2013
**
*)

type t

val create	: string -> t
val delete	: t -> unit
val send_int	: t -> int -> int
val send_char	: t -> char -> unit
val recv_int    : t -> int
val recv_blk    : t -> int
