(*
** manager.ml for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Sun Apr 28 17:35:58 2013 Pierre Pagnoux
** Last update Sun May 26 12:35:04 2013 Pierre Pagnoux
*)

let tocore = ref Unix.stdout

let window =
  let w = GWindow.window
    ~title: "StarDSP - Manager"
    ~height: 500
    ~width: 500 ()
  in
  let _ = w#connect#destroy
    ~callback: GMain.quit
  in w

let table = GPack.table
  ~rows: 3
  ~columns: 4
  ~packing: window#add ()

(* MENU *)
let menu =
  let pos = table#attach
    ~left: 0
    ~top: 0
    ~right: table#columns
    ~expand: `X
  in
  let menu_bar = GMenu.menu_bar
    ~packing: pos ()
  in
  Menu.init tocore menu_bar;
  menu_bar

(* EFFECTS BUTTONS *)
let effects_frame =
  let pos = table#attach
    ~left: 0
    ~top: 1
    ~right: 1
    ~expand: `BOTH
  in
  GBin.frame
    ~label: "Add an effect"
    ~border_width: 10
    ~packing: pos ()

let effects_vbox =
  let pos = effects_frame#add in
  GPack.vbox
    ~homogeneous: true
    ~spacing: 20
    ~border_width: 20
    ~packing: pos ()

let effect_button_add label =
  let pos = effects_vbox#add in
  GButton.button
    ~label: label
    ~packing: pos ()

let effect_amp_button = effect_button_add "Amplifier"
let effect_eq_button = effect_button_add "Equalizer"
let effect_echo_button = effect_button_add "Echo"
let effect_reverb_button = effect_button_add "Reverberation"
let effect_balance_button = effect_button_add "Balance"
let effect_stereo_button = effect_button_add "Stereo effect"

(* MISC. ARROW *)
let arrow =
  let pos = table#attach
    ~left: 1
    ~top: 1
    ~right: 2
  in
  GMisc.arrow
    ~kind: `RIGHT
    ~packing: pos ()

(* EFFECTS LIST *)
let list_frame =
  let pos = table#attach
    ~left: 2
    ~top: 1
    ~right: 3
    ~expand: `BOTH
  in
  GBin.frame
    ~label: "Effects list "
    ~border_width: 10
    ~packing: pos ()

let effects_list =
  let pos = list_frame#add in
  GList.liste
    ~selection_mode: `SINGLE
    ~border_width: 10
    ~packing: pos ()

(* EFFECTS LIST CONTROLS *)
let list_controls_table =
  let pos = table#attach
    ~left: 3
    ~top: 1
  in
  GPack.table
    ~columns: 1
    ~rows: 10
    ~homogeneous: true
    ~row_spacings: 10
    ~border_width: 5
    ~width: 100
    ~height: 200
    ~packing: pos ()

let list_controls_remove_button =
  let pos = list_controls_table#attach
    ~left: 0
    ~top: 3
  in
  GButton.button
    ~label: "Remove"
    ~packing: pos ()

let list_controls_clear_all_button =
  let pos = list_controls_table#attach
    ~left: 0
    ~top: 4
  in
  GButton.button
    ~label: "Clear all"
    ~packing: pos ()


(* CONTROLS *)
let mode_label =
  let pos = table#attach
    ~left: 0
    ~top: 2
  in
  GMisc.label
    ~text: "Current audio mode : PulseAudio"
    ~height: 30
    ~packing: pos ()

let controls_hbox =
  let pos = table#attach
    ~left: 2
    ~top: 2
  in
  GPack.hbox
    ~homogeneous: true
    ~border_width: 10
    ~packing: pos ()

let control_add_button label =
  let pos = controls_hbox#add in
  GButton.button
    ~label: label
    ~packing: pos ()

let control_stop_button = control_add_button "Start/Stop"
let control_pa_button = control_add_button "Realtime"



(* INIT MANAGER *)
let init fd =
  tocore := fd;
  window#show ()
