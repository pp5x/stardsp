(*
** gui.ml for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Sun Apr 28 17:43:49 2013 Pierre Pagnoux
** Last update Sat May 25 12:30:01 2013 Pierre Pagnoux
*)

let init fd =
  Manager.init fd;
  GMain.main ()

let main tocore =
  init tocore;
  Com.send_order tocore Com.Quit;
  exit 0

