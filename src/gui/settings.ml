(*
 ** settings.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Mon Apr 29 20:06:25 2013 Pierre Pagnoux
** Last update Sun May 26 15:41:29 2013 Pierre Pagnoux
 *)

(* Base Amplifier Window*)

let create_amplifier_window name count nb_amp =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in

  (* Drive cursor *)
  let drive_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits: 0
    ~packing: (table#attach ~left:0 ~top:1 ~bottom:7) ()
  in
  let _ = drive_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(drive_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Drive"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in

  (* Noise cursor *)
  let noise_cursor = GRange.scale `VERTICAL
    ~adjustment:( GData.adjustment
                    ~value: 0.
                    ~lower: 0.
                    ~upper: 100.
                    ~page_incr: 0.1
                    ~step_incr: 1.
                    ~page_size: 0.0 ())
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:3 ~top:1 ~bottom:7) ()
  in
  let _ = noise_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(noise_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Noise"
    ~packing:(table#attach ~left:3 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
	~pos: count
	!Manager.tocore
	(Com.Rm ('s', nb_amp)))
  in
  window

(* Base Equalizer Window *)

let create_equalizer_window name count nb_eq =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Low cursor *)
  let l_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 0.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:0 ~top:1 ~bottom:7) ()
  in
  let _ = l_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(l_cursor#adjustment#value)));
      () )
  in
  let _= GMisc.label
    ~text:"L"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in
  (* Medium cursor *)
  let m_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 0.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:2 ~top:1 ~bottom:7) ()
  in
  let _ = m_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(m_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"M"
    ~packing:(table#attach ~left:2 ~top:0) ()
  in
  (* High cursor *)
  let h_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 0.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:4 ~top:1 ~bottom:7) ()
  in
  let _ = h_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(h_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"H"
    ~packing:(table#attach ~left:4 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
	~pos: count
	!Manager.tocore
	(Com.Rm ('u', nb_eq)))
  in
  window

(* Basic Echo Window *)

let create_echo_window name count nb_echo =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Delay cursor *)
  let delay_cursor = GRange.scale `VERTICAL
    ~adjustment: (  GData.adjustment
                      ~value: 0.
                      ~lower: 0.
                      ~upper: 100.
                      ~page_incr: 1.
                      ~step_incr: 1.
                      ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:0 ~top:1 ~bottom:7) ()
  in
  let _ = delay_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(delay_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Delay"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in
  (* Decay cursor *)
  let decay_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:3 ~top:1 ~bottom:7) ()
  in
  let _ = decay_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(decay_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Decay"
    ~packing:(table#attach ~left:3 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
	~pos: count
	!Manager.tocore
	(Com.Rm ('v', nb_echo)))
  in
  window


(* Basic Reverbe Window *)

let create_reverbe_window name count nb_rev =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Reverb cursor *)
  let rev_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:1 ~top:1 ~bottom:7) ()
  in
  let _ = rev_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(rev_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Seconds"
    ~packing:(table#attach ~left:1 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
	~pos: count
	!Manager.tocore
	(Com.Rm ('x', nb_rev)))
  in
  window

(* Basic Balance Window*)

let create_balance_window name count nb_bal =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 3
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:10
    ~packing:window#add ()
  in
  (* Balance cursor *)
  let bal_cursor = GRange.scale `HORIZONTAL
    ~adjustment: ( GData.adjustment
                     ~value: 50.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:false
    ~digits:0
    ~packing: (table#attach ~left:0 ~top:1 ~right:3) ()
  in
  let _ = bal_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(bal_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Balance"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
	~pos: count
	!Manager.tocore
	(Com.Rm ('y', nb_bal)))
  in
  window

(* Basic Stereo Window *)

let create_stereo_window name count nb_stereo =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Stereo cursor *)
  let stereo_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:1 ~top:1 ~bottom:7) ()
  in
  let _ = stereo_cursor#connect#value_changed
    ~callback:(fun () ->
      print_endline (string_of_int(int_of_float(stereo_cursor#adjustment#value)));
      () )
  in
  let _ = GMisc.label
    ~text:"Stereo"
    ~packing:(table#attach ~left:1 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
	~pos: count
	!Manager.tocore
	(Com.Rm ('z', nb_stereo)))
  in
  window

(* Show *)
(*
  let show =
  let current = ref None in
  fun () ->
  match !current with
  | None ->
  let settings = create_amplifier_window "Amplifier"
  in
  let _ = settings#connect#destroy ~callback:
  (fun _ ->
  settings#destroy ();
  current := None)
  in
  current := Some settings;
  settings#present ()
  | Some settings ->
  settings#present ()

*)
