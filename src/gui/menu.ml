(*
 ** menu.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sun Apr 28 19:08:10 2013 Pierre Pagnoux
** Last update Sun May 26 13:47:34 2013 Pierre Pagnoux
 *)

let _ = GMain.Main.init ()

let about =
  let dialog () = GWindow.about_dialog
    ~name:"StarDSP"
    ~authors:["Pierre Pagnoux";"Alexandra Pizzini";"Pierre Surply";
              "Vincent Molinié"]
    ~copyright:"All rights reserved - Turn It Loud"
    ~version:"v1.5 Glass"
    ~website:"http://star-dsp.epimeros.org"
    ~border_width: 20
    ~position: `CENTER ()
  in
  let current = ref None in
  fun () ->
    match !current with
      | None ->
        let d = dialog () in
        let _ = d#connect#response ~callback:
          (fun _ ->
            d#destroy ();
            current := None)
        in
        current := Some d;
        d#present ()
      | Some d ->
        d#present ()

let open_file fd () =
  let path = GToolbox.select_file
    ~title: "Open an audio file ..." () in
  match path with
    | None -> ()
    | Some str ->
      let length = String.length str in
      if length > 255 then
        failwith "The path is too long."
      else
        Com.send_order
          ~pos: length
          !fd
          (Com.File str)

let file_entries fd = [
  `I ("Open", open_file fd);
  `S;
  `I ("Quit", GMain.Main.quit)]

let about_entries = [
  `I ("About us", about)]

let add_entry menu_bar label l =
  let item = GMenu.menu_item
    ~label
    ~packing: menu_bar#add ()
  in
  let submenu = GMenu.menu
    ~packing: item#set_submenu ()
  in
  GToolbox.build_menu submenu ~entries: l

let init fd  menu =
  add_entry menu "File" (file_entries fd);
  add_entry menu "About" about_entries
