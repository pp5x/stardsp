(*
 ** action.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Tue Apr 30 15:23:47 2013 Pierre Pagnoux
** Last update Sun May 26 17:40:41 2013 Pierre Pagnoux
 *)

type t =
    {
      mutable count     : int;
      mutable nb_amp    : int;
      mutable nb_eq     : int;
      mutable nb_echo   : int;
      mutable nb_rev    : int;
      mutable nb_bal    : int;
      mutable nb_stereo : int;
      mutable current   : GList.list_item option;
      mutable windows   : GWindow.window list;
    }

type s =
  | Amplifier
  | Equalizer
  | Echo
  | Reverb
  | Balance
  | Stereo

let reset t =
  let rec del_windows = function
    | [] -> ()
    | h::l ->
      h#destroy ();
      del_windows l
  in
  del_windows t.windows;
  t.windows <- [];
  t.count       <- 0;
  t.nb_amp      <- 0;
  t.nb_eq       <- 0;
  t.nb_echo     <- 0;
  t.nb_rev      <- 0;
  t.nb_bal      <- 0;
  t.nb_stereo   <- 0;
  t.current     <- None

(*******)
let create_amplifier_window name count nb_amp c =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in

  (* Drive cursor *)
  let drive_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits: 0
    ~packing: (table#attach ~left:0 ~top:1 ~bottom:7) ()
  in
  let _ = drive_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_amp - 1)
        !Manager.tocore
        (Com.Amp_drive (int_of_float(drive_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Drive"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in

  (* Noise cursor *)
  let noise_cursor = GRange.scale `VERTICAL
    ~adjustment:( GData.adjustment
                    ~value: 0.
                    ~lower: 0.
                    ~upper: 100.
                    ~page_incr: 0.1
                    ~step_incr: 1.
                    ~page_size: 0.0 ())
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:3 ~top:1 ~bottom:7) ()
  in
  let _ = noise_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_amp - 1)
        !Manager.tocore
        (Com.Amp_noise (int_of_float(noise_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Noise"
    ~packing:(table#attach ~left:3 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
        ~pos: count
        !Manager.tocore
        (Com.Rm ('s', nb_amp));
      c.nb_amp <- c.nb_amp - 1)
  in
  window

(* Base Equalizer Window *)

let create_equalizer_window name count nb_eq c =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let hg = ref 0 in
  let mg = ref 0 in
  let lg = ref 0 in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Low cursor *)
  let l_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 0.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:0 ~top:1 ~bottom:7) ()
  in
  let _ = l_cursor#connect#value_changed
    ~callback:(fun () ->
      lg := int_of_float(l_cursor#adjustment#value);
      Com.send_order
        ~pos: (nb_eq - 1)
        !Manager.tocore
        (Com.Eq (!lg, !mg, !hg)))
  in
  let _ = GMisc.label
    ~text:"L"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in
  (* Medium cursor *)
  let m_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 0.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:2 ~top:1 ~bottom:7) ()
  in
  let _ = m_cursor#connect#value_changed
    ~callback:(fun () ->
      mg := int_of_float(m_cursor#adjustment#value);
      Com.send_order
        ~pos: (nb_eq - 1)
        !Manager.tocore
        (Com.Eq (!lg, !mg, !hg)))
  in
  let _ = GMisc.label
    ~text:"M"
    ~packing:(table#attach ~left:2 ~top:0) ()
  in
  (* High cursor *)
  let h_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 0.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:4 ~top:1 ~bottom:7) ()
  in
  let _ = h_cursor#connect#value_changed
    ~callback:(fun () ->
      hg := int_of_float(h_cursor#adjustment#value);
      Com.send_order
        ~pos: (nb_eq - 1)
        !Manager.tocore
        (Com.Eq (!lg, !mg, !hg)))
  in
  let _ = GMisc.label
    ~text:"H"
    ~packing:(table#attach ~left:4 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
        ~pos: count
        !Manager.tocore
        (Com.Rm ('u', nb_eq));
      c.nb_eq <- c.nb_eq - 1)
  in
  window

(* Basic Echo Window *)

let create_echo_window name count nb_echo c =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Delay cursor *)
  let delay_cursor = GRange.scale `VERTICAL
    ~adjustment: (  GData.adjustment
                      ~value: 0.
                      ~lower: 0.
                      ~upper: 100.
                      ~page_incr: 1.
                      ~step_incr: 1.
                      ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:0 ~top:1 ~bottom:7) ()
  in
  let _ = delay_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_echo - 1)
        !Manager.tocore
        (Com.Echo_delay (int_of_float(delay_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Delay"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in
  (* Decay cursor *)
  let decay_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:3 ~top:1 ~bottom:7) ()
  in
  let _ = decay_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_echo - 1)
        !Manager.tocore
        (Com.Echo_decay (int_of_float(decay_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Decay"
    ~packing:(table#attach ~left:3 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
        ~pos: count
        !Manager.tocore
        (Com.Rm ('v', nb_echo));
      c.nb_echo <- c.nb_echo - 1)
  in
  window


(* Basic Reverbe Window *)

let create_reverbe_window name count nb_rev c =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Reverb cursor *)
  let rev_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:1 ~top:1 ~bottom:7) ()
  in
  let _ = rev_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_rev - 1)
        !Manager.tocore
        (Com.Rev (int_of_float(rev_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Seconds"
    ~packing:(table#attach ~left:1 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
        ~pos: count
        !Manager.tocore
        (Com.Rm ('x', nb_rev));
      c.nb_rev <- c.nb_rev - 1)
  in
  window

(* Basic Balance Window*)

let create_balance_window name count nb_bal c =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 3
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:10
    ~packing:window#add ()
  in
  (* Balance cursor *)
  let bal_cursor = GRange.scale `HORIZONTAL
    ~adjustment: ( GData.adjustment
                     ~value: 50.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:false
    ~digits:0
    ~packing: (table#attach ~left:0 ~top:1 ~right:3) ()
  in
  let _ = bal_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_bal - 1)
        !Manager.tocore
        (Com.Balance (int_of_float(bal_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Balance"
    ~packing:(table#attach ~left:0 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
        ~pos: count
        !Manager.tocore
        (Com.Rm ('y', nb_bal));
      c.nb_bal <- nb_bal - 1)
  in
  window

(* Basic Stereo Window *)

let create_stereo_window name count nb_stereo c =
  let window = GWindow.window
    ~title: name
    ~height: 200
    ~width: 200
    ~type_hint: `UTILITY
    ~show: true ()
  in
  let table = GPack.table
    ~rows: 10
    ~columns: 2
    ~width:200
    ~height:200
    ~homogeneous:true
    ~row_spacings:10
    ~border_width:15
    ~packing:window#add ()
  in
  (* Stereo cursor *)
  let stereo_cursor = GRange.scale `VERTICAL
    ~adjustment: ( GData.adjustment
                     ~value: 0.
                     ~lower: 0.
                     ~upper: 100.
                     ~page_incr: 1.
                     ~step_incr: 1.
                     ~page_size: 0.0 () )
    ~inverted:true
    ~digits:0
    ~packing: (table#attach ~left:1 ~top:1 ~bottom:7) ()
  in
  let _ = stereo_cursor#connect#value_changed
    ~callback:(fun () ->
      Com.send_order
        ~pos: (nb_stereo - 1)
        !Manager.tocore
        (Com.Stereo (int_of_float(stereo_cursor#adjustment#value))))
  in
  let _ = GMisc.label
    ~text:"Stereo"
    ~packing:(table#attach ~left:1 ~top:0) ()
  in
  let _ = window#connect#destroy
    ~callback:(fun () ->
      Com.send_order
        ~pos: count
        !Manager.tocore
        (Com.Rm ('z', nb_stereo));
      c.nb_stereo <- c.nb_stereo - 1)
  in
  window

(*******)

let init () =
  let c =
    {
      count     = 0;
      nb_amp    = 0;
      nb_eq     = 0;
      nb_echo   = 0;
      nb_rev    = 0;
      nb_bal    = 0;
      nb_stereo = 0;
      current   = None;
      windows   = [];
    }
  in
  let count_string c s =
    let conv str i =
      str ^ (string_of_int i)
    in
    c.count <- c.count + 1;
    match s with
      | Amplifier ->
        c.nb_amp <- c.nb_amp + 1;
        conv "Amplifier " c.nb_amp
      | Equalizer ->
        c.nb_eq <- c.nb_eq + 1;
        conv "Equalizer " c.nb_eq
      | Echo ->
        c.nb_echo <- c.nb_echo + 1;
        conv "Echo " c.nb_echo
      | Reverb ->
        c.nb_rev <- c.nb_rev + 1;
        conv "Reverb " c.nb_rev
      | Balance ->
        c.nb_bal <- c.nb_bal + 1;
        conv "Balance " c.nb_bal
      | Stereo ->
        c.nb_stereo <- c.nb_stereo + 1;
        conv "Stereo " c.nb_stereo
  in

  let insert_item s () =
    let str = count_string c s in
    let item = GList.list_item
      ~label: str ()
    in
    Manager.effects_list#insert item
      ~pos: c.count;
    str
  in

  (* AMP *)
  let _ = Manager.effect_amp_button#connect#clicked
    ~callback: (fun () ->
      Com.send_order !Manager.tocore (Com.Add 's');
      let str = insert_item Amplifier () in
      let window = create_amplifier_window str c.count c.nb_amp c in
      c.windows <- window :: c.windows;
      ())
  in

  (* EQ *)
  let _ = Manager.effect_eq_button#connect#clicked
    ~callback: (fun () ->
      Com.send_order !Manager.tocore (Com.Add 'u');
      let str = insert_item Equalizer () in
      let window = create_equalizer_window str c.count c.nb_eq c in
      c.windows <- window :: c.windows;
      ())
  in

  (* ECHO *)
  let _ = Manager.effect_echo_button#connect#clicked
    ~callback:  (fun () ->
      Com.send_order !Manager.tocore (Com.Add 'v');
      let str = insert_item Echo () in
      let window = create_echo_window str c.count c.nb_echo c in
      c.windows <- window :: c.windows;
      ())
  in

  (* REV *)
  let _ = Manager.effect_reverb_button#connect#clicked
    ~callback:  (fun () ->
      Com.send_order !Manager.tocore (Com.Add 'x');
      let str = insert_item Reverb () in
      let window = create_reverbe_window str c.count c.nb_rev c in
      c.windows <- window :: c.windows;
      ())
  in

  (* BALANCE *)
  let _ = Manager.effect_balance_button#connect#clicked
    ~callback: (fun () ->
      Com.send_order !Manager.tocore (Com.Add 'y');
      let str = insert_item Balance () in
      let window = create_balance_window str c.count c.nb_bal c in
      c.windows <- window :: c.windows;
      ())
  in

  (* STEREO *)
  let _ = Manager.effect_stereo_button#connect#clicked
    ~callback: (fun () ->
      Com.send_order !Manager.tocore (Com.Add 'z');
      let str = insert_item Stereo () in
      let window = create_stereo_window str c.count c.nb_stereo c in
      c.windows <- window :: c.windows;
      ())
  in

  (* CLEAR ALL *)
  let _ = Manager.list_controls_clear_all_button#connect#clicked
    ~callback: (fun () ->
      Manager.effects_list#clear_items 0 c.count;
      reset c)
  in

  (* REMOVE *)
  let _ = Manager.list_controls_remove_button#connect#clicked
    ~callback: (fun () ->
      match c.current with
        | None -> ()
        | Some i ->
          let pos = Manager.effects_list#child_position i in
          let rec list_rm l i =
            match l with
              | [] -> failwith "List is too short"
              | h::t when i = 1 ->
                h#destroy ();
                t
              | h::t -> h::list_rm t (i - 1)
          in
          c.windows <- list_rm c.windows (c.count - pos);
          c.count <- c.count - 1;
          let _ = Manager.effects_list#clear_items
            ~start: pos
            ~stop: (pos + 1)
          in
          c.current <- None)
  in

  (* LIST *)
  let _ = Manager.effects_list#connect#select_child
    ~callback: (fun item ->
      c.current <- Some item)
  in

  let _ = Manager.effects_list#connect#unselect_child
    ~callback: (fun item ->
      c.current <- None)
  in

  (* PROCESSING *)
  let _ = Manager.control_stop_button#connect#clicked
    ~callback: (fun () ->
      Com.send_order !Manager.tocore Com.Process)
  in

  let _ = Manager.control_pa_button#connect#clicked
    ~callback: (fun () ->
      Com.send_order !Manager.tocore Com.Realtime)
  in
  ()

let _ = init ()
