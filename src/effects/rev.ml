type buffer =
{
  mutable g      : float;
  mutable posw   : int;
  mutable dBuff  : float array;
}

type t =
{
  mutable del       : float;
  mutable posr      : int;
  mutable cobuff    : buffer list;
  mutable albuff    : buffer list;
}


let create_buffer g d =
  let buff =
    {
      g = g;
      posw = d;
      dBuff = Array.make 441000 0.
    }
  in buff

let get_g rvt d =
  let g = 0.001**(d /. rvt) in
  g


let rec init_buff = function
  |[] -> []
  |(g,d)::l -> (create_buffer g d)::(init_buff l)

let init () =
  let lcf = (0.773,1687)::(0.802,1601)::(0.753,2503)::(0.733,2251)::(0.738,1949)::(0.718,2113)::[] in
  let laf = (0.4,225)::(0.4,556)::(0.4,441)::(0.4,341)::[] in
  let rs =
    {
      del = 1.;
      posr = 0;
      cobuff = init_buff lcf;
      albuff = init_buff laf;
    }
  in rs

let init_g cobuff =
  let lcf = 0.773::0.802::0.753::0.733::0.738::0.718::[] in
  let j = ref 0 in
  let rec fct i lf = function
    |[] -> []
    |b::l -> b.g <- List.nth lf (!i);
             i := !i + 1;
	     b::(fct i lf l)
  in fct j lcf cobuff

let update rs delay =
  let rec fct d = function
    |[] -> []
    |b::l -> b.g <- b.g ** d; b::(fct d l)
  in
  init_g rs.cobuff;
  rs.del <- delay;
  rs.cobuff <- (fct (1. /. delay) (rs.cobuff))

let incrR rs =
  rs.posr <- rs.posr + 1;
  if (rs.posr >= 10 * 44100) then rs.posr <- 0

let incrW b =
  b.posw <- b.posw + 1;
  if (b.posw >= 10 * 44100) then b.posw <- 0

let comb b input rs =
  let out = b.dBuff.(rs.posr) in
  b.dBuff.(b.posw) <- (input +. out *. b.g);
  incrW b;
  out

let rec speComb input rs n = function
  |[] -> 0.
  |b::l -> (comb b input rs)/. n +. speComb input rs n l


let allp rs b input  =
  let out = b.dBuff.(rs.posr) in
  b.dBuff.(b.posw) <- input +. out *. b.g;
  incrW b;
  out -. input *. b.g

let rec speAllp rs input = function
  |[] -> 0.
  |b::[] -> allp rs b input
  |b::l -> speAllp rs (allp rs b input) l


let compute rs input =
  let out = ref (speComb input rs (float_of_int(List.length rs.cobuff)) rs.cobuff) in
  out := speAllp rs !out rs.albuff;
  incrR rs;
  out := 0.5 *. (!out +. input);
  !out
