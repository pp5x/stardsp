(*
 ** waveshaper.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sun Mar 24 10:00:18 2013 Pierre Pagnoux
** Last update Mon Apr 15 12:49:48 2013 Pierre Pagnoux
 *)

type t =
    {
      mutable drive             : float;
      mutable noise             : float;
    }

let init () =
  {
    drive = 0.8;
    noise = 0.1;
  }

let update t drive noise =
  t.drive <- drive;
  t.noise <- noise

let compute t sample =
  let pi = 4. *. atan 1. in
  let z = pi *. t.drive in
  let s = 1. /. sin z in
  let b = 1. /. t.drive in
  if (sample > b) then
    1.
  else
    sin (z *. sample) *. s

let compute1 t sample =
  let k = (2. *. t.drive) /. (1. -. t.drive) +. Random.float t.noise in
  ((1. +. k) *. sample) /. (1. +. k *. abs_float sample)

let compute2 t sample =
  if sample > 0. then
    t.drive +. (1. -. t.drive) *. tanh ((sample -. t.drive) /. (1. -. t.drive))
  else
    -.(t.drive +. (1. -. t.drive) *. tanh ((-.sample -. t.drive) /. (1. -. t.drive)))
