type t =
    {
      mutable i : float; (** permet de savoir sur quelle enceinte on est, PAS TOUCHE ! *)
      mutable p : float; (** Periode du signal, donner un temp en seconde *)
    }

val init : unit -> t
(** Initialize the stereo effect *)

val update : t -> float -> unit
(** Met à jour la periode p en seconde *)

val compute : t -> float -> float
(** Compute the stereo effect *)
