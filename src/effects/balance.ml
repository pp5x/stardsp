type t =
    {
      mutable i : int;
      mutable left : float;
      mutable right : float;
    }

let init () =
  {
    i = 0;
    left = 0.5;
    right = 0.5;
  }

let update t l =
  t.left <- l;
  t.right <- 1. -. l

let update2 t l r =
  t.left <- l;
  t.right <- r

let compute t sample =
  let out = ref sample in
  if (t.i mod 2 > 0) then
    out := !out *. t.left
  else
    out := !out *. t.right;
  t.i <- t.i + 1;
  !out
