type t =
    {
      mutable i : float;
      mutable p : float;
    }

let pi = 8. *. atan 1.

let init () =
  {
    i = 0.;
    p = 10.*. 44100.0;
  }

let update t p =
  t.p <- p *. 44100. *. 2.

let compute t sample =
  if t.p <= 0.01 then
    sample
  else
    let out = ref (abs_float (cos (t.i *. pi /. t.p))) in
    if ((int_of_float t.i) mod 2 > 0) then
      out := 1. -. !out;
    out := sample *. !out;
    t.i <- t.i +. 1.;
    !out
