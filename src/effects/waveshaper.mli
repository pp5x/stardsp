(*
** waveshaper.mli for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Fri Apr 12 19:12:18 2013 Pierre Pagnoux
** Last update Mon Apr 15 12:45:21 2013 Pierre Pagnoux
*)

type t =
    {
      mutable drive             : float;
      mutable noise             : float;
    }

val init : unit -> t
(** Initialize waveshaper render. *)

val update : t -> float -> float -> unit
(** Update waveshaper render. *)

val compute : t -> float -> float
(** Waveshaper effect type 1. *)

val compute1 : t -> float -> float
(** Waveshaper effect type 2.
    NB : needs random numbers (noise).
*)

val compute2 : t -> float -> float
(** Waveshaper effect type 3. *)
