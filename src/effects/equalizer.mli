(*
** equalizer.mli for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Sat Mar  9 11:25:31 2013 Pierre Pagnoux
** Last update Fri May 24 16:50:29 2013 Pierre Surply
*)

type t =
{
  mutable lf	: float;
  mutable f1p0	: float;
  mutable f1p1	: float;
  mutable f1p2	: float;
  mutable f1p3	: float;

  mutable hf	: float;
  mutable f2p0	: float;
  mutable f2p1	: float;
  mutable f2p2	: float;
  mutable f2p3	: float;

  mutable sdm1	: float;
  mutable sdm2	: float;
  mutable sdm3	: float;

  mutable lg	: float;
  mutable mg	: float;
  mutable hg	: float;
}

val init : float -> float -> float -> t
(** Initialize the equalizer render. *)

val compute : t -> float -> float
(** Compute the equalizer on sample value. *)

val update : t -> float -> float -> float -> unit
(** Update the equalizer settings. *)
