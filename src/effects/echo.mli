(*
** echo.mli for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Sat Apr 13 12:20:43 2013 Pierre Pagnoux
** Last update Sat Apr 13 12:25:36 2013 Pierre Pagnoux
*)

type t =
    {
      buffer		: (float, Bigarray.float32_elt, Bigarray.c_layout)
			   Bigarray.Array1.t;
      mutable position	: int;
      mutable delay	: int;
      mutable decay	: float;
    }

val init : unit -> t
(** Initialize the echo render. *)

val compute : t -> float -> float
(** Compute the echo effect on sample value. *)

val update : t -> float -> float -> unit
(** Update the echo settings :
    - delay in second
    - decay
*)
