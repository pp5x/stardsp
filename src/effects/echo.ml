(*
** echo.ml for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Fri Apr 12 19:17:12 2013 Pierre Pagnoux
** Last update Fri May 24 19:42:27 2013 Pierre Pagnoux
*)

open Conf

module B = Bigarray
module A = Bigarray.Array1

let max_delay = 4.

type t =
    {
      buffer		: (float, B.float32_elt, B.c_layout) A.t;
      mutable position	: int;
      mutable delay	: int;
      mutable decay	: float;
    }

let buffer () =
  A.create
    B.float32
    B.c_layout
    (int_of_float (max_delay *. (float_of_int conf.rate)))

let init () =
  {
    buffer	= buffer ();
    position	= 0;
    delay	= conf.rate;
    decay	= 0.5;
  }

let update t delay decay =
  if delay >= max_delay then
    invalid_arg "Delay value is too high.";
  if decay >= 1.0 then
    invalid_arg "Decay can't be higher than 1.0";
  if delay <= 0. then
    t.delay <- int_of_float max_delay
  else
    t.delay <- int_of_float (delay *. (float_of_int conf.rate));
  t.decay <- decay

let compute t sample =
  t.position <- (t.position + 1) mod t.delay;
  let delayed = t.buffer.{t.position} in
  t.buffer.{t.position} <- sample +. (delayed *. t.decay);
  t.buffer.{t.position}
