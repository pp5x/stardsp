type buffer =
{
  mutable g      : float;
  mutable posw   : int;
  mutable dBuff  : float array;
}

type t =
{
  mutable del       : float;
  mutable posr      : int;
  mutable cobuff    : buffer list;
  mutable albuff    : buffer list;
}

val init : unit -> t
(** Initialize the buffers for reverb *)

val update : t -> float -> unit
(** Update the delay of the reverb *)

val compute : t -> float -> float
(** Compute the reverb on sample value *)
