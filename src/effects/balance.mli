type t =
    {
      mutable i : int;
      mutable left : float; (** amp of left speaker *)
      mutable right : float; (** amp of right speaker *)
    }

val init : unit -> t
(** Initialize effect *)

val update : t -> float -> unit
(** Update just left, and change by the way right with it*)

val update2 : t -> float -> float -> unit
(** update left and right *)

val compute : t -> float -> float
(** Compute the effect*)
