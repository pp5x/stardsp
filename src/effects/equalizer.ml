(*
** equalizer.ml for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Sat Mar  9 11:20:29 2013 Pierre Pagnoux
** Last update Fri May 24 16:50:38 2013 Pierre Surply
*)

type t =
{
  mutable lf	: float;
  mutable f1p0	: float;
  mutable f1p1	: float;
  mutable f1p2	: float;
  mutable f1p3	: float;

  mutable hf	: float;
  mutable f2p0	: float;
  mutable f2p1	: float;
  mutable f2p2	: float;
  mutable f2p3	: float;

  mutable sdm1	: float;
  mutable sdm2	: float;
  mutable sdm3	: float;

  mutable lg	: float;
  mutable mg	: float;
  mutable hg	: float;
}

let init lowfreq highfreq mixfreq =
  let pi = 4. *. atan 1. in
  let es =
    {
      lf	= 2. *. sin(pi *. (lowfreq /. mixfreq));
      f1p0	= 0.;
      f1p1	= 0.;
      f1p2	= 0.;
      f1p3	= 0.;

      hf	= 2. *. sin(pi *. (highfreq /. mixfreq));
      f2p0	= 0.;
      f2p1	= 0.;
      f2p2	= 0.;
      f2p3	= 0.;

      sdm1	= 0.;
      sdm2	= 0.;
      sdm3	= 0.;

      lg	= 0.1;
      mg	= 0.5;
      hg	= 1.;
    }
  in
  es

let compute es sample =
  let vsa = 1. /. 4294967295. in
  es.f1p0 <- es.f1p0 +. (es.lf *. (sample -. es.f1p0)) +. vsa;
  es.f1p1 <- es.f1p1 +. (es.lf *. (es.f1p0 -. es.f1p1));
  es.f1p2 <- es.f1p2 +. (es.lf *. (es.f1p1 -. es.f1p2));
  es.f1p3 <- es.f1p3 +. (es.lf *. (es.f1p2 -. es.f1p3));
  let l = es.f1p3 in
  es.f2p0 <- es.f2p0 +. (es.hf *. (sample -. es.f2p0)) +. vsa;
  es.f2p1 <- es.f2p1 +. (es.hf *. (es.f2p0 -. es.f2p1));
  es.f2p2 <- es.f2p2 +. (es.hf *. (es.f2p1 -. es.f2p2));
  es.f2p3 <- es.f2p3 +. (es.hf *. (es.f2p2 -. es.f2p3));
  let h = es.sdm3 -. es.f2p3 in
  let m = sample -. (h +. l) in
  let l = l *. es.lg in
  let m = m *. es.mg in
  let h = h *. es.hg in
  es.sdm3 <- es.sdm2;
  es.sdm2 <- es.sdm1;
  es.sdm1 <- sample;
  l +. m +. h

let update es lg mg hg =
  es.lg <- lg;
  es.mg <- mg;
  es.hg <- hg
