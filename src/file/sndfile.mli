(*
 ** sndfile.mli for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sat Mar 23 11:18:06 2013 Pierre Pagnoux
** Last update Sat Apr  6 19:30:20 2013 Pierre Pagnoux
 *)

type t

val openf : string -> t
(** Open a sound file. *)

val closef : t -> unit
(** Close a sound file *)

val readf : t -> (float, Bigarray.float32_elt, Bigarray.c_layout)
  Bigarray.Array1.t -> int -> int
(** read : file -> buffer -> frames -> nRead
    Read "frames" (number of items * number of channels) in the file
    and fill the buffer.
    Return the number of frames read.
	0 : nothing more to read. *)
