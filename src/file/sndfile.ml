(*
 ** sndfile.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sat Mar 23 11:12:26 2013 Pierre Pagnoux
** Last update Sat Apr  6 19:30:41 2013 Pierre Pagnoux
 *)

module A = Bigarray.Array1
module B = Bigarray

type t

external openf   : string  -> t = "sndfile_open"
external closef  : t -> unit    = "sndfile_close"
external readf   : t -> (float, B.float32_elt, B.c_layout) A.t ->
  int -> int = "sndfile_readf"
