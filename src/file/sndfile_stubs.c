/*
** sndfile_stubs.c for StarDSP
**
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
**
** Started on  Sat Mar 23 09:32:22 2013 Pierre Pagnoux
** Last update Sat Mar 23 18:50:18 2013 Pierre Pagnoux
*/

#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/misc.h>
#include <caml/mlvalues.h>
#include <caml/signals.h>
#include <caml/bigarray.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>

CAMLprim
value sndfile_open(value str)
{
  char          *path;
  SF_INFO       sfinfo;
  SNDFILE       *sndfile;

  CAMLparam1 (str);

  path = String_val (str);
  memset(&sfinfo, 0, sizeof (sfinfo));

  sndfile = sf_open (path, SFM_READ, &sfinfo);

  if (!sndfile)
    fprintf (stderr, "%s", sf_strerror (sndfile));

  CAMLreturn ((value) sndfile);
}

CAMLprim
value sndfile_close(value file)
{
  CAMLparam1 (file);
  if (!(SNDFILE*) file)
    if (!sf_close ((SNDFILE*) file))
      fprintf (stderr, "%s", sf_strerror ((SNDFILE*) file));

  CAMLreturn (Val_unit);
}

CAMLprim
value sndfile_readf(value file,
                    value buffer,
                    value nbframes)
{
  SNDFILE       *sndfile;
  float         *ptr;
  sf_count_t    frames;
  sf_count_t    frames_read;

  CAMLparam3(file, buffer, nbframes);

  sndfile       = (SNDFILE*) file;
  ptr           = Data_bigarray_val (buffer);
  frames        = Int_val (nbframes);

  frames_read = sf_read_float (sndfile,
                               ptr,
                               frames);

  CAMLreturn (Val_int (frames_read));
}
