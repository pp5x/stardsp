(*
** handler.mli for StarDSP
** 
** Made by Pierre Pagnoux
** <Pierre.Pagnoux@gmail.com>
** 
** Started on  Sun Apr  7 15:45:28 2013 Pierre Pagnoux
** Last update Wed Apr 10 08:11:23 2013 Pierre Pagnoux
*)

val add : 'a -> int -> 'a list -> 'a list
(** Add an element at the index position in ocaml list.
    Index begin at position 1.
*)

val del : int -> 'a list -> 'a list
(** Delete an element at the index position in ocaml list.
    Index begin at position 1.
*)

val move : int -> int -> 'a list -> 'a list
(** Move an element at src position to dst position in ocaml list.
*)

val compute : (unit -> unit) list -> unit
(** Compute the effects list on the current sample.
*)

