(*
 ** audio.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sat Apr  6 16:56:58 2013 Pierre Pagnoux
** Last update Wed Apr 10 08:12:50 2013 Pierre Pagnoux
 *)

open Conf

module B = Bigarray
module A = B.Array1

type t =
    {
      buffer            : (float, B.float32_elt, B.c_layout) A.t;
      output            : Pa.t;
      input             : Pa.t;
      mutable mode      : unit -> unit;
      mutable effects   : (unit -> unit) list;
      mutable sample    : float;
    }

type mode =
  | None
  | File of Sndfile.t
  | Mic

let ss =
  {
    Pa.rate     = conf.rate;
    Pa.channels = conf.channels;
  }

let buffer () =
  A.create
    B.float32
    B.c_layout
    conf.size

let pa_output () =
  Pa.create
    ~name:"StarDSP"
    ~direction:Pa.Playback
    ~stream_name:"output"
    ~ss:ss ()

let pa_input () =
  Pa.create
    ~name:"StarDSP"
    ~direction:Pa.Record
    ~stream_name:"input"
    ~ss:ss ()

let loop t =
  for i = 0 to conf.size - 1 do
    t.sample <- t.buffer.{i};
    Handler.compute t.effects;
    t.buffer.{i} <- t.sample;
  done

let none () = ()

let mic t () =
  Pa.read t.input t.buffer conf.size;
  loop t;
  Pa.write t.output t.buffer conf.size

let file t fd () =
  let _ = Sndfile.readf fd t.buffer conf.size in
  loop t;
  Pa.write t.output t.buffer conf.size

let init () =
  {
    buffer      = buffer ();
    output      = pa_output ();
    input       = pa_input ();
    mode        = none;
    effects     = [];
    sample      = 0.0;
  }

let compute t =
  t.mode ()

let free t =
  Pa.free t.input;
  Pa.free t.output;
  t.mode <- none

let switch t = function
  | None        -> t.mode <- none
  | File (fd)   -> t.mode <- file t fd
  | Mic         -> t.mode <- mic t

let pass t f =
  fun () -> t.sample <- f t.sample
