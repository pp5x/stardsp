(*
 ** handler.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sat Apr  6 15:25:54 2013 Pierre Pagnoux
** Last update Wed Apr 10 08:10:38 2013 Pierre Pagnoux
 *)

let rec add f i = function
  | []                  -> f :: []
  | h :: t when i = 1   -> f :: h :: t
  | h :: t              -> h :: add f (i - 1) t

let rec del i = function
  | []                  -> failwith "Handler list is empty."
  | h :: t when i = 1   -> t
  | h :: t              -> h :: del (i - 1) t

let move src dst l =
  let rec get i = function
    | []                -> failwith "Handler list is empty."
    | h :: t when i = 1 -> h
    | h :: t            -> get (i - 1) t
  in
  let e = get src l in
  let l = del src l in
  add e dst l

let rec compute = function
  | []                  -> ()
  | h :: t              -> h (); compute t
