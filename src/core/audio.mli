(*
 ** audio.mli for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Sat Apr  6 19:45:50 2013 Pierre Pagnoux
** Last update Wed Apr 10 08:12:29 2013 Pierre Pagnoux
 *)

type t =
    {
      buffer            : (float, Bigarray.float32_elt, Bigarray.c_layout)
			   Bigarray.Array1.t;
      output            : Pa.t;
      input             : Pa.t;
      mutable mode      : unit -> unit;
      mutable effects   : (unit -> unit) list;
      mutable sample    : float;
    }
(** Audio core stuff. *)

type mode =
  | None
  | File of Sndfile.t
  | Mic
(** Audio core mode :
    - None : do nothing.
    - File : file mode with file descriptor.
    - Mic  : microphone mode.
*)

val init : unit -> t
(** Audio core initialization. *)

val compute : t -> unit
(** Depending on the mode previously selected,
    this will apply various effects.
*)

val free : t -> unit
(** Free core audio ressources. *)

val switch : t -> mode -> unit
(** It enable to change audio mode. *)

val pass : t -> (float -> float) -> (unit -> unit)
(** Transform an (float -> float) effect function into
    (unit -> unit) function with automatic load/save of sample.
*)
