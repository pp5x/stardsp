(*
 ** cli.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Fri Mar 22 18:41:05 2013 Pierre Pagnoux
** Last update Sat May 25 16:22:36 2013 Pierre Pagnoux
 *)

open Conf

let send_surface d =
  let tty = Term.create conf.surface in
  let _ = Term.send_int tty d in ();
  Term.delete tty;
  exit 0

let spec =
  [("-core", Arg.Unit (fun () -> conf.core_only <- true),
    " Run core only");
   ("-file", Arg.String (fun f -> conf.file <- f),
    "<path> AIFF file input");
   ("-surface", Arg.String
     begin
       fun d ->
         conf.surface <- d;
         (*conf.effects <- [Amplifier;
                          Echo;
                          Stereo]*)
     end,
    "<device> TTY");
   ("-ssurface", Arg.Int send_surface,
    "<data> test Control-surface");
   ("-eq",
    (let lg, mg, hg = (ref 0., ref 0., ref 0.) in
     Arg.Tuple [Arg.Float (fun f -> lg := f);
                Arg.Float (fun f -> mg := f);
                Arg.Float (fun f -> hg := f);
	        Arg.Unit (fun () ->
                  conf.effects <-
                   (Equalizer (!lg, !mg, !hg)) :: conf.effects)]),
    "<lg,mg,hg> Use Equalizer");
   ("-amp",
    (let drive, noise = (ref 0., ref 0.) in
     Arg.Tuple [Arg.Float (fun f -> drive := f);
                Arg.Float (fun f -> noise := f);
	        Arg.Unit (fun () ->
                  conf.effects <-
                    Amplifier (!drive, !noise) :: conf.effects)]),
    "<drive,noise> Use Amplifier");
   ("-rev",
    (let rev = ref 0. in
    Arg.Tuple [Arg.Float (fun f -> rev := f);
	       Arg.Unit (fun () ->
		 conf.effects <-
		   Reverberation (!rev) :: conf.effects)]),
    "<modulation> Use Reverberation");
   ("-echo",
    (let delay, decay = (ref 0., ref 0.) in
     Arg.Tuple [Arg.Float (fun f -> delay := f);
                Arg.Float (fun f -> decay := f);
	        Arg.Unit (fun () ->
                  conf.effects <- Echo (!delay, !decay) :: conf.effects)]),
    "<delay,decay> Use Echo");
   ("-stereo",
    (let modulation = ref 0. in
     Arg.Tuple [Arg.Float (fun f -> modulation := f);
	        Arg.Unit (fun () -> conf.effects <- Stereo (!modulation) :: conf.effects)]),
     "<modulation> Use Stereo effect");
   ("-balance",
    (let lr = ref 0. in
     Arg.Tuple [Arg.Float (fun f -> lr := f);
	        Arg.Unit (fun () -> conf.effects <- Balance (!lr) :: conf.effects)]),
    "<left/right> Stereo Balance");
   ("-default",
    Arg.Unit
      (fun () ->
        conf.effects <- [Amplifier (0., 0.);
                         Stereo (0.);
                         Echo (0.1, 0.1);
                         Reverberation  (5.)]),
   " Surface preset")
  ]

let parse () =
  Arg.parse
    (Arg.align spec)
    (fun _ -> ())
    ("Usage :\t./stardsp [OPTIONS]\n" ^
        "Input :\tPulseAudio or AIFF")
