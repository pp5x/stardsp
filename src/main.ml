(*
 ** main.ml for StarDSP
 **
 ** Made by Pierre Pagnoux
 ** <Pierre.Pagnoux@gmail.com>
 **
 ** Started on  Thu Feb 14 21:20:48 2013 Pierre Pagnoux
** Last update Sun May 26 15:35:20 2013 Pierre Pagnoux
 *)

open Conf

let core fd =
  (* Core Audio Initialization *)
  let a = Audio.init () in

  (* File mode *)
  let f = ref (
    if conf.file <> "" then
      if Sys.file_exists conf.file then
        Some (Sndfile.openf conf.file)
      else
        invalid_arg "The input file doesn't exist."
    else
      None)
  in
  begin
    match !f with
      | None    -> Audio.switch a Audio.Mic
      | Some f  -> Audio.switch a (Audio.File(f))
  end;

  (* CLI effects *)
  let rec cli_effects = function
    | [] -> ()
    | h :: t ->
      let f =
        match h with
          | Equalizer (lg, mg, hg) ->
            let eq = Equalizer.init 880. 5000. 44100. in
            Equalizer.update eq lg mg hg;
            conf.eq <- eq :: conf.eq;
            Audio.pass a (Equalizer.compute eq)

          | Amplifier (drive, noise) ->
            let amp = Waveshaper.init () in
            Waveshaper.update amp drive noise;
            conf.amp <- amp :: conf.amp;
            Audio.pass a (Waveshaper.compute1 amp)

          | Reverberation (modulation) ->
            let rev = Rev.init () in
            Rev.update rev modulation;
            conf.rev <- rev :: conf.rev;
            Audio.pass a (Rev.compute rev)

          | Echo (delay, decay) ->
            let echo = Echo.init () in
            conf.echo <- echo :: conf.echo;
            Echo.update echo delay decay;
            Audio.pass a (Echo.compute echo)

          | Stereo (modulation) ->
            let stereo = Stereo.init () in
            conf.stereo <- stereo :: conf.stereo;
            Stereo.update stereo modulation;
            Audio.pass a (Stereo.compute stereo)

          | Balance (lr) ->
            let balance = Balance.init () in
            conf.balance <- balance :: conf.balance;
            Balance.update balance lr;
            Audio.pass a (Balance.compute balance)
      in
      a.Audio.effects <- Handler.add f 1 a.Audio.effects;
      cli_effects t
  in
  cli_effects conf.effects;

  (* Init random generator *)
  Random.self_init ();

  let surface = Surface.create conf.surface in
  Surface.connect surface conf.file;
  let processing = ref
    begin
      match fd with
        | Some _        -> false
        | None          -> true
    end
  in
  let buff = String.create 8 in

  (* Orders *)
  let handle_order (pos, order) =
    let conv i =
      (float i /. 100.)
    in
    match order with
      | Com.Add c ->
        let f =
          begin
            match c with
              | 's' | 't' ->
                let amp = Waveshaper.init () in
                conf.amp <- amp :: conf.amp;
                Audio.pass a (Waveshaper.compute1 amp)

              | 'u'       ->
                let eq = Equalizer.init 880. 5000. 44100. in
                conf.eq <- eq :: conf.eq;
                Audio.pass a (Equalizer.compute eq)

              | 'v' | 'w' ->
                let echo = Echo.init () in
                conf.echo <- echo :: conf.echo;
                Audio.pass a (Echo.compute echo)

              | 'x'       ->
                let rev = Rev.init () in
                conf.rev <- rev :: conf.rev;
                Audio.pass a (Rev.compute rev)

              | 'y'       ->
                let balance = Balance.init () in
                conf.balance <- balance :: conf.balance;
                Audio.pass a (Balance.compute balance)

              | 'z'       ->
                let stereo = Stereo.init () in
                conf.stereo <- stereo :: conf.stereo;
                Audio.pass a (Stereo.compute stereo)

              | _ -> failwith "Unknown effect to add."
          end
        in
        a.Audio.effects <- Handler.add f 1 a.Audio.effects

      | Com.Rm (c, i) ->
	a.Audio.effects <- Handler.del pos a.Audio.effects;
        begin
          match c with
            | 's' | 't' ->
	      conf.amp <- Handler.del i conf.amp
            | 'u'	->
	      conf.eq <- Handler.del i conf.eq
            | 'v' | 'w' ->
	      conf.echo <- Handler.del i conf.echo
            | 'x'	->
	      conf.rev <- Handler.del i conf.rev
            | 'y'	->
	      conf.balance <- Handler.del i conf.balance
            | 'z'	->
	      conf.stereo <- Handler.del i conf.stereo
            | _ -> failwith "Unknown effect to remove."
        end
      | Com.Amp_drive i ->
        if conf.amp = [] then ()
        else
          (List.nth conf.amp pos).Waveshaper.drive <- conv i

      | Com.Amp_noise i ->
        if conf.amp = [] then ()
        else
          (List.nth conf.amp pos).Waveshaper.noise <- conv i

      | Com.Eq (lg, mg, hg) ->
        if conf.eq = [] then ()
        else
          Equalizer.update (List.nth conf.eq pos)
            (conv lg)
            (conv mg)
            (conv hg)

      | Com.Echo_delay i ->
        if conf.echo = [] then ()
        else (List.nth conf.echo pos).Echo.delay <-
          (let delay = (float i) /. 100. *. 2. in
           if delay <= 0. then
             4
           else
             int_of_float (delay *. (float_of_int conf.rate)))

      | Com.Echo_decay i ->
        if conf.echo = [] then ()
        else (List.nth conf.echo pos).Echo.decay <- conv i

      | Com.Rev i ->
        if conf.rev = [] then ()
        else Rev.update (List.nth conf.rev pos) (float i /. 10.)

      | Com.Balance i ->
        if conf.balance = [] then ()
        else Balance.update (List.nth conf.balance pos) (conv i)

      | Com.Stereo i ->
        if conf.stereo = [] then ()
        else Stereo.update (List.nth conf.stereo pos) (conv i)

      | Com.Process   ->
        begin
          processing := not !processing;
          if !processing then
            Printf.printf "\r               \r%!"
          else
            Printf.printf "=== PAUSE ===%!"
        end

      | Com.File str ->
        begin
          match !f with
            | None -> ()
            | Some i -> Sndfile.closef i
        end;
        begin
          try
            f := Some (Sndfile.openf str)
          with _ -> f := None
        end;
        begin
          match !f with
            | None -> ()
            | Some i -> Audio.switch a (Audio.File (i))
        end

      | Com.Realtime ->
        begin
          match !f with
            | None -> ()
            | Some i -> Sndfile.closef i;
              Audio.switch a Audio.Mic
        end

      | Com.Quit      ->
        Printf.printf "Stopped by the GUI\n%!";
        exit 0
  in
  let get_order_from_pipe fd =
    try
      if Unix.read fd buff 0 1 > 0 then
        handle_order (Com.char_to_order fd buff.[0])
    with
      | Unix.Unix_error (Unix.EAGAIN, "read", "") -> ()
      | Com.Unknown_order order ->
        Printf.fprintf stderr "Core : Unknown order %c\n" order
      | _ ->
        (Printf.fprintf stderr "Core : Broken pipe\n";
         exit 2)
  in
  (* Main loop *)
  while true do
    begin
      match fd with
        | None -> ()
        | Some fd -> get_order_from_pipe fd
    end;
    begin
      match Surface.update surface a.Audio.buffer.{0} !processing with
        | Some order -> handle_order (0, order)
        | None -> ()
    end;
    if !processing then
      Audio.compute a
    else
      let _ = Unix.select [] [] [] 0.01 in ()
  done;
  let _ =
    match !f with
      | None    -> ()
      | Some f  -> Sndfile.closef f
  in
  Audio.free a

let main () =
  Cli.parse ();
  if conf.core_only then
    core None
  else
    begin
      let fromgui, tocore = Unix.pipe() in
      Printf.printf "StarDSP%!\n";
      let pid_gui = Unix.fork () in
      if pid_gui = 0 then
        begin
          Unix.close fromgui;
          Gui.main tocore
        end
      else
        begin
          Unix.close tocore;
          Sys.set_signal Sys.sigint
            (Sys.Signal_handle (fun _ ->
              let _ = Unix.wait () in
              Printf.printf "Bye !\n";
              exit 0));
          Printf.printf "PID GUI : %d\n%!" pid_gui;
          Unix.set_nonblock fromgui;
          core (Some fromgui)
        end
    end

let _ = main ()
