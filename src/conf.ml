type effect =
  | Equalizer           of float * float * float
  | Amplifier           of float * float
  | Reverberation	of float
  | Echo                of float * float
  | Stereo              of float
  | Balance             of float

type arg =
{
  mutable core_only     : bool;
  mutable file      	: string;
  mutable size      	: int;
  mutable rate		: int;
  mutable channels	: int;
  mutable surface	: string;
  mutable effects	: effect list;
  mutable eq            : Equalizer.t list;
  mutable amp           : Waveshaper.t list;
  mutable rev           : Rev.t list;
  mutable echo          : Echo.t list;
  mutable stereo        : Stereo.t list;
  mutable balance       : Balance.t list;
}

let conf =
{
  core_only	= false;
  file		= "";
  size		= 1024;
  rate		= 44100;
  channels	= 2;
  surface	= "";
  effects	= [];
  eq            = [];
  amp           = [];
  rev           = [];
  echo          = [];
  stereo        = [];
  balance       = [];
}
