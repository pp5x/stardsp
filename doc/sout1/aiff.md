## Fichiers AIFF

Au cours de la phase de développement de ce projet, nous avons besoin de régulierement tester notre travail. C'est pourquoi, nous avons ajouté la prise en charge de la lecture de fichiers `AIFF`. Pour cela, nous reprenons une bibliothèque écrite en `C`. La `LibAIFF` nous a permis de nous simplifier la tâche, en ce qui concerne la lecture de ce format de fichier, étant donné que cela n'est pas le but principal de notre projet.

Nous avons réalisé un *binding* de cette bibliothèque. Cela consiste à traduire les fonctions de cette bibliothèque en `OCaml`. Nous obtenons au final des fonctions simples :

~~~ {.ocaml}
val open_file	: string -> t
val close_file	: t -> unit
val read		: t -> (float, Bigarray.float32_elt, Bigarray.c_layout)
	Bigarray.Array1.t -> int -> int
~~~

On peux facilement remarquer que la lecture des fichiers AIFF consiste a remplir un *buffer* de nombres flottants -- representant les valeurs discrètes du signal sonore numérisé. On appelle communément ces valeurs *sample*.