# Amplification

Le but principal de ce projet est d’amplifier une onde sonore et à cela s’ajoute divers effets. Le son est une onde produite par la vibration mécanique d’un support *fluide* ou *mécanique* et propagée grâce à l’élasticité du milieu environnant sous forme d’ondes *longitudinales*. Dans notre cas, le son est produit par un instrument de musique et est capté par un micro — classique ou bien piézo-électrique. On obtient alors un signal analogique continu qui est transmis via un câble jusqu’à la carte son de notre ordinateur.

## Signaux analogiques et discrets

Sur un modèle électronique, un amplificateur et les autres effets sont reliés les uns à la suite des autres. Les modifications du signal opèrent grâce des matériaux électroniques — condensateurs, transistors, résistances, etc ... Le signal analogue d’entré $x(t)$ est traduit en un nouveau signal analogue de sortie $y(t)$.

Sur un modèle informatique, l’ordinateur manipule des valeurs dites *discrètes*. Le signal analogique est capté par un récepteur — un micro — et est traduit par le périphérique d’*acquisition* — carte son — en signal discret. Le signal est enregistré dans un tableau appelé *buffer* sur lequel est effectué le traitement. Le signal d’entré $x[t]$ est alors transformé en un nouveau signal discret de sortie $y[t]$.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.20]{../img/analog_discrete.png}
\caption{Terminologie des signaux et systèmes analogiques et discrets.}
\end{figure}

Lorsque le signal entre dans la carte son, il est échantillonné selon la *fréquence d’échantillonnage*. Plus la fréquence d’échantillonnage est élevée plus le signal est précis *i-e* que nous sommes capable de visualiser les fréquences élevée du signal analogique. De même la *résolution de quantification* — en *bits* permet d’améliorer la précision de la mesure du signal. Par example, un disque compact est échantillonné à $44,1$ KHz et quantifié à 16 bits.

## Distorsion

Un amplificateur idéal conserve la forme du signal d’entrée et augmente l’amplitude. L’amplification que nous souhaitons n’est pas celle d’un amplificateur idéal. Nous souhaitons reproduire les effets de *sustain* et *crunch* des amplis pour guitare électrique. 

Grâce à la distorsion, les notes acquièrent un son diffus. Plus on augmente la distorsion et plus le son naturel de l’instrument est brouillé au profit du *crunch* et du *sustain* qui augmentent. Cet effet, obtenu par saturation d’un amplificateur classique, procède par *écrêtage* : comprime l’attaque de la note jouée tout en augmentant ses armoniques, ce qui lui donne plus de tenue et de fluidité ainsi qu’un *timbre* substantiellement modifié.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.50]{../img/ecretage.png}
\caption{L’effet d’écrêtage.}
\end{figure}

D’une manière simple, le phénomène recherché est produit par une perte d’information volontaire. Il suffit donc de définir un seuil pour notre signal. Notre signal est transformé de manière à ne pas dépasser ce seuil.