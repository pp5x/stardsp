# La surface de contrôle

En plus d'être utilisable avec une interface graphique, le logiciel *StarDSP* pourra être contrôlé à l'aide d'une surface de contrôle séparé de l'ordinateur.

Cette surface de contrôle, que nous fabriquerons nous-même, permettra de contrôler le logiciel sans avoir besoin d'utiliser le clavier et la souris. Le but ici est de grandement simplifier l'ergonomie du *DSP*.

Elle proposera donc à l'utilisateur les principales fonctionnalités du programme à l'aide de potentiomètres, d'interrupteurs et de boutons poussoirs. Le tout sera disposé de manière ergonomique sur un boitier pupitre.

## État d'avancement

La surface de contrôle est déjà capable de gérer 8 `LEDS` représentant l'intensité du signal sonore traité par le logiciel.

En effet, *StarDSP* envoie réguliérement au système électronique la valeur de l'intensité d'un échantillon sonore par `RS-232`. Le microcontrôleur inclus dans le système se charge alors d'allumer les `LEDS` en fonction de l'information reçue.

## Le microcontrôleur `Atmega 8515`

### Caractéristiques

Le microcontrôleur que nous utiliserons pour cette première soutenance sera l'`Atmega 8515` d'*Atmel*.

Ce dernier possède 35 ports d'entrées/sorties programmables (utilisés pour le contrôle des `LEDS`) et une interface `UART` pour communiquer par `RS-232`.

\begin{figure}[!h]
\centering
\includegraphics{../img/pinout.png}
\caption{Pin configuration}
\end{figure}

Ce composant est programmé à l'aide de la carte de développement `STK500` fabriquée par *Atmel*. Elle permet de d'injecter le code machine par *ICSP* (In Circuit Serial Programming) et de tester rapidement l'état des ports grâce à 8 `LEDS` et 8 switches.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.50]{../img/STK500.jpg}
\caption{STK500}
\end{figure}

### Gestion des sorties parallèles

Les `LEDS` de la surface de contrôle sont gérées par les sorties parallèles du microcontrôleur.

Les ports du `8515` étant bidirectionnels, il est nécessaire de procéder à une courte phase de paramétrage de ces derniers avant de pouvoir les utiliser en tant que *sortie*.

#### Registres de contrôle

##### PORT B Data (`PORTB`)

Le registre `PORTB` permet de définir les valeurs des ports `PB0` à `PB7`.

Par exemple, si le bit 5 du registre `PORTB` est à 1, alors le port `PB5` sera une source de tension. Il sera relié à la masse sinon (*sink*).

##### PORT B Data Direction (`DDRB`)

Le registre `DDRB` permet, quant à lui, de définir la direction des ports `PB0` à `PB7`, càd de définir si un port est une entrée ou une sortie.

Par exemple, si le bit 3 du registre `DDRB` est à 1, alors le port `PB3` sera une sortie. Il sera une entrée sinon.

#### Utilisation des sorties parallèles en langage d'assembleur

La routine suivante place le port `PB0` en tant que source de tension :

    pb0_high:
                ldi r24, (1 << PB0)
                ldi r25, (1 << DDB0)
                out PORTB, r24
                out DDB0, r25
                ret

### Gestion de l'`UART`

L'`UART` (Universal Asynchronous Serial Receiver and Transmitter) permet au `8515` de communiquer à travers une liaison série asynchrone.

Dans notre cas, elle permet de recevoir les différentes valeurs d'intensité du signal sonore traité.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.50]{../img/uart.png}
\caption{Fonctionnement de l'UART}
\end{figure}

#### Format de trame

\begin{figure}[!h]
\centering
\includegraphics[scale=0.50]{../img/frame_format.png}
\caption{Format de trame}
\end{figure}

Pour la surface de contrôle, nous transmettrons des données codées sur 8 bits, envoyées sans bit de parité et en utilisant uniquement un seul bit de d'arrêt.

#### Registres de contrôle

##### UART Data Register (`UDR`)

Que ce soit pour la transmission ou pour la réception, le registre `UDR` servira de registre de donnée. Une donnée à envoyée sera au préalable chargée dans ce registre, tandis qu'une donnée reçue pourra simplement être lu à l'emplacement mémoire de ce dernier.

##### UART Baud Rate Register (`UBRRH:UBRRL`)

Le registre `UBBR` défini la vitesse de transmission des données. Sa valeur peut être calculée en fonction de la fréquence d'oscillation de l'horloge du microcontrôleur et de la vitesse de transmission souhaitée ainsi :

$$ UBBR = \frac{f_{osc}}{BAUD \times 16} - 1$$

$BAUD$ : Vitesse de transmission en $Baud$

$f_{osc}$ : Fréquence d'oscillation de l'horloge du MCU en $Hz$

Sachant que le `STK500` cadence le `8515` à $3.68 MHz$ et que l'on souhaite communiquer à une vitesse de $9600 Baud$, on a donc

$$ UBBR = \frac{3 680 000}{9600 \times 16} - 1 = 22$$

##### UART Control Status Register (`UCSRA:UCSRB:UCSRC`)

La programmation des différents modes de fonctionnement de l'`UART` est, quant à elle, définie par les registres de contrôle `UCSRA:UCSRB:UCSRC`.

Selon la documentation de constructeur, il est nécessaire de placer les bits `UCSZ2:UCSZ1:UCSZ0` du registre `UCSRC` à $011$ pour un format de trame de 8 bits avec 1 bit de stop.

#### Configuration en langage d'assembleur

On suppose que la nouvelle valeur du `BAUD RATE REGISTER` soit dans les registres `r25:r24`.

    uart_init:
                ; Initialisation du BAUD RATE REGISTER
                out UBRRL, r24
                out UBRRH, r25
                
                ; Activation de la transmission et de la reception
                ldi r24, (1 << TXEN) | (1 << RXEN)
                out UCSRB, r24
                
                ; Configuration du 'Frame format' : 8 bits et 1 bit de stop
                ldi r24, (1 << URSEL) | (3 << UCSZ0)
                out UCSRC, r24
                
                ret
    
#### Réception en langage d'assembleur

La routine ci-dessous place la donnée reçue par `UART` dans le registre `r24` :

    uart_recv:
                sbis UCSRA, RXD
                rjmp uart_recv
                in   r24, UDR
                ret

## Utilisation de *Mara*

*Mara*[^1] est un langage de programmation dédié aux microcontrôleurs `AVR` que je développe à titre personnel depuis Septembre 2012. Celui-ci a pour but de simplifier l'utilisation de ces composants.

Nous l'utilisons pour programmer notre surface de contrôle.

Grâce à la simplicité du langage, le programme principal ne contient que quelques lignes :

    begin setup
      PORTB.set_mode(0xFF)
      PORTB.write(0xFF)
      var uart <- new uart(22)
    end

    begin loop
      if uart.available() then
        var a <- uart.recv()
        PORTB.write(~a)
      endif
    end

[^1]: \url{http://www.mara.psurply.com/}

Ce projet a aussi été l'occasion d'ajouter les drivers d'`UART` à la bibliothèque standard de *Mara*.

Ainsi, la classe permettant de contrôler ce périphérique a pour signature :

    # integer rate
    # init: (rate:integer) -> uart
    # destr: uart
    # send: (x:undef) -> undef
    # recv: undef
    # available: integer
    # write: (s:cstring) -> undef
    
## Le programme *Maitre*

### Gestion des terminaux en *C*

En ce qui concerne le programme *Maitre* inclus dans le logiciel *StarDSP*, il a été nécessaire d'adapter la structure *C* `termios` au *Caml*.

En effet, cette structure, définie dans `termios.h` sur les systèmes `POSIX`, permet de configurer un terminal représenté par un descripteur de fichier. Sur *Linux*, les terminaux connectés en *USB* sont décrits généralement par `/dev/ttyUSBX` tandis que sur *BSD*, ils sont plutôt représentés par `/dev/ttyUX`. Cependant, ces derniers s'utilisent de la même manière, seule la dénomination change.

Cette structure est définie par `POSIX` ainsi :

~~~{.c}

    tcflag_t c_iflag;      /* input modes */
    tcflag_t c_oflag;      /* output modes */
    tcflag_t c_cflag;      /* control modes */
    tcflag_t c_lflag;      /* local modes */
    cc_t     c_cc[NCCS];   /* special characters */
                                             
~~~

Pour le format de trame utilisé (`8N1` à 9600 Baud), il est nécessaire de disposer les flags de cette manière :

~~~{.c}

    // B9600    : Vitesse de transmission à 9600 Baud
    // CS8      : Donnée sur 8 bits 
    t.c_cflag = B9600 | CS8;
    // IGNPAR   : Ignorer les erreurs de trame et de parité
    t.c_iflag = IGNPAR;
    t.c_oflag = 0;
    t.c_lflag = 0;
    
~~~

La configuration pourra alors être appliquée au descripteur de fichier grâce à la fonction `tcseattr`.

Une fois configurée, il est possible d'envoyer et de recevoir des informations uniquement grâce aux fonctions standard `write` et `read` déclarées dans l'en-tête `unistd.h`.

### Utilisation en *OCaml*

Une fois la gestion des terminaux réalisée en *C*, il a été nécessaire de rendre disponible ces fonctions à partir de *Caml*.

La communication *Caml*/*C* s'effectue donc grâce à ces fonctions :

~~~{.ocaml}

    type t

    val create          : string -> t
    (* Ouverture d'un terminal *)
    
    val delete          : t -> unit
    (* Fermeture d'un terminal *)
    
    val send_int        : t -> int -> unit
    (* Envoie d'un entier (8 bits) *)
    
    val send_char       : t -> char -> unit
    (* Envoie d'un caractère *)
    
~~~

Vous remarquerez que seules les fonctions d'envoi sont disponibles, la gestion de la réception sera implémentée par la suite.
