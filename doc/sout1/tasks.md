#### Répartition des tâches
* Pierre \textsc{Surply}
	* PulseAudio *binding*
	* Surface de contrôle
	* Mara
	* CGI \& moteur de template
* Pierre \textsc{Pagnoux}
	* Fichiers AIFF
	* Amplification
	* Égaliseur *3-Band*
* Alexandra \textsc{Pizzini}
	* GUI
	* Contenu du site web
* Vincent \textsc{Molinié}