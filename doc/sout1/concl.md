\chapter*{Conclusion}

Cette première partie a été très enrichissante. Le projet commence a prendre forme et démarre bien. Pour la deuxième soutenance le projet sera beaucou plus abouti et verra apparaître un boitier de contrôle. L'interface graphique sera plus avancée et adaptée à nos besoins. Aussi le site Web sera plus complet. Les algorithmes d'analyse seront plus performants et sophistiqués.

Nous espérons garder notre bonne humeur et notre enthousiasme pour que ce projet soit *presque* parfait. Dans tout les cas, la suite s'annonce très interessante !
