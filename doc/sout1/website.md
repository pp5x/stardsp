# Site web

Pour ce projet le site Web est le principal support d'information de notre projet. Il devra permettre à l'utilisateur de disposer de toute l'aide et informations nécessaire à la bonne utilisation de *StarDSP*. Il aura ainsi accès à la description du projet ainsi qu'a une brève présentation de l'équipe.

## Conception du site web

Le projet StarDSP, se classant dans la catégorie des utilitaires, nous avons opté pour un style très épuré. Le site web est un élément important. Il s’agit du premier contact avec l’utilisateur. Nous avons donc décidé d’aborder une interface très claire et simple d’utilisation. Il est a noter que le design *CSS* a été fait de nos propres mains !

La grande nouveauté pour ce projet est l’utilisation d’un moteur de template, réalisé par Pierre — *l’autre !* Ce moteur de template a considérablement accéléré la création du site internet. Et évite l’apprentissage d’un langage comme *PHP*.