
## Graphical User Interface

L'objectif de *StarDSP* est de reproduire les fonctionnalités d'un amplificateur de guitare. Pour faciliter l'utilisation de ce logiciel nous disposons d'une interface graphique. Notre interface est réalisée avec `LablGTK 2.0` et `OCaml`.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.50]{../img/gui.png}
\caption{Interface graphique}
\end{figure}

Cette interface est pour l'instant sobre et épurée, elle contient peu de fonctionnalités.

Le menu *File* permet depuis *Quit* de quitter l'application autrement qu'avec la croix.

Le curseur gain peut varier de -$\infty$ dB à 10 dB, ces valeurs sont pour l'instant arbitraires. Le button *On* permet d'activer ou non cette option.

Les trois curseurs de la partie *Equalizer* sont les curseurs d'un égaliseur 3 bandes.

- Le premier noté L correspond au *Low Gain* ou *Gain Basse Fréquence*, il permet de faire varier les basses d'une piste audio.
- Le second noté M correspond au *Medium Gain* ou *Gain Fréquence Moyenne*, il permet de faire varier les moyennes fréquences.
- Le troisième noté H correspond au *High Gain* ou *Gain Haute Fréquence*, il permet de faire varier les hautes fréquences.

Comme pour le gain, le bouton *On* permet d'activer la fonctionnalité. Le curseur volume sert à augmenter ou diminuer le volume de la piste audio. Le bouton *Processing* est ce qu'on appelle un `Toogle Button`, il reste enclenché tant que l'on ne réappuie pas dessus. Il permet d'entendre ou non la piste audio.

Passons maintenant à l'aspect plus technique. `LablGTK` se sert d'un système de *boites* pour gérer ses widgets. Il existe des hbox — horizontal box -- et des vbox
— vertical box, ces *boîtes* ajoutent les widgets horizontalement ou verticalement. Elles peuvent s'imbriquer les unes dans les autres pour des interfaces plus complexes.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{../img/gui_exp.png}
\caption{Interface graphique}
\end{figure}


On peut voir sur l'image ci-dessus qu'il faut beaucoup de *boîtes* pour faire un interface assez simple. La gestion de celles-ci est un peu fastidieuse au début mais un petit schéma aide toujours.
