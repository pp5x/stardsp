\chapter*{Introduction}

Les étudiants restants au second semestre d'*InfoSPE* doivent réaliser un projet libre. Notre projet *StarDSP* est un processeur de signal numérique — *DSP* -- que l'on utilisera comme amplificateur de guitare électrique ou plus généralement de piste audio.

Ce projet est très intéressant et algorithmiquement complexe. En effet, le traitement de signal est un domaine compliqué d'un point de vue mathématique et algorithmique. Nous devons donc étudier ses différents aspects avant de pouvoir les mettre en place.

Pour cette première soutenance, nous possédons déjà un égaliseur 3 bandes, une surface de contrôle, une interface graphique et un site internet. Nous utilisons `PulseAudio` et une bibliothèque pour lire des fichiers `AIFF`. Nos algorithmes sont pour l'instant assez simples mais n'attendent que d'être développés.

