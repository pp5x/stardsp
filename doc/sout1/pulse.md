# PulseAudio

## Adaptation de la bibliothèque client *PulseAudio* pour le langage *OCaml*

PulseAudio est le serveur de son que nous utilisons dans ce projet. Il permet de grandement simplifier l'utilisation du matériel audio de la machine.

Cependant, la bibliothèque permettant de communiquer avec ce serveur est seulement disponible en *C*. C'est pour cela qu'il a été nécessaire de l'adapter au *Caml*.

L'interface réalisée spécialement pour ce projet prend la forme du bibliothèque *Caml* indépendante de *StarDSP*. Elle doit donc être installée au préable sur la machine de l'utilisateur avant de pouvoir utiliser notre projet.

La bibliothèque en elle-même fournit des fonctions pour jouer et lire du son en faisant une complète abstraction du matériel de l'utilisateur. Les données sont échangées sous forme de *buffer* dont la taille peut être paramétrée au préalable. Les traitements effectués par *StarDSP* s'appliquerons donc sur ces derniers.

## Exemple d'utilisation de la bibliothèque *PA*

Le programme ci-dessous joue simplement sur la sortie audio principale le son transmit par le microphone.

~~~ {.ocaml}

let main () =
  let ss =
    {
      Pa.rate = 44100;
      Pa.channels = 2;
    }
  in
  let size = 512 in
  let buffer = Bigarray.Array1.create
    Bigarray.float32
    Bigarray.c_layout
    size
  in
  let simple_rec =
    Pa.create
      ~name:"Echo"
      ~direction:Pa.Record
      ~stream_name:"read"
      ~ss:ss ()
  in
  let simple_play =
    Pa.create
      ~name:"Echo"
      ~direction:Pa.Playback
      ~stream_name:"play"
      ~ss:ss ()
  in
  while true do
    Pa.read simple_rec buffer size;
    Pa.write simple_play buffer size
  done;
  Pa.free simple_rec;
  Pa.free simple_play;
  exit 0

let _ = main ()
~~~
