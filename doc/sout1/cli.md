# CLI \& GUI

## Command Line Interface

*CLI* ou bien *Command Line Interface* permet à l’utilisateur de se passer d’une interface graphique.

\begin{verbatim}
$ ./stardsp —help
Usage : ./stardsp [OPTIONS]
Input : PulseAudio or AIFF
	-gui						GUI Mode
	-file <path>				AIFF file input
	-dev <device>				TTY
	-send-surface <data>		test Control-surface
	-use-surface				Use Control-surface
\end{verbatim}

Les options qui sont présentes permettent à l’utilisateur de choisir le mode d’éxecution de StarDSP :

- `PulseAudio`
- Fichier `AIFF`
- GUI

De même, nous avons également ajouté des options pour la manipulation de la *surface de contrôle*.

Cette interface textuelle se réalise facilement avec le module `Arg` d’`OCaml`.