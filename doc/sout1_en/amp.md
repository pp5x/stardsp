# Sound effects

## Amplification

The main goal of our project is to amplify a sound wave with different effects. The sound comes from a vibration that is transmitted by the air. In our case, the sound is produced by a musical instrument. The sound is recorded using a microphone and then transmitted to a computer’s sound card.

On an electronic model, modifications of the input signal are operated by electronic components such as capacitor, resistor, transistor.
On a computer model, the computer handles discrete values. The analog signal is picked up by a receiver — a microphone — and translated by the capture device — the sound card — in discrete signal. Then the signal is stocked in an array also called *buffer*. The input signal $x[i]$ is transformed in an output signal $y[i]$.

When the signal enters the sound card, it is sampled by the sampling frequency. The higher is the sampling frequency, the higher the signal is accurate, which mean that we are able to visualize the high frequency of the analog signal. Similarly a good quantization resolution improves the accuracy of the measurement signal. For example, an audio CD is sampled at $44, 1$ KHz and quantized to 16 bits.

A simple algorithm to amplify a sound is to multiply each discrete value by the gain.

## Equalizer

Equalization is the process of adjusting the balance between frequency components within an algorithm. It enables us to emphasize a part of the sound such as bass, medium or treble. Following this way, we can accentuate some characteristics of the instrument. For example, if the musician wanted to play a solo, he will probably choose to enhance treble because that will distinguish his part from the background music.

There are lots of methods to do that, such as the *Fast Fourier Transform* algorithm. But this one didn’t respond to our needs, so we chose the convolution method. Convolution is a mathematical way of combining two signals to form a third signal. It is the single most important technique in Digital Signal Processing.

## Echo

This effect is widespread in music. It is similar to the echo that we can reproduce in mountainous regions. The sound propagates until it encounters an obstacle — the mountain — and is returned in the opposite direction with a slight attenuation due to the elasticity of the air. We can therefore hear the same sound repeatedly at regular interval until it becomes inaudible due to attenuation.

To do this kind of effect, we have to introduce a time dimension in our project. The is in fact represented by a buffer. We chose $44 100$Hz as sampling frequency, which means that our main buffer must have the size of $44 100$ values. Filling a buffer of $44 100$ values is equivalent to one second of time. If we want to reproduce an echo with four seconds of delay, we have to set our circular buffer size to $4 * 44 100$. Then we fill the buffer with the current sample value and we keep the previous value which is multiplied by an attenuation coefficient.
