## Reverberation

Reverberation is the persistence of sound in a place while the source no longer exists. It is an addition of successive echo, with different delays. This effect tries to simulate the ambiance of a room in which the sound signal will bounce several obstacles. For example, in a cathedral when the product sound is high there will be a natural reverb. This is one of the most popular musical effects, because it's very pleasing to the ear and it allows the musician to prolong a sound.
For implementing it, we need before to implement a comb filter and an all-pass filter.
A comb filter is used for adding to a signal a delay version of this last one.
An all-pass filter doesn't change the signal but the phase response is delayed.
When these two filters are implemented we just have to combine them in parallel or in series. I choose to combine six comb filters in parallel followed by four all-pass filters in series. I was inspired by the Schroeder and Moorer algorithms.