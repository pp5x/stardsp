# Connections

Our project is composed of some master parts. We need to use a communication system for sending orders between them. It’s a very important thing. Without it, we wouldn’t be able to make everything work correctly.

In our project we can distinguish three major parts :

* the core
* the control surface
* the graphical user interface
\newline

Each parts must run independently to obtain a fluid behavior.  We introduce a communication system to send messages to each part. We’re using a *pipe* system which consist to create subprocess and use *pipes* for communication. We’re sending messages under the form of *characters* — *i-e* one byte.

Once we have received the first byte, we analyse it. Then the program follows a predetermined behavior. For example, if the *GUI* tell to change the value of the drive of the amplification. It will send a message to the *core* with the new value of the drive. Then, the core analyses the message and do the modifications as well.