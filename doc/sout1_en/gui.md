# GUI

GUI is for *Graphical User Interface*. The GUI helps the user to interact  with the software. In fact, using a terminal and command line is more difficult than just clicking a button. An interface should be simple to use and have a good look.
\newline

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.40]{../img/gui.png}
\end{figure}

Also, GUI mixes all the tools of the software in one window. To make this interface we used LablGTK 2.0 with OCaml. For the first *presentation* we use *boxes* to include widgets in the GUI window. There are two types of *boxes*  : *hbox* for horizontal *boxes* and *vbox* for vertical *boxes*. The *hbox* adds widgets horizontally and the *vbox* adds widgets vertically. *Boxes* can contain other *boxes*. With all these *boxes* we can add buttons, cursors, menus and more.
\newline

For the second *soutenance*, we tried many different ways to make the GUI. First, we tried to add widgets in a *table*, this system was easy to manage but the look was worse than the first one. After, we used *layouts*. With *layouts* we could put widgets pixels by pixels. The GUI had a nice look but it wasn't resizable. Finally, we chose imbricated *tables*. Widgets were put in *tables* and these *tables* were in a cell of the main *table*. This system allows more flexibility and the GUI was resizable. The GUI also had a better look.
\newline

In the GUI, we found six buttons : four for the different effects, one for settings and one for the real-time system. The *Settings* button opened two windows : the *Equalizer* window with three cursors and the *Amplifier* window with a *gain* bar and a *noise* bar.
\newline

For this last *soutenance*, we use *table* and *boxes* to do this GUI. The menu is more simple, it contains only the essential. A *File* menu which contains a *Open* option and a *Quit* option. An *About* menu with the credit and the current version of the *StarDSP* project.
\newline

We have six *buttons*, one for each *effects* : *Amplifier*, *Equalizer*, *Echo*, *Reverberation*, *Balance* and *Stereo effect*. When a *button* is clicked a *label* appears in a *list*. This *label* contains the name of the selected *effect* and a number representing the number of the effect occurrences.
\newline

We can select an *effect* by clicking on its name in the *list* and remove it by clicking on the *Remove* button. We can also remove all the effects by clicking on the *Clear All* button.
\newline

If we select an *effect* and click on the *Setting* button a setting window appears. The windows are similar to the second *soutenance* settings windows.
There are four new settings windows.The *Echo* window  with two scales  one for the *delay* and one for the *decay*. The *Reverbe* window with only one scale. The *Balance* window with one horizontal scale to the left/right balance. The *Stereo effect* window with one scale. When an effect is deleted the associated window is also deleted.
\newline

The final version of our *GUI* is more elegant and is more professional.

