## Stereo

Stereo, or stereophonic sound, is a method of sound reproduction that creates an illusion of directionality and audible perspective. This is achieved by using two or more speakers.
For this last soutenance, I create two new effects based on the stereo.

### Manual

The first one is a balance: You can "displace" the sound, that means you can move the sound to the left speaker for example and it seams as if the sound comes from the left. We use this effect with a simple cursor that we move to the left or the right. If we move the cursor to the left the sound produces by the left speaker will be more powerfull than the right speaker and conversely.

### Automatic

The second one is an invention by me (may be I actually  don't know if that's already existing): The sound goes to the left then to the right like a "wave". The sound, like in the last effect, comes from left but it moves automaticaly (by itself) to the right and then to the left, etc... I use for that a cosinus that creates these waves. We give to the function a period T, more we increase T more the movement will be low. This effect is kind of funny to ear.


