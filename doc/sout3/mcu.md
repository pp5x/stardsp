### Le microcontrôleur

#### Le microcontrôleur `Atmega 8515`

##### Caractéristiques

Le microcontrôleur que nous utiliserons pour cette première soutenance sera l'`Atmega 8515` d'*Atmel*.

Ce dernier possède 35 ports d'entrées/sorties programmables (utilisés pour le contrôle des `LEDS`) et une interface `UART` pour communiquer par `RS-232`.

Ce composant est programmé à l'aide de la carte de développement `STK500` fabriquée par *Atmel*. Elle permet de d'injecter le code machine par *ICSP* (In Circuit Serial Programming) et de tester rapidement l'état des ports grâce à 8 `LEDS` et 8 switches.

##### Gestion des sorties parallèles

Les `LEDS` de la surface de contrôle sont gérées par les sorties parallèles du microcontrôleur.

Les ports du `8515` étant bidirectionnels, il est nécessaire de procéder à une courte phase de paramétrage de ces derniers avant de pouvoir les utiliser en tant que *sortie*.

##### Registres de contrôle

###### PORT B Data (`PORTB`)

Le registre `PORTB` permet de définir les valeurs des ports `PB0` à `PB7`.

Par exemple, si le bit 5 du registre `PORTB` est à 1, alors le port `PB5` sera une source de tension. Il sera relié à la masse sinon (*sink*).

###### PORT B Data Direction (`DDRB`)

Le registre `DDRB` permet, quant à lui, de définir la direction des ports `PB0` à `PB7`, càd de définir si un port est une entrée ou une sortie.

Par exemple, si le bit 3 du registre `DDRB` est à 1, alors le port `PB3` sera une sortie. Il sera une entrée sinon.

##### Utilisation des sorties parallèles en langage d'assembleur

La routine suivante place le port `PB0` en tant que source de tension :

    pb0_high:
                ldi r24, (1 << PB0)
                ldi r25, (1 << DDB0)
                out PORTB, r24
                out DDB0, r25
                ret

#### Changement de microcontrôleur

L'`Atmega 8515` a pour avantage d'être facilement programmé et testé avec le `STK500`. Cependant, ce microcontrôleur ne convient pas à une utilisation concrète. En effet, le nombre d'entrées/sorties est beaucoup trop limité pour notre surface. C'est pourquoi nous avons choisi de la contrôler grâce à un `Atmega 1280` interfacé sur une carte de prototypage rapide `DFRduino Mega`, basée sur l'`Arduino Mega 1280`.

#### Différence entre l'`Atmega 8515` et l'`Atmega 1280`

La principale différence entre l'`Atmega 8515` et l'`Atmega 1280` est le nombre de pins. Le `8515` en possède 40 (figure \ref{pin8515}), tandis que le `1280` en a 100 (figure \ref{pin1280}). Cela implique que ce dernier existe uniquement en `CMS` — Composant Monté en Surface.

Au niveau de la mémoire, le `1280` est équipé d'une mémoire flash de programme de 128Ko et de 8Ko de RAM, tandis que le `8515` ne possède qu'une flash de 8Ko et d'une RAM de 512 octets. Il est donc possible de développer des programmes plus imposants sur l'`Atmega1280`.

Le `1280` est aussi fournit avec un nombre plus important de périphériques internes — 3 interfaces `UART`, 8 canaux de `PWM`, ... Dans notre cas, une interface `UART` et quelques convertisseurs analogiques nous suffisent.

#### Avantage de l'interface `Arduino`

L'`Atmega 1280`, à cause d'une nombre important d'entrées/sorties, existe uniquement en `CMS`. C'est pourquoi nous utilisons une carte de prototypage rapide `DFRduino` pour contrôler notre surface.

Alimentée par `USB` avec une tension de 5V, elle permet d'accéder facilement à 54 pins bidirectionnelles du microcontrôleur et 16 entrées analogiques. Un crystal est aussi disponible pour cadencer le `1280` à 16MHz — contre 3.68MHz sur le `STK500`.

#### Communication par `USB` : le `FT232RL`

Le principal avantage de cette carte est de fournir une puce `FT232RL` (figure \ref{pinft232}) permettant de transmettre les informations envoyées par l'`UART` du microcontrôleur par `USB`.

Ainsi, les pins `PE0` (`TX0`) et `PE1` (`RX0`) du `1280` sont respectivement reliés aux pins `RXD` et `TXD` du `FT232RL`.

Grâce à ce système, nous pouvons utiliser la connexion `USB` comme s'il s'agissait d'une simple liaison série.

En ce qui concerne l'utilisateur, celui-ci préférera connecter la surface en `USB` plutôt qu'à l'aide d'un cable `RS-232` comme c'était le cas lors de la première soutenance.

Contrairement à ce qui a été présenté lors de la première soutenance, la communication entre la surface et le logiciel est maintenant bidirectionnelle. En plus de l'affichage de l'intensité du signal sonore traité par *StarDSP*, la surface permet de contrôler le logiciel. Pour l'instant, il est possible d'activer ou non le traitement du son à l'aide d'un bouton poussoir.

#### Programmation par ICSP

Pour pouvoir programmer le microcontrôleur par `USB`, les cartes *Arduino* sont fournis avec un *bootloader* qui, une fois installé sur le composant, lui permet d'écrire lui-même dans sa mémoire de programme. Cependant, ce dernier prend quelques secondes à s'executer avant de laisser la main au programme principal, ce qui allonge le temps de *reset* de la machine.

Pour résoudre ce problème, certe mineur, nous avons utilisé l'interface `ICSP` (In Circuit Serial Programming) du `STK500` pour injecter le programme dans le `1280`. Cela nous permet donc de nous passer de *bootloader*.

#### Sélection de l'horloge externe

C'est en programmant le `1280` par `ICSP` que nous nous sommes aperçu que ce dernier était cadencé à 1MHz au lieu de 16MHz comme le permet le crystal se trouvant sur la `DFRduino` — peut-être une erreur de configuration lors de la fabrication ?

Il est possible de configurer la source d'horloge du microcontrôleur grâce aux fusibles — *fuses* —  de ce dernier. Il s'agit de registres particuliers de 8 bits non-volatiles qui permettent de configurer le comportement global du composant.

Le `1280` en possède 3 : *Low*, *High* et *Extended*. Les trois bits de poids faibles du fusible *Low* (`CKSEL`) permettent de configurer la source d'horloge comme cela est décrit sur la figure \ref{fuses}.

Le but ici était donc de changer les fusibles pour que le microcontrôleur accepte un crystal entre s'est bornes `XTAL1` et `XTAL2` comme sur le schema \ref{lpco} et ainsi respecter le montage réel du `DFRduino` \ref{clockduino}.

Cependant, nous avons, par mégarde, placé les bits `CKSEL` à 0000, ce qui correspond à une source externe d'horloge. Ce n'est évidemment pas ce que nous recherchions.

Sachant que le microcontrôleur doit être cadencé pour pouvoir être programmé par `ICSP`, un problème s'est alors posé. 

#### Comment rétablir les bonnes valeurs des fusibles si ce dernier n'est plus cadencé ?

À ce moment, le microcontrôleur ne pouvait être cadencé uniquement grâce à un montage similaire à celui de la figure \ref{clockext}. Il fallait imposer un signal d'horloge sur le port `XTAL1` du `1280` et relier le `XTAL2` à la masse.

Le composant en question étant soudé en surface, il était impossible d'établir un contact entre un fil et ces ports : les pattes, étant côte à côte, sont bien trop petites.

Comme le montre la figure \ref{contact}, les soudures du crystal sont suffisamment grandes pour pouvoir les mettre en contact. Par chance, une d'entre elles est directement reliée au port `XTAL1` !

Maintenant que nous savions où nous pouvions injecter le signal d'horloge, il fallait encore réussir à trouver un moyen de générer ce fameux signal.

#### Comment obtenir un signal d'horloge permettant de cadencer le microcontrôleur ?

Ayant peu de matériel, la seule source de signal d'horloge dont nous disposions était celui du `STK500`.

Comme le montre la figure \ref{jumpers}, le `STK500` permet, à l'aide de jumpers, de choisir la source du signal d'horloge qui sera appliquée aux microcontrôleurs se trouvant sur les sockets de la carte. Ainsi, il est possible soit d'utiliser son propre crystal soit d'utiliser un signal émis par le microcontrôleur *Maitre* du `STK500` (figure \ref{clocksrc})

N'ayant pas de crystal, nous nous sommes servis du signal généré de 3.68MHz.

En mettant en contact la broche 1 du jumper `OSCSEL` du `STK500` avec la soudure du `DFRduino`, l'`Atmega 1280` était maintenant cadencé. Il suffisait ainsi de changer la valeur de fusibles grâce à l'`ICSP` tout en maintenant le contact entre les deux cartes.

Avec *avrdude*, il est facile de modifier les fusibles par `ICSP` grâce à cette commande :

    $ avrdude -p m1280 -c stk500 -P /dev/ttyUSB0 -e -U lfuse:w:0xff:m -U hfuse:w:0xda:m

En configurant correctement les fusibles pour que le microcontrôleur utilise le quartz externe, l’`Atmega1280` fonctionnait de nouveau !
