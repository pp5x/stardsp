## Égaliseur *3-Band*

La première fonctionnalité que nous avons ajouté est celle d’un *égaliseur* simple à trois bandes. Ceci permet de modifier le signal de manière à favoriser une partie du *spectre sonore*[^1]. Concrêtement, l’utilisateur peux choisir de favoriser les basses fréquences, les mediums ou bien les hautes fréquences à sa guise.

### Traitement du signal

#### Les séries de \textsc{Fourier}
Pour réaliser cet effet, il faut pouvoir décomposer notre signal. Une onde sonore est en réalité composée d’une somme de signaux.

\label{series}
\begin{figure}[htbp]
\centering
\includegraphics[width=15cm]{../img/fourier.pdf}
\caption{Séries de Joseph \textsc{Fourier}}
\end{figure}

Dans l’example de la figure \ref{series}, le signal *bleu* se décompose en une série de trois fonctions sinusoïdales.

\begin{displaymath}
f(x)=\cos \; x\; -\; \frac{1}{3}\cos \; 3x\; +\; \frac{1}{5}\cos \; 5x
\end{displaymath}

Ainsi, toute fonction continue sinusoïdale peut se décomposer en *série de Fourier*.


\begin{figure}
\begin{displaymath}
\sum_{n=0}^{+\infty}{\left( \mathcal{A} _{n}\sin \left( n\omega _{0}t \right)\; +\;\mathcal{B}_{n}\cos \left( n\omega _{0}t \right) \right)}
\end{displaymath}
\caption{Série de \textsc{Fourier}}
\end{figure}

Dans l’équation, on voit qu’il s’agit d’une combinaison linéaire de fonctions trigonométriques qui ont toutes des pulsations proportionnelles à la pulsation $\omega_{0}$ : cette pulsation fondamentale $\omega_{0}$ est la pulsation de la note jouée par une corde de guitare.

On obtient ainsi divers signaux avec des fréquences différentes. Si nous souhaitons augmenter les basses dans notre son, il faut augmenter l’amplitude des signaux basse fréquence et inversement pour les aïgus.

#### La transformée de \textsc{Fourier} discrète

Diviser pour régner est la stratégie utilisée ici. Le signal d’entrée est très complexe. Pour pouvoir le traiter, on se propose de le décomposer en somme sinusoïdes. C’est ainsi beaucoup plus simple pour faire les modifications souhaitées.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.4]{../img/dft.png}
\caption{Décomposition d’un signal et synthèse.}
\end{figure}

Un signal peut être continu ou discret, et périodique ou apériodique. Cela donne quatre catégories de transformée de \textsc{Fourier} :

- Apériodique, continue :
	Transformée de \textsc{Fourier}
- Périodique, continue :
	Série de \textsc{Fourier}
- Apériodique, discrète :
	Transformée de \textsc{Fourier} à temps discret
- Périodique, discrète :
	Transformée de Fourier discrète
	
La réalisation de la transformée de \textsc{Fourier} permet de réaliser un égaliseur paramétrique très précis. Cette tranformée est très pratique également en analyse du signal. Ne nous ne l’avons pas utilisée dans ce projet, nous utilisons la méthode par convolution.

#### Convolution

Une autre méthode très utilisée dans le traitement des signaux est la *convolution* également très connue dans le traitement d’image via les matrices de convolution.
\newline

La convolution est une méthode mathématique pour combiner deux signaux pour en former un troisième. C’est l’une des techniques incontournable dans le traitement des signaux. La convolution prend en compte trois signaux : le signal d’*entrée*, le signal de *sortie* et la *réponse impulsionnelle*.
\newline

La *réponse impulsionnelle* est obtenue lorsque l’entrée est une impulsion, c’est-à-dire une variation soudaine et brève du signal. Dans un système à temps continu, le modèle mathématique d’une impulsion est une distribution de \textsc{Dirac}. Pour un système à temps discret, une impulsion est définie par la suite $\delta(n)$ valant $1$ si $n = 0$ et $0$ sinon. Cette fonction est aussi appelée l’*impulsion unitaire*. On peut donc visualiser notre signal comme un groupement d’impulsions.
\newline

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.4]{../img/delta_impulse.png}
\caption{\label{delta}Définition de la fonction \emph{delta} et de la \emph{réponse impulsionnelle}.}
\end{figure}

Dans la figure \ref{delta}, $\delta[n]$ est la fonction *delta*. La *réponse impulsionnelle* d’un système linéaire, dénotée par $h[n]$, est la sortie du système lorsque l’entrée est la fonction *delta*.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.3]{../img/input_impulse.png}
\caption{\label{input}Le signal d’entrée est convolutionné par la \emph{réponse impulsionnelle} du système.}
\end{figure}

Le système \ref{input} est représenté par l’équation suivante :
\begin{displaymath}
x[n] \ast h[n] = y[n]
\end{displaymath}

La *convolution* est représentée par le signe $\ast$. Le signal d’entré est convolutionné par la *réponse impulsionnelle* et produit le signal de sortie.

\begin{figure}[!hb]
\centering
\includegraphics[scale=0.4]{../img/lowpass_filter.png}
\caption{Filtre passe-bas obtenu par convolution.}
\end{figure}
 
### Implémentation

Au niveau de l’implémentation, on utilise le principe de convolution évoqué dans la partie d’avant. On défini une structure, pour stocker les nouvelles valeurs — ainsi que les anciennes — et faire les calculs. Il s’agit d’une implémentation très efficace.

On calcule donc avant toute chose, les valeurs des fréquence de *coupure* des filtres *passe-bas* et *passe-haut*, stockée dans `lf` et `hf`. Les valeurs de gain à appliquer sont stockée dans `lg`, `mg`, et `hg` correspondant respectivement aux gains des filtres *passe-bas*, *passe-haut* et *passe-bande*. Les champs *type* `f1p0`, etc ... représentent la *réponse impulsionnelle*. Il faut voir la réponse impulsionnelle comme un effet d’inertie — les calculs précédents ont une influence sur les prochains.

~~~{.ocaml}
type state =
{
  mutable lf	: float;
  mutable f1p0	: float;
  mutable f1p1	: float;
  mutable f1p2	: float;
  mutable f1p3	: float;

  mutable hf	: float;
  mutable f2p0	: float;
  mutable f2p1	: float;
  mutable f2p2	: float;
  mutable f2p3	: float;

  mutable sdm1	: float;
  mutable sdm2	: float;
  mutable sdm3	: float;

  mutable lg	: float;
  mutable mg	: float;
  mutable hg	: float;
}
~~~

On initialise notre structure avec les valeurs des fréquences des filtres passe-bas et passe-haut — on calcule les fréquences de coupure des filtres.

~~~{.ocaml}
let init lowfreq highfreq mixfreq =
  let pi = 4. *. atan 1. in
  let es =
    {
      lf	= 2. *. sin(pi *. (lowfreq /. mixfreq));
      f1p0	= 0.;
      f1p1	= 0.;
      f1p2	= 0.;
      f1p3	= 0.;

      hf	= 2. *. sin(pi *. (highfreq /. mixfreq));
      f2p0	= 0.;
      f2p1	= 0.;
      f2p2	= 0.;
      f2p3	= 0.;

      sdm1	= 0.;
      sdm2	= 0.;
      sdm3	= 0.;

      lg	= 0.1;
      mg	= 0.5;
      hg	= 1.;
    }
  in
  es
~~~

On réalise via cette fonction l’égalisation, selon le principe de convolution. La petite valeur `vsa` permet de corriger les erreurs d’arrondi. La valeur du nouveau *sample* sera la somme de ce que chaque filtre aura calculé. Le filtre *passe-bande* est obtenu par différence des deux filtres *passe-bas* et *passe-haut*.

~~~{.ocaml}
let compute es sample =
  let vsa = 1. /. 4294967295. in
  es.f1p0 <- es.f1p0 +. (es.lf *. (sample -. es.f1p0)) +. vsa;
  es.f1p1 <- es.f1p1 +. (es.lf *. (es.f1p0 -. es.f1p1));
  es.f1p2 <- es.f1p2 +. (es.lf *. (es.f1p1 -. es.f1p2));
  es.f1p3 <- es.f1p3 +. (es.lf *. (es.f1p2 -. es.f1p3));
  let l = es.f1p3 in
  es.f2p0 <- es.f2p0 +. (es.hf *. (sample -. es.f2p0)) +. vsa;
  es.f2p1 <- es.f2p1 +. (es.hf *. (es.f2p0 -. es.f2p1));
  es.f2p2 <- es.f2p2 +. (es.hf *. (es.f2p1 -. es.f2p2));
  es.f2p3 <- es.f2p3 +. (es.hf *. (es.f2p2 -. es.f2p3));
  let h = es.sdm3 -. es.f2p3 in
  let m = sample -. (h +. l) in
  let l = l *. es.lg in
  let m = m *. es.mg in
  let h = h *. es.hg in
  es.sdm3 <- es.sdm2;
  es.sdm2 <- es.sdm1;
  es.sdm1 <- sample;
  l +. m +. h
~~~

[^1]: L’ensemble des fréquences harmoniques ou inharmoniques.