# Site web

Le site internet est le principal support d'information de notre projet. Il doit rendre le projet attractif. Nous avons optés pour un aspect *multimédia* et *didactique*.

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{../img/website1.png}
\caption{\emph{Design} du site internet}
\end{figure}

## Conception du site web

Le projet StarDSP, se classant dans la catégorie des utilitaires, nous avons opté pour un style très épuré. Le site web est un élément important. Il s’agit du premier contact avec l’utilisateur. Nous avons donc décidé d’aborder une interface très claire et simple d’utilisation. Il est a noter que le design *CSS* a été fait de nos propres mains !

Le site internet est structuré de la façon suivante :

* Une page d’accueil allant à l’essentiel.
* Une section de *présentation détaillée*.
* Une section *documentation*.
* Une section *téléchargement*.
* Une section *à propos*.

La page d’accueil permet de montrer en quelques coups d’oeil les différentes fonctionnalités qu’offre *StarDSP*. Nous y avons mis une vidéo montrant un guitariste en train d’utiliser *StarDSP* comme amplificateur.

La section *téléchargement* permet de distribuer rapidement notre logiciel et nos rapports de soutenance. La section *documentation* permet quant à elle d’expliquer en détails le fonctionnement de *StarDSP* et préciser les solutions aux eventuels problèmes. La présentation *détaillée* présente globalement le logiciel mais avec beaucoup plus d’explication et de détails par rapport à la page d’accueil qui se limite à la présentation des points forts. Cette section contient d’example de son obtenu avec le logiciel et des images montrant l’interface graphique et la surface de contrôle.

La section *à propos* permet quant à elle de présenter le travail effectué par chaque membres de l’équipe, ainsi que des liens vers des projets personnels. On notera également que le site est désormais rédigé exclusivement en anglais pour pouvoir attirer un public plus large.

## Hébergement

Le projet commence à prendre de l’ampleur. C’est pourquoi nous hébergeons le site sur un serveur. Le moteur de template de Pierre S. a permis de réaliser les bases du site internet très rapidement. Mais pour pouvoir héberger facilement le site internet, nous avons dû générer toutes les pages en html — seulement le contenu est à modifier. \newline

Retrouvez le site internet à l’adresse suivante : \url{http://star-dsp.epimeros.org}.