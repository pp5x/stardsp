# CLI \& GUI

## Command Line Interface

*CLI* ou bien *Command Line Interface* permet à l’utilisateur de se passer d’une interface graphique.

\begin{verbatim}
$ ./stardsp —help
Usage : ./stardsp [OPTIONS]
Input : Realtime or AIFF
	-core						Run core only
	-file <path>				AIFF file input
	-surface <device>			TTY
	-ssurface <data>			test Control-surface
	-eq <lg,mg,hg>				Use Equalizer
	-amp <drive,noise>			Use Amplifier
	-rev <modulation>			Use Reverberation
	-echo <delay,decay>			Use Echo
	-stereo <modulation>		Use Stereo effect
	-balance <left/right>		Stereo Balance
	-default					Surface preset
\end{verbatim}

Les options qui sont présentes permettent à l’utilisateur de choisir le mode d’execution de StarDSP et des options :

- Utiliser un fichier.
- Ajouter les effets qu’il souhaite appliquer — l’ordre d’ajout est respecté.
- Utiliser la surface de contrôle.
- Utiliser l’interface graphique — par défaut.
- Utiliser que le *core* du programme.

Cette interface textuelle se réalise facilement avec le module `Arg` d’`OCaml`. Celle-ci a été indispensable tout le long du développement de StarDSP.