\newpage

#### Répartition des tâches

* Pierre \textsc{Surply}
	* PulseAudio *binding*
	* Surface de contrôle
	* Mara
	* Moteur de template en *CGI*
	* Initiatialisation de la communication par *pipe*
* Pierre \textsc{Pagnoux}
	* Lecture des fichiers audio — *binding*
	* Moteur audio
	* Gestions des effets
	* Effet d’amplification
	* Égaliseur *3-Band*
	* Effet d’écho
	* GUI
	* Communication par *pipe*
	* Site internet
	* Retouche d’images et montage vidéo
* Alexandra \textsc{Pizzini}
	* GUI
* Vincent \textsc{Molinié}
	* Effet de réverbération
	* Effet stéréo
	* Contrôle de la balance (L/R)