## Fichiers audio

Au cours de la phase de développement de ce projet, nous avions besoin de régulierement tester notre travail. C'est pourquoi, nous avons ajouté la prise en charge de la lecture de fichiers audio. Pour cela, nous reprenons une bibliothèque écrite en `C` : `libsndfile`. Celle-ci nous a permis de nous simplifier la tâche, en ce qui concerne la lecture de multiples formats de fichier, étant donné que cela n'est pas le but principal de notre projet.\newline

La bibliothèque nous offre beaucoup de souplesse. Elle s’occupe des différentes conversions à effectuer.
\newline

Nous avons réalisé un *binding* de cette bibliothèque. Cela consiste à traduire les fonctions de cette bibliothèque en `OCaml`. Nous obtenons au final des fonctions simples :

~~~ {.ocaml}
val openf			: string -> t
val closef			: t -> unit
val readf			: t -> (float, Bigarray.float32_elt, Bigarray.c_layout)
	Bigarray.Array1.t -> int -> int
~~~

### Liste des formats de fichier pris en charge par *StarDSP*
* Microsoft `WAV`
* Apple `AIFF/AIFC`
* Format `RAW`
* Paris Audio File `PAF`
* Apple `CAF`
* Free Lossless Audio Codec `FLAC`
* Soundforge `W64`


On peux facilement remarquer que la lecture des fichiers audio consiste à remplir un *buffer* de nombres flottants — representants les valeurs discrètes du signal sonore numérisé. On appelle communément ces valeurs *sample*.