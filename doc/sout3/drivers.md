## Programmation des pilotes

Pour ce projet, il a été nécessaire de coder entièrement les pilotes de chaques composants électronique que nous utilisons en *Mara*. En effet, ce langage étant très jeune, les pilotes d'`UART`, de `C`onvertisseur `A`nalogique vers `N`umérique et d'afficheur `LCD` ont été programmés spécialement pour l'occasion.

### UART

L'`UART` (Universal Asynchronous Serial Receiver and Transmitter) permet au `8515` de communiquer à travers une liaison série asynchrone.

Dans notre cas, elle permet de recevoir les différentes valeurs d'intensité du signal sonore traité.

#### Format de trame

Pour la surface de contrôle, nous transmettrons des données codées sur 8 bits, envoyées sans bit de parité et en utilisant uniquement un seul bit de d'arrêt.

#### Registres de contrôle

##### UART Data Register (`UDR`)

Que ce soit pour la transmission ou pour la réception, le registre `UDR` servira de registre de donnée. Une donnée à envoyée sera au préalable chargée dans ce registre, tandis qu'une donnée reçue pourra simplement être lu à l'emplacement mémoire de ce dernier.

##### UART Baud Rate Register (`UBRRH:UBRRL`)

Le registre `UBBR` défini la vitesse de transmission des données. Sa valeur peut être calculée en fonction de la fréquence d'oscillation de l'horloge du microcontrôleur et de la vitesse de transmission souhaitée ainsi :

$$ UBBR = \frac{f_{osc}}{BAUD \times 16} - 1$$

$BAUD$ : Vitesse de transmission en $Baud$

$f_{osc}$ : Fréquence d'oscillation de l'horloge du MCU en $Hz$

Sachant que le `STK500` cadence le `1280` à $16 MHz$ et que l'on souhaite communiquer à une vitesse de $9600 Baud$, on a donc

$$ UBBR = \frac{16 000 000}{9600 \times 16} - 1 = 104$$

##### UART Control Status Register (`UCSRA:UCSRB:UCSRC`)

La programmation des différents modes de fonctionnement de l'`UART` est, quant à elle, définie par les registres de contrôle `UCSRA:UCSRB:UCSRC`.

Selon la documentation de constructeur, il est nécessaire de placer les bits `UCSZ2:UCSZ1:UCSZ0` du registre `UCSRC` à $011$ pour un format de trame de 8 bits avec 1 bit de stop.

#### Configuration en langage d'assembleur

On suppose que la nouvelle valeur du `BAUD RATE REGISTER` soit dans les registres `r25:r24`.

    uart_init:
                ; Initialisation du BAUD RATE REGISTER
                out UBRRL, r24
                out UBRRH, r25
                
                ; Activation de la transmission et de la reception
                ldi r24, (1 << TXEN) | (1 << RXEN)
                out UCSRB, r24
                
                ; Configuration du 'Frame format' : 8 bits et 1 bit de stop
                ldi r24, (1 << URSEL) | (3 << UCSZ0)
                out UCSRC, r24
                
                ret
    
#### Réception en langage d'assembleur

La routine ci-dessous place la donnée reçue par `UART` dans le registre `r24` :

    uart_recv:
                sbis UCSRA, RXD
                rjmp uart_recv
                in   r24, UDR
                ret


### Afficheur `LCD`

L'afficheur `LCD` que nous avonc choisi de placer sur notre surface est basé sur le `HD44780`. Ce microcontrôleur standard réalisé par *Hitachi* permet de piloter un dispositif d'affichage par cristaux liquides. Il peut afficher les caractères `ASCII` et `Kana` sur 2 lignes de 16 caractères.

Le code `ASCII` des caractères à afficher se trouvent dans une DDRAM accessible en lecture et en écriture. Un compteur d'adresse représente la position du curseur à l'écran : il s'incrémente automatiquement lorsqu'un caractère est imprimé.

#### Rôle des broches

* `Vss` : Masse (0V)
* `Vdd` : Tension d'alimentation (5V)
* `V0` : Reglage du contraste (Maximal lorsque relié à la masse)
* `RS` (`R`egister `S`elect)
    - 0 : Sélection du registre d'instruction
    - 1 : Sélection du registre de donnée
* `R/W` (`R`ead/`W`rite)
    - 0 : Écriture
    - 1 : Lecture
* `E` (`E`nable)
* `DB0:DB7` : Bus de données

#### Initialisation de l'écran

L'afficheur peut être utilisé en mode 4 bits, cela signifie qu'il est possible de relier un bus de données de 4 fils seulement au lieu de 8. Pour pouvoir utiliser ce mode, il est nécessaire d'initialiser l'afficheur d'une certaine manière (voir annexe \ref{initlcd}). En mode 4 bits, les 8 bits nécessaire à l'encodage des instructions sont envoyés séquentiellement sur les broches 4 à 7.

#### Description des commandes

##### Effacer l'écran

Cette instruction écrit le caractère 20H (espace) sur l'intégralité de la DRAM.

- `RS` : 0
- `RW` : 0
- `DB7` : 0
- `DB6` : 0
- `DB5` : 0
- `DB4` : 0
- `DB3` : 0
- `DB2` : 0
- `DB1` : 0
- `DB0` : 1

##### Retour

Cette instruction place le curseur dans sa position d'origine (0, 0).

- `RS` : 0
- `RW` : 0
- `DB7` : 0
- `DB6` : 0
- `DB5` : 0
- `DB4` : 0
- `DB3` : 0
- `DB2` : 0
- `DB1` : 1
- `DB0` : X

##### Sélection de mode

Permet de paramètrer la direction du curseur et de l'affichage.

* `RS` : 0
* `RW` : 0
* `DB7` : 0
* `DB6` : 0
* `DB5` : 0
* `DB4` : 0
* `DB3` : 0
* `DB2` : 0
* `DB1` :
    - 0 : Se déplacer vers la gauche
    - 1 : Se déplacer vers la droite
* `DB0` : Faire défiler l'affichage entier

##### Positionnement manuel du curseur

Cette instruction permet de définir une adresse de DDRAM dans le compteur d'adresse. Cela permet de déplacer manuellement le curseur à une position donnée.

- `RS` : 0
- `RW` : 0
- `DB7` : 1
- `DB6` : `AC6`
- `DB5` : `AC5`
- `DB4` : `AC4`
- `DB3` : `AC3`
- `DB2` : `AC2`
- `DB1` : `AC1`
- `DB0` : `AC0`

Où `AC6:AC0` est la valeur que l'on souhaite affecter au compteur d'adresse.

##### Afficher un caractère sur l'écran

Cette instruction écrit dans la DDRAM un caractère à la position indiquée par le compteur d'adresse. Ce dernier est ensuite incrémenté.

- `RS` : 1
- `RW` : 0
- `DB7` : `D7`
- `DB6` : `D6`
- `DB5` : `D5`
- `DB4` : `D4`
- `DB3` : `D3`
- `DB2` : `D2`
- `DB1` : `D1`
- `DB0` : `D0`

Où `D7:D0` est la valeur `ASCII` du caractère que l'on souhaite afficher.

### Convertisseur Analogique vers Numérique

Le microcontrôleur est muni d'un convertisseur analogique vers numérique à approximations successives. Ce périphérique est indispensable pour pouvoir exploiter les potentiomètres de la surface. \ref{can}

#### Utilisation du `CAN`

Le convertisseur de `Atmega 1280` est connecté à un multiplexeur analogique 16 canaux qui permet de prendre en charge 16 entrées de tensions mappé sur le Port `F` et `K` du composant. La tension se référe bien sûr à la masse (0V).

Le `CAN` convertit une tension analogique en une valeur codée sur 10 bits grâce à des approximations successives. La valeur minimale représente la masse (0V) et la maximale représente la tension de référence présente sur le pin `AREF` du microcontrôleur moins 1. Optionellement, le pin `AVCC` ou une tension interne (1.1V ou 2.56V) peut être connecté sur la broche `AREF`.

##### Le registre `ADMUX` (`ADC` `M`ultiplexer `S`election `R`egister)

Le registre `ADMUX` permet de contrôler le multiplexeur analogique du `CAN`.

- Les bits `7:6` permettent de sélectionner la tension de référence. Dans notre cas, nous utilisons la tension AVCC avec un condensateur externe (présent sur la carte `Atmega 1280`) : cela équivaut à la valeur 01.
- Le bit `5` permet d'aligner les 10 bits de résultat à gauche. Ici, nous les alignerons à droite.
- Les bits `4:0` permet de choisir le canal et le gain que le multiplexeur sélectionnera. Cette valeur dépend donc de l'entrée que l'on souhaite convertir.

##### Le registre `ADCSRA` (`ADC` `C`ontrol and `S`tatus `R`egister `A`)

Ce registre permet de définir le comportement global du `CAN` et de lancer une conversion.

- Le bit `7` permet d'activer le `CAN`.
- Le bit `6` permet de lancer une conversion
- Le bit `5` permet de lancer automatiquement une conversion lorsqu'un front montant est détecté.
- Le bit `4` est mis à 1 lorsqu'une conversion est terminée.
- Le bit `3` permet d'activer une interruption lorsqu'une conversion est terminée.
- Les bit `2:0` permettent de définir la prédivision qui sera appliquée entre signal d'horloge `XTAL` et l'horloge qui cadence le convertisseur. Ici, nous appliquerons un facteur de 128 (ce qui correspond à une valeur de 111).

##### Les registres `ADCL` et `ADCH` (`ADC` Data Register)

Ces registres contiennent le resultat de la conversion sur 10 bits.. Lorsque l'alignement est à gauche, le registre `ADCL` contient les 8 bits de poids faibles et le registre `ADCH` contient les 2 bits de poids forts.

##### Pilote du convertisseur en *Mara*

    func analog_read(channel, aref)
      @ADCSRB <- 0
      @ADMUX <- (aref bitls 6) bitor (channel bitand 0x07)
      @ADCSRA <- 0b11010111
      waitfor ((@ADCSRA bitrs 6) bitand 1)
      return asm "lds r24, ADCL
                  lds r25, ADCH"
    end

#### Problèmes rencontrés
\label{sec:pbcan}

La bonne manière de faire pour réaliser un tel circuit éléctronique aurait été d'utiliser des bobines et des condensateurs pour filtrer les écarts de tensions et ainsi diminuer le bruit.

En effet, après de nombreux essais, nous avons remarqué que les 4 bits de poids faibles du registre `ADCL` semblent prendre des valeurs complétement aléatoire. Nous sommes donc contraint de nous baser sur un résultat sur 6 bits. Nos potentiomètres peuvent donc seulement prendre des valeurs entre 0 et 63 : cela est cependant suffisant pour notre surface mais aurait pû poser plus de problèmes sur une utilisation plus "sensible".
