\chapter*{Conclusion}

Le développement de *StarDSP* touche maintenant à sa fin. Le rendu final correspond à nos attentes et répond parfaitement à la problématique posée dans le cahier des charges.\newline

Ce projet à été l’occasion d’apprendre énormément dans plusieurs domaines de l’informatique. Nous avons au cours du développement rencontré des problèmes et ceux-ci nous ont incités à nous renseigner d’avantage. Les tâches ont été très diverses et ont demandées beaucoup de temps personnel pour leurs réalisations.
\newline

Nous espèrons que vous aurez eu beaucoup de plaisir à lire ce rapport de projet.

#### Statistiques

Lignes de code par type de fichier :

\begin{center}
\begin{tabular}{|c|c|c|}
	\hline
   C & OCaml & Mara \\
   \hline
   268 & 3088 & 444 \\
   \hline
\end{tabular}
\end{center}

La barre psychologique des 4000 lignes de code est pratiquement atteinte !