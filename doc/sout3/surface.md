# Surface de contrôle

Pour cette dernière soutenance, nous sommes en mesure de présenter une surface de contrôle opérationnelle permettant de contrôler le logiciel *StarDSP*.

Cette surface, que nous avons entièrement fabriquée nous-même, se connecte par *USB* à la machine : cela lui permet d'être alimenté et de communiquer avec le logiciel. Elle est contrôlée par in microcontrôleur `Atmega 1280` entrièrement programmé em *Mara*, un langage dédié aux *AVR*. Les cinq potentiomètres permettent de controler les différents effets du logiciel, le bouton poussoir permet de mettre en pause le traitement et l'écran `LCD` permet d'afficher le titre du fichier traité, la valeur des différents effets et une animation basée sur l'intensité du signal sonore traité en temps réel.

- Blanc : Drive
- Jaune : Stereo
- Bleu : Delay
- Rouge : Decay
- Vert : Reverberation

