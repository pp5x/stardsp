## Matériel utilisé

### Nomenclature

#### Le bouton poussoir

\begin{figure}[!h]
\centering
\includegraphics{../img/nom_bp.jpg}
\end{figure}

- Désignation : BP miniature éco PM300N
- Quantité : 1

##### Description

Bouton-poussoir momentané noir à 1 contact travail.

- Pouvoir de coupure: 0,3 A/50 Vac. 
- Sorties: cosses à souder. 
- Perçage: 7.3 mm. 
- Hauteur: 27 mm.

#### Les potentiomètres

\begin{figure}[!h]
\centering
\includegraphics{../img/nom_pot.jpg}
\end{figure}

- Désignation : Potentiomètre linéaire 10k$\Omega$.
- Quantité : 5

##### Description

Potentiomètre mono linéaire en version économique à axe plastique 6 mm.

- Valeur: 10 k$\Omega$
- Puissance: 0,1 W
- Perçage: 10 mm 
- Dimensions du corps: 16 x 18 mm

#### Les Boutons pour potentiomètre

\begin{figure}[!h]
\centering
\includegraphics[scale=0.7]{../img/nom_boutons.jpg}
\end{figure}

##### Description

Boutons en plastique pour axe de 6 mm.
Fixation en force sur un axe de 6 mm.

- Dimensions: Diamètre 14 x 19 mm
- Couleur: Vert, bleu, jaune, rouge et blanc.

#### Le boitier

\begin{figure}[!h]
\centering
\includegraphics{../img/nom_boitier.jpg}
\end{figure}

- Désignation : Boîtier Europult 115P
- Quantité : 1

##### Description

Boîtier Teko série Europult en plastique ABS gris munis de guides internes pour CI, avec panneau supérieur incliné à 15 degrès.

Dimensions extérieures: 160 x 95 x 45 (64) mm

#### Le shield à borniers pour Arduino Mega 1280

\begin{figure}[!h]
\centering
\includegraphics{../img/nom_bornier.jpg}
\end{figure}

- Désignation : Shield à borniers DFR0060
- Quantité : 1

##### Description

Ce shield à borniers permet d'accéder à toutes les entrées/sorties d'une carte compatible Arduino sur bornier à vis. Le module est constitué de deux parties et se connecte directement sur une carte compatible Arduino.

- Dimensions: 66 x 21 x 19 mm pour chaque partie

#### L'afficheur `LCD`

\begin{figure}[!h]
\centering
\includegraphics{../img/nom_lcd.jpg}
\end{figure}

- Désignation : Afficheur LCD1602LC
- Quantité : 1

##### Description

Afficheur alphanumérique 2 x 16 caractères à driver et générateur de caractères incorporés sans rétroéclairage.

- Dimension des caractères: 5,55 x 2,95 mm
- Dimensions de l'afficheur: 80 x 36 x 9,5 mm
