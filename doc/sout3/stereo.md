## Stéréo

### Explication

Le son stéréophonique, ou plus communément le stéréo, est une méthode de reproduction du son qui créée une illusion de profondeur, de son directionnel. Pour celà, on utilise deux canaux audio indépendants (ou plus) à travers deux haut-parleurs ou plus de manière à créer l'impression d'un son provenant de diverses directions, comme dans l'audition naturelle.

Pour cette dernière soutenance, nous avons ajouté deux effets basé sur la stéréophonie. Une balance audio manuelle et une automatique.

### Implémentation

Tout d'abord, il faut comprendre que lorque l'on créé un *buffer* toutes les *samples* qui seront à un emplacement pair dans le buffer seront des *samples* joués par le haut-parleur de gauche et les impairs par celui de droite.

Pour qu'un son semble provenir de droite il suffit d'avoir une amplification linéaire plus forte à droite qu'à gauche.

#### Balance audio manuelle

Une balance est un réglage double, c'est-à-dire que lorque l'on va diminuer le son d'un côté, il va augmenter de l'autre, donnant donc l'impression de direction. Cette effet est très simple et peut être très utile selon ce que l'on veut faire percevoir.
Il nous faut donc juste un nombre `n` que l'on incrémente à chaque fois. Cela nous permet de savoir si nous sommes positionné sur l’enceinte de droite ou de gauche.

Nous avons rajouté au passage un autre effet avec la balance qui permet d'augmenter l'amplification d'une seule enceinte sans modifier l'autre.

#### Balance audio automatique

Cet effet reprend le principe de la balance manuelle mais le son va provenir de droite puis de gauche puis a nouveau de droite par vague. Il va faire balancer le son de gauche a droite automatiquement.
Pour cela on utilise une fonction sinusoïdale qui va permettre cette sensation de *vague*.
Cet effet est très agréable à écouter, très facilement audible et nécessite deux haut-parleurs. Sur des périodes très courtes le son d’une guitare est semblable au son produit par un orgue. On produit de cette manière un effet *chorus*.