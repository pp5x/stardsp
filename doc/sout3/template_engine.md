## CGI et moteur de template

Pour la réalisation du site web, j'ai un réalisé un `CGI` en `OCaml` pour assurer la partie dynamique du site. Son principal intêret réside dans le moteur de template qu'il intègre.

Ce dernier est destiné à traiter des templates `HTML` (situés dans le dossier `/website/templates/`) et de les liés avec du contenu dynamique afin de produire une page web.

### Syntaxe

J'ai défini la syntaxe des templates ainsi :

* `[begin` *sect* `]` *html code* `[end]` : délimite une section de code html
* `{` *sect* `}` : Inclut au template le contenu de la section *sect* délimitée précédemment
* `{%` *fun* `%}` : Inclut le resultat de la fonction `OCaml` *fun*. Elle doit être déclarée dans le fichier `/website/cgi-bin/cgi_functions.ml`
* `{@` *temp* `@}` : Indique au moteur de template que le template courant est inclus dans le template *temp*

Une fois que le traitement est terminé, le contenu de la section `base` est envoyé au client grâce au serveur web (Nous utilisons `Apache` dans le cadre du projet).


### `URL` dispatcher

L'`URL` entrée par le client sera divisée par le `CGI` de cette manière :

  http://domain?arg0/arg1/arg2/...

`arg0` represente le nom de la page demandée par l'utilisateur.
Il est possible de liéer `arg0` à un template dans le fichier `/website/cgi-bin/url.ml`.
Les autres arguments peuvent être récupérés avec la fonctions `Env.get_url_arg n`.

### Exemples

#### base.html

~~~html
[begin base]
<!DOCTYPE html>
<html>
  <head>
   <title>OCaml-OCR -{ title }</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
    <nav>
      <ul>
	<li><a href="?index/">Home</a></li>
	<li><a href="?about/">About</a></li>
      </ul>
    </nav>
    { body }
  </body>
</html>
[end]
~~~

#### templates/index.html

~~~html
{@ base.html @}

[begin title] Index [end]

[begin body]
<h1>Hello World ! </h1>
<p>{% get_time %}</p>
[end]
~~~

#### templates/about.html

~~~html
{@ base.html @}

[begin title] About [end]

[begin body]
<h1>About</h1>
[end]
~~~

#### cgi-bin/url.ml

~~~ocaml
let get_page_from_url () =
  match Env.get_url_arg 0 with
    | "about" -> "about.html"
    | _ -> "index.html"
~~~

#### cgi-bin/cgi_functions.ml

~~~ocaml
let get_function = function
  | "get_time" -> Date.get_date ()
  | f -> "Cannot find function : " ^ f
~~~
