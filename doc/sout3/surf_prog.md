## Comportement de la surface

### Protocol de communication

#### Connexion avec le logiciel

Lorsque le logiciel cherche à se connecter à la surface, celui-ci envoie l'octet `0b00101010` à travers la connexion série. Si la surface répond la même chose, alors la connexion s'établie. Dans le cas contraire, le programme s'arrête avec un message d'erreur.

Une fois la connxion établie, le logiciel envoie le titre du fichier traité sans extension ni chemin d'accès. La fin du titre est marquée par un caractère nul. À cause de la taille de l'écran, la taille du titre est limitée à 16 caractères.

#### Actualisation des données

Lors du traitement du signal sonore, le logiciel *StarDSP* envoie régulierement un octet décrivant l'état du programme. Il est découpé de cette manière :

* Bit `4` :
    - 0 : Le signal est en train d'être traité
    - 1 : Le traitement est mis en pause
* Bits `3:0` : Cette valeur représente l'intensité du signal sonore traité à cet instant. Elle est comprise entre 0 et 7.

Une fois cet octet reçu, la surface répond en indiquant un évenement qui vient de se produire sur ses entrées. Un premier octet est alors envoyé au logiciel pour indiquer le type d'évenement :

* `00000000` : Cette instruction indique qu'aucun évenement ne s'est produit.
* `00000001` : Le bouton poussoir vient d'être pressé
* `000XXX10` : Indique que le potentiomètre XXX vient de changer de valeur.

Lorsqu'un potentiomètre vient de changer de valeur, un second octet est alors envoyé pour indiquer la nouvelle valeur de ce dernier. Cette valeur est comprise en 0 et 63 (voir \ref{sec:pbcan}).

### Affichage

Un grand soin a été apporté sur l'affichage de la surface : c'est par ce biais qu'elle communique avec l'utilisateur.

Lorsque le périphérique est brancher par `USB`, cet écran est affiché :

    *** StarDSP  ***
                   /

Cela signifie que la surface est en attente de connexion avec le logiciel *StarDSP* : elle attend de recevoir le caractère `0b00101010`.

Essayons de lancer le logiciel de cette manière :

    $ stardsp -core -surface /dev/ttyUSB0 -file ~/Music/titre_du_fichier.aiff -default
    
La surface affichera alors :

    titre_du_fichier
    =====      =====

Lorsque le bouton poussoir est pressé :

    titre_du_fichier
    ==== PAUSE  ====

Et enfin, lorsque le potentiomètre blanc est tourné :

    titre_du_fichier
    Drive       42 %

