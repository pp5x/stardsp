## Echo

### Explication

Cet effet est très répendu en musique. Il est semblable à l’écho que nous pouvons reproduire dans les régions montagneuse. Le son produit se propage jusqu’à ce que celui-ci rencontre un obstacle — la montagne — et est renvoyé dans la direction opposée avec une légère atténuation due à l’élasticité du milieu. On peut donc entendre plusieurs fois le même son à interval régulier jusqu’à ce qu’il devienne inaudible à cause de l’atténuation.
	
### Implémentation

Il faut conserver le son pendant un certain temps et le restituer en sortie. Cela se traduit par la création d’un *buffer circulaire*.

#### Notion de temps

Le temps est représenté par nos *buffer*. La valeur d’échantillonage — par défaut $44,100$ KHz — signifie que la carte son vas recueillir $44,100$ mille échantillons en *une seconde*. Le buffer principal de notre application de taille $44,100$K est donc modifié une fois par seconde. En une seconde, le processeur est suffisamment rapide pour pouvoir appliquer tout les effets demandés. Pour conserver une valeur pendant 4 secondes il suffit de stocker celle-ci dans un buffer de taille $4 * f$, $f$ étant la fréquence d’échantillonnage.

#### Echo

Nous stockons le *sample* d’entrée en additionnant l’ancienne valeur multipliée par le *coefficient d’atténuation*. D’une manière générale, l’opération d’un délais est décrite comme :

\begin{figure}[htpb]
\centering
delayed($n$) = $s(n - d)$
\end{figure}

où $s$ représente le tableau d’entrée, $n$ le *sample* courant et $d$ le décalage — en quantité de *sample*.

L’implémentation d’un filtre d’écho en `C` :

\begin{verbatim}
	for (n = 0; n < N; n++)
	{
		out[n] = (in[n] + delayed) * 0.5; /* filter equation */
		delayed = in[n]; / * keep previous input */
	}
\end{verbatim}