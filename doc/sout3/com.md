# Communication par *pipe*

Le fait d’avoir une interface graphique nous a obligé à découper notre projet en 3 grandes parties : le *core*, la *GUI* et la surface de contrôle. En effet, une interface graphique intégrée directement au *core* poserait beaucoup de problème à cause de son action bloquante lorsqu’elle attend un évènement. Le traitement du signal devait se faire en simultané.

Au final, la surface de contrôle n’a pas nécessité un thread séparé pour son utilisation. Nous avons donc séparé le *core* et la *gui*. Nous utilisons les avantages d’un système *Unix* à notre profit.

## Fork \& Pipe

Le fork permet de créer un processus *fils*. La fonction de *fork* permet la séparation du processus *core*, celle-ci est gérée automatiquement. Le *PID* du nouveau processus est renvoyé est permet de différentier le traitement du processus père et fils.

~~~{.ocaml}
val fork : unit -> int
(** Fork a new process. The returned integer is 0 for the child process,
the pid of the child process for the parent process. *)
~~~

L’utilisation des *pipes* permet de transmettre un flux de données d’un processus à l’autre — utilise un système de *file descriptor*. La transmission des données unidirectionnelle : de la *GUI* vers le *core*. Leur utilisation nécessite la conception d’un *protocol de communication*.

~~~{.ocaml}
val pipe : unit -> file_descr * file_descr
(** Create a pipe. The first component of the result is opened for reading,
that's the exit to the pipe. The second component is opened for writing,
that's the entrance to the pipe. *)
~~~

#### Utilisation dans *StarDSP*

~~~{.ocaml}
let fromgui, tocore = Unix.pipe() in
Printf.printf "StarDSP%!\n";
let pid_gui = Unix.fork () in
if pid_gui = 0 then
	begin
    	Unix.close fromgui;
        Gui.main tocore
	end
else
	begin
    	Unix.close tocore;
        Sys.set_signal Sys.sigint
        	(Sys.Signal_handle (fun _ ->
              let _ = Unix.wait () in
              Printf.printf "Bye !\n";
              exit 0));
        Printf.printf "PID GUI : %d\n%!" pid_gui;
        Unix.set_nonblock fromgui;
        core (Some fromgui)
	end
~~~

Le processus père s’arrête lorsqu’il reçoit un signal système de la part de la *GUI*.

## Protocol de communication


### Les ordres

Les ordres permettent de différentiers les traitements de renvoyer les valeurs demandées.

~~~{.ocaml}
type order =
  | Add         of char
  | Rm          of (char * int)
  | Amp_drive   of int
  | Amp_noise   of int
  | Eq          of (int * int * int)
  | Echo_delay  of int
  | Echo_decay  of int
  | Rev         of int
  | Balance     of int
  | Stereo      of int
  | Process
  | File        of string
  | Realtime
  | Quit
~~~

À chaque ordre est associé un caractère — la communication dans le pipe s’effectue par *octet*.

~~~{.ocaml}
let order_to_char = function
  | Add _               -> 'a'
  | Rm _                -> 'b'
  | Amp_drive _         -> 's'
  | Amp_noise _         -> 't'
  | Eq _                -> 'u'
  | Echo_delay _        -> 'v'
  | Echo_decay _        -> 'w'
  | Rev _               -> 'x'
  | Balance _           -> 'y'
  | Stereo _            -> 'z'
  | Process             -> 'p'
  | File _              -> 'o'
  | Realtime            -> 'r'
  | Quit                -> 'q'
~~~

\newpage

### Structure de l’envois d’un ordre
\begin{center}
\begin{tabular}{|c|c|c|}
	\hline
   Ordre & Indication de position & Données \\
   \hline
\end{tabular}
\end{center}

L’indicateur de position n’est pas tout le temps utilisé mais est obligatoire. Cet indicateur permet de donner le numéro de l’effet à modifier dans le *core* étant donné que nous pouvons gérer plusieurs instances d’un même type d’effet. En outre, cet indicateur est utilisé également pour donner la taille de la donnée qui suit lorsque celle-ci est indéterminée comme c’est le cas avec l’ouverture d’un fichier — le *path* est variable.

### Décodage d’un ordre

##### Example : décodage du chemin d’un fichier :

L’octet définissant l’ordre à déjà été décodé et indique que la *GUI* a envoyé un ordre : `File “str”`. La première étape est de déterminer la longueur des données. Ensuite nous effectuons la lecture en fonction de la longueur et nous renvoyons le chemin envoyé en le tronquant du reste du *buffer* — qui contient souvant des données aléatoires.

~~~{.ocaml}
let arg_str () =
    let buff = String.create 256 in
    if Unix.read fd buff 0 1 > 0 then
      let length = int_of_char buff.[0] in
      if Unix.read fd buff 0 length = length then
        (length, String.sub buff 0 length)
      else
        failwith "Path is incomplete."
    else
      (0, "")
~~~

Ceci est le cas le plus complexe à gérer dans notre projet. Certes, il parait simple mais la moindre erreur avec la gestion des *pipes* est fatale et n’est pas évidente à débugger. Nous procédons plus ou moins de la même façon pour le décodage des autres ordres. L’encodage quant à lui est très simple et consiste la plupart du temps à transformer les données en caractère — car c’est un octet. L’utilisation des caractères nous à contrain à réduire la précisions en utilisant des pourcentages. Mais nous nous sommes aperçu qu’un pourcentage offrais déjà beaucoup de souplesse.