\chapter*{Introduction}

Nous tenons tout d’abord à remercier M. Christophe \textsc{Boullay} pour nous avoir confié un projet *libre* lors de ce second semestre de SPÉ. Nous remercions également l’équipe enseignante qui a dispensé les cours cette année et les personnes qui nous ont aidées pour la réalisation de ce logiciel. 

Ce projet à été l’occasion de mettre en oeuvre toutes nos compétences acquises à l’ÉPITA pendant ces deux années de prépa. Nous avons mis tout notre savoir faire en programmation, mathématiques, architecture et en électronique pour concevoir *StarDSP*.\newline

Notre projet *StarDSP* est un processeur de signal numérique — *DSP* — que l'on utilisera comme amplificateur de guitare électrique ou plus généralement de piste audio.\newline

Ce rapport de projet présente le chemin accompli pour la réalisation du projet. Il reprend les étapes effectués lors des deux soutenances précédentes et présente la version finale de ce projet.\newline

*StarDSP* est un projet algorithmiquement complexe et très complet. Il permet d'appliquer différents effets parmi les six proposés sur une piste audio préalablement enregistré ou en temps réel. Il propose trois modes de commande: par la console, par l'interface graphique ou bien par la surface de contrôle.\newline

Nous espérons vous intéresser tout au long de ce rapport et qui sait peut-être même vous étonner.