## Graphical User Interface

L'objectif du projet *StarDSP* est de reproduire les fonctionnalités d'un amplificateur de guitare. Pour faciliter l'utilisation de ce logiciel nous disposons d'une interface graphique. Notre interface est réalisée avec *LablGTK 2.0* et *OCaml*.\newline

Pour réaliser cet inteface nous avons utilisé de nombreuses méthodes.\newline

Notre première méthode était d'utiliser le système de *box* proposé par *LablGTK 2.0*. Ce système se met en place grâce à des *hbox* — horizontal box -- et des *vbox* — vertical box, ces *boîtes* ajoutent les widgets horizontalement ou verticalement. Elles peuvent s'imbriquer les unes dans les autres pour des interfaces plus complexes. Le résultat était simpliste et la méthode au contraire trop complexe.\newline

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.5]{../img/gui_exp.png}
\caption{Première interface graphique et son système complexe}
\end{figure}

À ce stade nous n'avions que trois fonctionnalités: un amplificateur agissant sur le gain, un égaliseur trois bandes et le contrôle du volume. Les curseurs permettait de définir des valeurs pour chaque fonctionnalités. Les *check box* quant à eux permettaient d'activer ou non les différents effets.\newline

Le menu *File* permettait depuis *Quit* de quitter le programme autrement qu'en cliquant sur la croix.\newpage

Avant d'arriver au résultat de notre deuxième soutenance, nous avions essayé un autre système : les *layout* ou placement absolu. Le placement absolu nous permettait de positionner chaque *widget* au pixel près et d'avoir un beau rendu facielement. Malheureusement ce système était non-redimensionnable. Nous avons donc ensuite utilisé les tableaux et plus précisément udes imbrications de tableaux.\newline

La barre de menu contenait trois menus: *File*, *Surface* et *About*.
Dans le menu *File* on pouvait trouver les options : *Open*, *Load presets* et *Exit*.
Dans le menu *Surface* l'option *TTY Configuration* et dans le menu *About* se trouvait l'option *About Us* qui affichait une fenêtre regroupant nos noms, le nom du projet, le nom du groupe, la version du logiciel et le site internet.\newline

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.40]{../img/gui_about.png}
\caption{Interface graphique avec la fenêtre \emph{About}}
\end{figure}

À ce moment l'interface n'était pas encore reliée au reste du projet mais elle était beaucoup plus complètes et complexe que l'ancienne.\newline

Sur la fenêtre principale on pouvait trouver une liste de *toggle buttons* qui permettaient d'activer les fonctionnalités suivantes : *Amplifier*, *Equalizer*, *Reverbe* et *Echo*. Les *toggle buttons* se grisent lorsqu'ils sont actifs, ils permettent à l'utilisateur de savoir quels effets sont actifs.
\newline

Le bouton *Setting* permettait d'ouvrir deux fenêtres :

- La fenêtre *Equalizer* avec les trois curseurs représentant les trois bandes de l'égaliseur.

- La fenêtre *Amplifier* avec le curseur *Gain* qui gère le gain et le curseur *Noise* qui permet de gérer le bruit.
\newline

Le bouton *Play* permettait d'activer/désactiver le système de temps réel. Il renvoyait le son perçu par le micro.

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.40]{../img/gui_settings.png}
\caption{Interface graphique avec les deux fenêtres de paramètres}
\end{figure}

\newpage

Enfin, pour cette dernière interface nous avons utilisé des tableaux, des *box* et une liste. En effet, l'utilisateur choisit désormais un effet en cliquant sur le bouton associé et cet effet viendra s'ajouter dans la liste à gauche. Le nombre d'effets lui est passé de quatre à six, l'effet *Balance* et *Stereo* se sont ajoutés au panel d'effets disponible. L'utilisateur peut ajouter autant d'effet qu'il le désire, pour lui faciliter la tâche les effets sont tous numérotés. Il peut les retirer de la liste un par un à l'aide du bouton *Remove* ou tous à la fois avec le bouton *Clear All*.\newline

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.40]{../img/gui.png}
\caption{Interface graphique finale}
\end{figure}

\newpage

Cliquer sur un effet permet d'ouvrir une fenêtre contenant les réglages associés. Nous avons garder les mêmes structures de fenêtres pour les effets Amplifier, Equalizer, Echo et Reverbe. Pour *Balance* contient seulement un curseur horizontal pour choisir le côté où le son sera le plus présent. L'effet *Stéréo* aussi ne compte qu'un seul curseur dans sa fenêtre de réglage.

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.40]{../img/gui3_settings.png}
\caption{Interface graphique finale avec fenêtres de réglages}
\end{figure}


Pour finir, le bouton *Start/Stop* permet de lancer ou couper la musique séléctionné. Le bouton *Realtime* permet d'activer le temps réel.