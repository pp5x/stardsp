\appendix

# Surface de contrôle

\begin{figure}[h]
\centering
\includegraphics[scale=0.50]{../img/surface.jpg}
\caption{La surface de contrôle}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{../img/pinout.png}
\caption{Atmega 8515}
\label{pin8515}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.50]{../img/STK500.jpg}
\caption{STK500}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/atmega1280.png}
\caption{Atmega 1280}
\label{pin1280}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{../img/dfrduino.png}
\caption{DFRduino Mega}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=200px]{../img/ft232.png}
\caption{FT232RL}
\label{pinft232}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{../img/icsp.jpg}
\caption{Programmation de l'Atmega 1280 par ICSP}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{../img/cksel.png}
\caption{Configuration de la source d'horloge grâce aux fusibles — \emph{fuses}}
\label{fuses}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics{../img/clock_osc.png}
\caption{Low power crystal oscillator}
\label{lpco}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics{../img/clock_ext.png}
\caption{External clock}
\label{clockext}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/crystal_1280.png}
\caption{Circuit d'horloge du DFRduino Mega}
\label{clockduino}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=\linewidth]{../img/osc_sel.png}
\caption{Sélection du signal d'horloge sur STK500}
\label{jumpers}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[width=\linewidth]{../img/osc_stk500.png}
\caption{Les différentes sources d'horloge du STK500}
\label{clocksrc}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.6]{../img/clock.jpg}
\caption{Cadencer l'Atmega 1280 avec un signal d'horloge externe}
\label{contact}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.50]{../img/uart.png}
\caption{Fonctionnement de l'UART}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.50]{../img/frame_format.png}
\caption{Format de trame}
\end{figure}

\clearpage

\begin{figure}[!h]
\centering
\includegraphics[scale=0.7]{../img/lcd_char.png}
\caption{Caractères proposés de base dans le HD44780}
\label{lcdchar}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.7]{../img/lcd_init.png}
\caption{Initialisation de l'afficheur en mode 4 bits}
\label{initlcd}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.45]{../img/can.png}
\caption{Fonctionnement du CAN}
\label{can}
\end{figure}

\clearpage

\begin{figure}[!h]
\centering
\includegraphics[scale=0.5]{../img/leds.jpg}
\caption{Visualisateur du signal sonore}
\label{leds}
\end{figure}
