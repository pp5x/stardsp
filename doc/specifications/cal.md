# Planning

## Première soutenance

Pour cette première soutenance, nous présenterons l’amorce de notre projet. L’adaptation des bibliothèques en `OCaml` sera terminée. L’interface utilisateur sera simple et permettra uniquement de présenter notre travail. Nos algorithmes de son seront basiques, cette tâche n’étant pas des plus simples. Une ébauche de site internet sera présentée.

## Seconde soutenance

Le développement de la surface de contrôle aura débuté, en collaboration avec l’interface utilisateur qui suivra de près les avancées de nos fonctionnalités. Plusieurs algorithmes de son seront présentés, avec quelques effets divers. Le site internet sera très avancé dans son développement.

## Soutenance finale

La surface de contrôle sera terminée et pleinement opérationnelle avec l’interface utilisateur. Cette interface comprendra tout le nécessaire pour utiliser notre logiciel. Le traitement du son sera pleinement fonctionnel avec les effets sus-cités dans les chapitres précédents. Le site internet sera complété avec la documentation, la distribution et des démonstrations.