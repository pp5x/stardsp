# Nature du projet

Ce chapitre décrit *StarDSP*, et comment celui-ci sera réalisé.

## Description
La principale ligne de développement de ce projet est de fournir un utilitaire pour les musiciens fonctionnant en temps réel sur ordinateur. L’idée de départ consiste à reproduire les effets d’un amplificateur de guitare électrique. L’utilisateur aura la possibilité d’effectuer divers réglages sur cet amplificateur virtuel, afin d’obtenir le son qu’il lui conviendra. *StarDSP* consiste à reproduire sur ordinateur l’équivalent du matériel professionnel pour guitariste à base de *DSP* — Digital Sound Processor. Bien que nous ne proposerons pas des dizaines d’amplificateurs de marques différentes, nous envisageons néanmoins de ne pas nous cantonner à simuler un amplificateur seul. Nous proposerons d’autres modules comme un égaliseur, délais, réverberation, etc\ldots

Pour que notre projet soit facilement utilisable, nous confectionnerons une interface utilisateur — *GUI* — qui permettra à l’utilisateur d’effectuer ses réglages sur les modules. Par ailleur, nous voulons confectionner périphérique *USB* pour permettre de contrôler l’interface utilisateur avec plus d’aisance qu’avec un ensemble clavier-souris.

Enfin, pour les besoins de ce projet, nous engagerons la création d’un site internet pour la communication, la documentation et la distribution des composants nécessaires à son fonctionnement.

## Principe

Un périphérique de capture de son est branché sur l’ordinateur — un micro, une guitare. Celui-ci est détecté par un serveur de son qui permet d’interfacer les entrées et sorties audio. Notre logiciel reçois un signal audio entrée. Nous effectuons la transformation de ce signal digitalisé selon la configuration définie par l’utilisateur. Cette transformation est effectuée via divers algorithmes de traitement de son et de mathématiques appliquées. Ensuite le signal sonore est renvoyé sur la sortie choisie par l’utilisateur.

## Organisation

Nous consacrerons l’utilisation d’un langage dit bas-niveau comme le `C` pour programmer les bases de notre projet. Nous réservons l’usage du `OCaml` pour le développement de nos algorithmes, de l’interface utilisateur et du site internet.

## Technologies envisagées

### Serveur de son

Un serveur de son permet de gérer l’usage et les accès des périphériques audio. Plus particulièrement la carte son. Celui ce présente sous la forme d’un processus qui “tourne” en tâche de fond. Nous souhaitons pour les besoins de *StarDSP* un serveur son adapté à nos attentes.

Il en existe beaucoup, nous nous sommes renseignés sur ceux-ci :

* `JACK` — *Audio Connection Kit*
* `PulseAudio`
* `aRts`
* `CoreAudio`
* `PortAudio`

*StarDSP* présente quelques contraintes. Nous devons effectuer le traitement du signal en temps réel. Et nous avons besoin d’une *API* relativement simple pour notre projet.

### Stockage du son

Afin d’éviter d’avoir constamment un périphérique d’acquisition sous la main, lors de notre développement, nous avons choisis d’enregistrer des “examples” de guitare électrique sans aucun effet ajouté. Divers formats ont été retenus pour la sauvegarde des données audio.

* `OGG`
* `WAV`
* `AIFF`
* `FLAC`
* `AAC`
* `MP3`

Afin de ne pas avoir à gérer de décompression, notre choix s’est porté sur un format dit *lossless*.

## Technologies retenues

En résumé, notre choix pour les technologies utilisées pour ce projet, sont les suivantes :

* `PulseAudio`
* `AIFF`
* `GTK`
* `Mara`

`PulseAudio` nous garanti une *API* assez simple et est adapté pour une utilisation en temps réel. Ensuite nous avons choisi le format `AIFF` parmi `WAV` et `FLAC`. Nous utiliserons une bibliothèque pour gérer ce format : `LibAIFF`. Pour l’interface `GTK` nous offre beaucoup de possibilités. De plus le *portage* vers `OCaml` existe déjà : `LablGTK`. Enfin, pour le développement du périphérique permettant le contrôle de l’interface utilisateur, nous utiliserons une technologie “fait maison”. `Mara` est un langage de programmation pour microcontrôleurs créé et développé par Pierre \textsc{Surply}.

