\chapter*{Conclusion}

Ce projet est assez ambitieux, en effet, les connaissances nécessaires à sa réalisation touchent des domaines variés. Nous allons donc devoir fournir un important travail de recherche ainsi qu'un investissement régulier. Ce projet est très motivant et nous espérons arriver à faire tout ce qui est prévu.


