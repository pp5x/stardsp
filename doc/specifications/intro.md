\chapter*{Introduction}

Dans le cadre du second semestre de SPÉ à \textsc{EPITA}, les élèves restants doivent réaliser un projet en `OCaml` et `C` dans un environnement *Linux*. Le projet \textsc{StarDSP} est présenté par le groupe *Turn It Loud*. Il dispose d'un outil fonctionnant en temps réel et s'adressant à tout musicien, débutant ou expert.

Ce projet mêlant deux langages très différents possède un grand intérêt algorithmique ainsi que mathématique. En effet, le traitement du signal n'est pas chose aisée, il demandera réflexion et application des cours, en particulier pour les séries de *Fourrier*.

Ce cahier des charges présente un descriptif détaillé de chaque partie constituant le projet, la répartition des tâches et les prévisions des soutenances.

