## Temps réel

## Traitement du son

Dans cette partie nous allons modifier le signal d'entrée afin d'obtenir ce que l'usager souhaite. Pour cela nous allons utiliser plusieurs théorèmes de mathématiques tel que les séries de Fourrier qui nous serons utiles.  

Il faut donc implémenter un certains nombres d'options pour obtenir le son désiré après le traitement. On aura donc une fonction pour amplifier le son, amplifier les basses, implémenter des effets\ldots