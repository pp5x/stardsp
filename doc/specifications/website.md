## Site internet

Le site internet est une partie très importante pour la vente de notre logiciel, afin d’acquérir une certaine notoriété. Pour cela il faut tout d'abord attirer l’attention des visiteurs, il faut donc un bel aspect graphique.

Ensuite, il faut qu'il soit facilement utilisable, accessible à toutes personnes — débutants, amateurs et experts.

Il faut que l'utilisateur puisse trouver toutes les informations dont il a besoin, il y aura donc de la documentation, des moyens pour nous joindre si l'utilisateur a besoin de plus d'informations comme une adresse *email*. Et enfin des vidéos de tutoriels pour apprendre à manipuler le logiciel.

Sur la page d'acceuil sera donc décrit le produit accompagné d'une courte vidéo de présentation pour voir l'aspect du logiciel et ses possibilités. Finalement il sera bien-sûr possible de télécharger le logiciel à partir du site web.
