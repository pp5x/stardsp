# Tâches

## Adaptation de la bibliothèque client *PulseAudio* pour le langage *OCaml*

La première étape de ce projet consiste à réaliser une interface entre la bibliothèque client *PulseAudio* et le langage *OCaml*.

En effet, cette dernière est uniquement utilisable en *C* : il sera donc nécessaire de rendre accessible en *OCaml* les fonctions mises à disposition pour communiquer avec le serveur de son.

Nous utiliserons cette bibliothèque pour récupérer le signal provenant de la carte son de la machine.

Cette étape doit être réalisée rapidement pour pouvoir entamer le coeur du projet : elle sera donc présentée dès la première soutenance.
