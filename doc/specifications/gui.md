## GUI

L'interface graphique est essentielle dans ce projet. Elle est bien plus agréable d'utilisation que les lignes de commandes. Cette interface devrait présenter une série de boutons correspondant à ceux d'un amplificateur de guitare. Le but étant de reproduire aussi le modèle graphique de ce dispositif.

Plus l'interface sera ressemblante, plus elle sera fonctionnelle et intuitive pour les utilisateurs. De plus, une interface graphique recherchée donnera une dimension plus professionnelle.

Dans un premier temps, une interface basique sera mise en place. Ensuite celle-ci sera étoffée jusqu'à obtenir le résultat escompté. Il faudra surement réaliser des croquis préliminaires pour faire quelque chose de vraiment original.

Les fonctionnalités seront incluses à cette interface à chaque fois que l'une d'entre elles sera disponible. Ainsi nous pourront optimiser ce dernier au fil de la progression.

Nous souhaitons vraiment que cette interface soit soignée pour donner envie à chaque personne désirant un logiciel de ce type d'utiliser *StarDSP*.

