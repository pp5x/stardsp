## Surface de contrôle

En plus d'être utilisable avec une interface graphique, le logiciel *StarDSP* pourra être contrôlé à l'aide d'une surface de contrôle séparé de l'ordinateur.

Cette surface de contrôle, que nous fabriquerons nous-même, permettra de contrôler le logiciel sans avoir besoin d'utiliser le clavier et la souris. Le but ici est de grandement simplifier l'ergonomie du *DSP*.

Elle devra donc proposer à l'utilisateur les principales fonctionnalités du programme à l'aide de potentiomètres, d'interrupteurs et de boutons poussoirs. Le tout sera disposé de manière ergonomique sur un boitier pupitre.

Le système utilisera un microcontrôleur *Atmel AVR* qui, une fois programmé, se chargera de transmettre au programme l'état des boutons par le biais de l'*USB*.

Le microcontrôleur sera programmé à l'aide d'un langage dédié : *Mara*[^1]

[^1]: https://bitbucket.org/Ptishell/mara

Bien entendu, la surface de contrôle n'agira pas directement sur le signal audio. Elle commandera seulement le traitement qui s'effectura sur l'*ordinateur*.
