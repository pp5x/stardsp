# Répartitions des tâches

## Alexandra

* Interface utilisateur — *GUI*
* Site internet

## Pierre P.

* Algorithmes de traitement du signal sonore

## Pierre S.

* Adaptation des bibliothèques pour `OCaml`
* Création de la surface de contrôle

## Vincent

* Algorithmes de traitement du signal sonore