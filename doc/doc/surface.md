# Audio control surface

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.5]{../img/surface.jpg}
\caption{Audio control surface}
\end{figure}

## Commands

- White potentiometer: Set amplifier effect
- \textcolor{yellow}{Yellow} potentiometer: Set stereo effect
- \textcolor{blue}{Blue} potentiometer : Set delay (echo effect)
- \textcolor{red}{Rouge} potentiometer : Set decay (echo effect)
- \textcolor{green}{Green} potentiometer : Set reverberation effect
- Pushbutton : Play/Pause
