# Dependencies

## StarDSP
Make sure dependencies are installed :

* OCaml 3.12
* LablGTK2
* PulseAudio
* LibSndFile
* Mara ($\geq 13$)

## Documentation

* \LaTeX
* Pandoc

# Compilation

## PulseAudio Simple API

First of all, you will need to install our *OCaml* binding for *PulseAudio Simple API*.

	$ cd path/to/stardsp/lib
	$ make
	# make install

## Installation of StarDSP

Then you can compile StarDSP — in the `src` folder — and run it :

	$ make
	$ ./stardsp
	
If you want to use it permanently, install it :

	# make install
# Usage

\begin{verbatim}
$ ./stardsp —help
Usage : ./stardsp [OPTIONS]
Input : Realtime or AIFF
	-core						Run core only
	-file <path>				AIFF file input
	-surface <device>			TTY
	-ssurface <data>			test Control-surface
	-eq <lg,mg,hg>				Use Equalizer
	-amp <drive,noise>			Use Amplifier
	-rev <modulation>			Use Reverberation
	-echo <delay,decay>			Use Echo
	-stereo <modulation>		Use Stereo effect
	-balance <left/right>		Stereo Balance
	-default					Surface preset
\end{verbatim}

 There are three different ways to use StarDSP :
 
 * The core only
 * The surface
 * The GUI

The command line interface takes into account the order of effect adding.