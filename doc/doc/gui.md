# GUI

Using the GUI is quite simple. See the image below to locate buttons and menus.\newline

\begin{figure}[!h!]
\centering
\includegraphics[scale=0.35]{../img/gui3_settings.png}
\end{figure}

To know more about us, the project and the website go to the *About* menu and click on *About us*.\newline

## Open a file

To open a file just click on the *File* menu and then on *Open*, a standard window will appear.\newline

## Adding \& setting effects

Once your file is open you can add one or several effects by clicking on the associated buttons. Their names will appear in the list on the right. Clicking on a button will also open an associated setting window. You can move the cursors to make your own settings.\newline


## Remove one or several effects

To remove one effect just click on its name in the list and the *Remove* button..\newline

*Warning: Remove an effect will destroy its setting window.*\newline

To remove all effects click on the *Clear All* button.\newline

*Warning: You can select only one effect at time. If you had clicked on the wrong effect just click again on the same effect, it will be deselected.*\newline


## Play or stop a music sample

You can play/stop a music by clicking on the *Start/Stop* button.\newline

## Realtime

You can activate the realtime mode by clicking on the *Realtime* button.\newline