# Encapsulation

## Module *core* Audio

### Problématique

Lors de la précédente version de *StarDSP*, l’accès direct aux ressources audio n’était pas protégé. De plus, il en résultait un code très verbeux et n’était pas facilement modulable. Par ailleurs, l’arrivée des *threads* nous a obligé à restructurer notre programme.

### Création d’un module dédié

La gestion des interactions avec `PulseAudio` est désormais *encapsulée* via la création d’un module. Cela instaure une autonomie dans le fonctionnement de notre logiciel et prévient l’apparition d’erreur inopinée.

Ce module permet donc tout simplement d’initialiser le moteur son, de libérer les ressources, et gère de lui même les mécanismes de gestion des effets. Nous en avons profité également pour ajouter des *modes de fonctionnement* :

* Mode lecture de fichier
* Mode temps réel
* Mode inactif

L’utilisateur peux désormais basculer à tout moment dans chacun de ces modes sans avoir à réinitialiser toute l’application. Le développement de l’application est beaucoup plus simple grâce à l’*encapsulation* c’est-à-dire que le principe est semblable à la programmation orientée objet : l’implémentation n’est pas directement accessible. Il suffit de lire l’interface `mli` pour comprendre le fonctionnement des divers modules sans avoir a lire leurs implémentations.

## Gestion des effets
### Intérêt

Au fur et à mesure que le projet se développe, nous avons de plus en plus d’effets à traiter. C’est pourquoi afin de prendre en charge l’interface utilisateur, nous avons ajouté un gestionnaire d’effets. L’utilisateur a désormais le choix d’ajouter, de supprimer, ou bien de déplacer l’ordre d’application des effets. Le son produit en sortie, peut-être différent en fonction de l’ordre d’application. 

Cela nous permet d’avoir un niveau d’abstraction élevé par rapport à la version précédente. La gestion des effets permet simplement de contrôler chacun d’eux sans avoir à se soucier des détails au niveau de l’implémentation qui peuvent encore évoluer. En outre, cela simplifie notre travail en équipe : on peut ajouter facilement un effet sans avoir à comprendre en détail le fonctionnement du coeur du programme.

### Conception & implémentation

#### Principe

L’idée est de pouvoir stocker facilement les différents effets dans un ordre bien particulier. Étant donné que le nombre d’effets applicables est variable, nous avons besoin d’une structure de donnée évolutive : les listes. Après mures réflexions, nous nous sommes aperçu que ce n’était pas les effets en eux même qu’il fallait ordonner mais les fonctions permettant de les appliquers. Nous partons donc du principe qu’il suffit de lire la liste d’effets dans l’ordre et d’appliquer un à un chaque effets.

#### Implémentation

Le principe évoqué ci-dessus est facilement applicable dans un langage fonctionnel comme le `OCaml`. Le *Caml* a une fonctionnalité particulièrement intéressante : les applications partielles. En effet, nous avons décidé afin de simplifier et de satisfaire le typage fort du *Caml* de normaliser le typage des fonctions de nos effets en fonctions du type `unit -> unit`.

Nous fournissons pour effectuer cela un module exclusivement réservé à la gestion des effets. Les listes en *Caml* sont récursives, nous avons donc implémenté des fonctions afin de pouvoir effectuer des ajouts/suppressions et déplacement *en place*.

~~~ {.ocaml}

val add : 'a -> int -> 'a list -> 'a list
(** Add an element at the index position in ocaml list.
    Index begin at position 1.
*)

val del : int -> 'a list -> 'a list
(** Delete an element at the index position in ocaml list.
    Index begin at position 1.
*)

val move : int -> int -> 'a list -> 'a list
(** Move an element at src position to dst position in ocaml list.
*)

~~~

#### Applications partielles

Le seul paramètre variable lors de l’application d’un effet est bien entendu la source sonore. Il est donc inutile de passer en paramètre la structure de donnée de l’effet. Nous utilisons les *applications partielles* pour réaliser cela. En effet dans le cas d’une fonction d’effet de base de type : `t -> float -> float` où `t` est la structure de donnée, l’application partielle de cette fonction avec le premier paramètre nous retourne une nouvelle fonction du type : `float -> float`.

D’une façon similaire, nous convertissons cette fonction en une fonction de type `unit -> unit` en automatisant la récupération et la modification des valeurs d’entrée et sortie indépendamment du *buffer* principal.