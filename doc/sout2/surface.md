# La surface de contrôle

Lors de la première soutenance, nous avons présenté la communication série par `UART` entre le logiciel *StarDSP* et le microcontrôleur `Atmega 8515`. À partir de cela, nous arrivions à afficher l'intensité du signal sonore traité par le logiciel grâce aux 8 `LEDS` du `STK500`.

## Changement de microcontrôleur

L'`Atmega 8515` a pour avantage d'être facilement programmé et testé avec le `STK500`. Cependant, ce microcontrôleur ne convient pas à une utilisation concrète. En effet, le nombre d'entrées/sorties est beaucoup trop limité pour notre surface. C'est pourquoi nous avons choisi de la contrôler grâce à un `Atmega 1280` interfacé sur une carte de prototypage rapide `DFRduino Mega`, basée sur l'`Arduino Mega 1280`.

### Différence entre l'`Atmega 8515` et l'`Atmega 1280`

La principale différence entre l'`Atmega 8515` et l'`Atmega 1280` est le nombre de pins. Le `8515` en possède 40 (figure \ref{pin8515}), tandis que le `1280` en a 100 (figure \ref{pin1280}). Cela implique que ce dernier existe uniquement en `CMS` — Composant Monté en Surface.

Au niveau de la mémoire, le `1280` est équipé d'une mémoire flash de programme de 128Ko et de 8Ko de RAM, tandis que le `8515` ne possède qu'une flash de 8Ko et d'une RAM de 512 octets. Il est donc possible de développer des programmes plus imposants sur l'`Atmega1280`.

Le `1280` est aussi fournit avec un nombre plus important de périphériques internes — 3 interfaces `UART`, 8 canaux de `PWM`, ... Dans notre cas, une interface `UART` et quelques convertisseurs analogiques nous suffisent.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{../img/pinout.png}
\caption{Atmega 8515}
\label{pin8515}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/atmega1280.png}
\caption{Atmega 1280}
\label{pin1280}
\end{figure}

### Avantage de l'interface `Arduino`

L'`Atmega 1280`, à cause d'une nombre important d'entrées/sorties, existe uniquement en `CMS`. C'est pourquoi nous utilisons une carte de prototypage rapide `DFRduino` pour contrôler notre surface.

Alimentée par `USB` avec une tension de 5V, elle permet d'accéder facilement à 54 pins bidirectionnelles du microcontrôleur et 16 entrées analogiques. Un crystal est aussi disponible pour cadencer le `1280` à 16MHz — contre 3.68MHz sur le `STK500`.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{../img/dfrduino.png}
\caption{DFRduino Mega}
\end{figure}

#### Communication par `USB` : le `FT232RL`

Le principal avantage de cette carte est de fournir une puce `FT232RL` (figure \ref{pinft232}) permettant de transmettre les informations envoyées par l'`UART` du microcontrôleur par `USB`.

Ainsi, les pins `PE0` (`TX0`) et `PE1` (`RX0`) du `1280` sont respectivement reliés aux pins `RXD` et `TXD` du `FT232RL`.

Grâce à ce système, nous pouvons utiliser la connexion `USB` comme s'il s'agissait d'une simple liaison série.

En ce qui concerne l'utilisateur, celui-ci préférera connecter la surface en `USB` plutôt qu'à l'aide d'un cable `RS-232` comme c'était le cas lors de la première soutenance.

\begin{figure}[h]
\centering
\includegraphics[width=200px]{../img/ft232.png}
\caption{FT232RL}
\label{pinft232}
\end{figure}

Contrairement à ce qui a été présenté lors de la première soutenance, la communication entre la surface et le logiciel est maintenant bidirectionnelle. En plus de l'affichage de l'intensité du signal sonore traité par *StarDSP*, la surface permet de contrôler le logiciel. Pour l'instant, il est possible d'activer ou non le traitement du son à l'aide d'un bouton poussoir.

### Programmation par ICSP

Pour pouvoir programmer le microcontrôleur par `USB`, les cartes *Arduino* sont fournis avec un *bootloader* qui, une fois installé sur le composant, lui permet d'écrire lui-même dans sa mémoire de programme. Cependant, ce dernier prend quelques secondes à s'executer avant de laisser la main au programme principal, ce qui allonge le temps de *reset* de la machine.

Pour résoudre ce problème, certe mineur, nous avons utilisé l'interface `ICSP` (In Circuit Serial Programming) du `STK500` pour injecter le programme dans le `1280`. Cela nous permet donc de nous passer de *bootloader*.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{../img/icsp.jpg}
\caption{Programmation de l'Atmega 1280 par ICSP}
\end{figure}

### Sélection de l'horloge externe

C'est en programmant le `1280` par `ICSP` que nous nous sommes aperçu que ce dernier était cadencé à 1MHz au lieu de 16MHz comme le permet le crystal se trouvant sur la `DFRduino` — peut-être une erreur de configuration lors de la fabrication ?

Il est possible de configurer la source d'horloge du microcontrôleur grâce aux fusibles — *fuses* —  de ce dernier. Il s'agit de registres particuliers de 8 bits non-volatiles qui permettent de configurer le comportement global du composant.

Le `1280` en possède 3 : *Low*, *High* et *Extended*. Les trois bits de poids faibles du fusible *Low* (`CKSEL`) permettent de configurer la source d'horloge comme cela est décrit sur la figure \ref{fuses}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.7]{../img/cksel.png}
\caption{Configuration de la source d'horloge grâce aux fusibles — \emph{fuses}}
\label{fuses}
\end{figure}

Le but ici était donc de changer les fusibles pour que le microcontrôleur accepte un crystal entre s'est bornes `XTAL1` et `XTAL2` comme sur le schema \ref{lpco} et ainsi respecter le montage réel du `DFRduino` \ref{clockduino}.

Cependant, nous avons, par mégarde, placé les bits `CKSEL` à 0000, ce qui correspond à une source externe d'horloge. Ce n'est évidemment pas ce que nous recherchions.

Sachant que le microcontrôleur doit être cadencé pour pouvoir être programmé par `ICSP`, un problème s'est alors posé. 

#### Comment rétablir les bonnes valeurs des fusibles si ce dernier n'est plus cadencé ?

À ce moment, le microcontrôleur ne pouvait être cadencé uniquement grâce à un montage similaire à celui de la figure \ref{clockext}. Il fallait imposer un signal d'horloge sur le port `XTAL1` du `1280` et relier le `XTAL2` à la masse.

Le composant en question étant soudé en surface, il était impossible d'établir un contact entre un fil et ces ports : les pattes, étant côte à côte, sont bien trop petites.

Comme le montre la figure \ref{contact}, les soudures du crystal sont suffisamment grandes pour pouvoir les mettre en contact. Par chance, une d'entre elles est directement reliée au port `XTAL1` !

Maintenant que nous savions où nous pouvions injecter le signal d'horloge, il fallait encore réussir à trouver un moyen de générer ce fameux signal.

\begin{figure}[h]
\centering
\includegraphics{../img/clock_osc.png}
\caption{Low power crystal oscillator}
\label{lpco}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics{../img/clock_ext.png}
\caption{External clock}
\label{clockext}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/crystal_1280.png}
\caption{Circuit d'horloge du DFRduino Mega}
\label{clockduino}
\end{figure}

#### Comment obtenir un signal d'horloge permettant de cadencer le microcontrôleur ?

Ayant peu de matériel, la seule source de signal d'horloge dont nous disposions était celui du `STK500`.

Comme le montre la figure \ref{jumpers}, le `STK500` permet, à l'aide de jumpers, de choisir la source du signal d'horloge qui sera appliquée aux microcontrôleurs se trouvant sur les sockets de la carte. Ainsi, il est possible soit d'utiliser son propre crystal soit d'utiliser un signal émis par le microcontrôleur *Maitre* du `STK500` (figure \ref{clocksrc})

N'ayant pas de crystal, nous nous sommes servis du signal généré de 3.68MHz.

En mettant en contact la broche 1 du jumper `OSCSEL` du `STK500` avec la soudure du `DFRduino`, l'`Atmega 1280` était maintenant cadencé. Il suffisait ainsi de changer la valeur de fusibles grâce à l'`ICSP` tout en maintenant le contact entre les deux cartes.

Avec *avrdude*, il est facile de modifier les fusibles par `ICSP` grâce à cette commande :

    $ avrdude -p m1280 -c stk500 -P /dev/ttyUSB0 -e -U lfuse:w:0xff:m -U hfuse:w:0xda:m

En configurant correctement les fusibles pour que le microcontrôleur utilise le quartz externe, l’`Atmega1280` fonctionnait de nouveau !

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/osc_sel.png}
\caption{Sélection du signal d'horloge sur STK500}
\label{jumpers}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/osc_stk500.png}
\caption{Les différentes sources d'horloge du STK500}
\label{clocksrc}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{../img/clock.jpg}
\caption{Cadencer l'Atmega 1280 avec un signal d'horloge externe}
\label{contact}
\end{figure}

## Construction de la surface

Cette deuxième soutenance a aussi été l'occasion de commencer la construction de la surface physique.

Nous avons réalisé une carte de support pour les *LED* du visualisateur d'intensité sonore (figure \ref{leds}). Ce support physique a été réalisé à la main sur une plaque d'essai de dimension 50x100mm.

Pour se connecter à la `DFRduino`, 9 broches sont disponibles. Les 8 premières permettent de contrôler les 8 *LED* tandis que la neuvième doit être reliée à la masse.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{../img/leds.jpg}
\caption{Visualisateur du signal sonore}
\label{leds}
\end{figure}
