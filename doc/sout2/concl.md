\chapter*{Conclusion}

Le projet a pris un bon départ, une énorme quantité de travail a déjà été accomplie. Un certain nombre de fonctionnalités sont pleinements opérationnelles. Mais il nous reste encore beaucoup de détails et de développement à effectuer pour obtenir le rendu final souhaité. Notre travail pour la suite consistera a continuer à ajouter des effets pour le traitement du son, finaliser le développement de la surface de contrôle afin que celle-ci puisse substituer l’interface graphique sans pour autant la négliger. Notre site internet est déjà à son stade terminal : quelques ajouts mineurs seront à ajouter.

Une bonne vitesse de croisière propulse notre projet vers le rendu final, nous espérons que celui-ci sera à la hauteur de nos espérances.