# Site internet

## Évolutions
Nous arrivons à mi-parcours. Le site présenté précedemment était viable pour présenter un utilitaire. Mais nous avons préféré le refaire complètement afin d’y ajouter un aspect plus *multimédia* et plus *didactique*. Notre site internet se voit donc offrir un tout nouveau look !

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{../img/website.png}
\caption{\emph{Design} du nouveau site internet}
\end{figure}

Le site internet a été restructuré de la façon suivante pour plus lisibilité.

* Une page d’accueil allant à l’essentiel.
* Une section de *présentation détaillée*.
* Une section *documentation*.
* Une section *téléchargement*.
* Une section *à propos*.

La nouvelle page d’accueil permet de montrer en quelques coups d’oeil les différentes fonctionnalités qu’offre *StarDSP*. La section *téléchargement* permet de distribuer rapidement notre logiciel et nos rapports de soutenance. La section *documentation* permet quant à elle d’explique en détails le fonctionnement de *StarDSP* et préciser les solutions aux eventuels problèmes. La présentation *détaillée* présente globalement le logiciel mais avec beaucoup plus d’explication et de détails par rapport à la page d’accueil qui se limite à la présentation des points forts. La section *à propos* permet quant à elle de présenter le travail effectué par chaque membres de l’équipe, ainsi que des liens vers des projets personnels. On notera également que le site est désormais rédigé exclusivement en anglais pour pouvoir attirer un public plus large.

## Hébergement

Le projet commence à prendre de l’ampleur. C’est pourquoi nous hébergeons le site sur un serveur. Notre site étant réalisé grâce au moteur de *template* de Pierre \textsc{Surply}, il est nécessaire de le compiler pour pouvoir l’afficher. Le serveur distant n’ayant pas forcément la même configuration que nos ordinateurs personnels, nous devons effectuer la compilation sur le serveur distant.

Pour la dernière soutenance le site internet sera éventuellement disponible sur internet, cela dépendra de la capacité de notre hébergeur à exécuter un site internet en *CGI*.