# Interface graphique — *GUI*

Pour cette deuxième soutenance, nous avons essayé d'autres méthodes pour simplifier cette interface. Après les *box*, nous avons essayé un système de tableaux simple, mais il était difficile d'avoir une interface avec un joli design. 
\newline

Nous avons donc essayé par la suite avec un système de *layout* et de placement absolu. Ce principe permettait d'avoir des *widgets* placés au pixel près ce qui nous donnait un rendu assez beau. Cependant, ce système était non redimensionnable et donc nous perdions un avantage. Nous sommes donc revenu à un système de tableaux mais imbriqués cette fois-ci. Les *widgets* ne sont plus rajoutés case par case mais dans certains cas, ils sont rajoutés dans un tableau placé dans une des cases du tableau principal. Par exemple, les trois barres de l'égaliseur sont dans un *sous-tableau*.
\newline

La barre de menu a changé, nous avons maintenant trois menus :`File`, `Surface`, `About`.
\newline

Dans le menu `File` on trouve : `Open`, `Exit` et `Load presets`. 

- `Open` ouvre la piste audio

- `Load presets` n'est pas encore implémentée mais permettra de charger des *préferences utilisateurs* ou des profils pré-faits pour les différents effets.

- `Exit` permet de quitter autrement qu'en cliquant sur la croix.
\newline

Dans le menu `Surface` on trouve l'option `TTY Configuration` qui servira à la configuration de la *surface*.
\newline

Enfin dans le menu `About` on trouve l'option `About us` qui ouvre une fenêtre où sont indiqués nos noms — bouton `Credits`, le nom du projet, sa version et l'adresse de notre site internet.
\newline


\begin{figure}[!h!]
\centering
\includegraphics[scale=0.40]{../img/gui_about.png}
\caption{Interface graphique avec la fenêtre \emph{About}}
\end{figure}


Sur la fenêtre principale on trouve une liste de *toggle buttons* qui permettent d'activer les fonctionnalités suivantes : *Amplifier*, *Equalizer*, *Reverbe* et *Echo*. Les *toggle buttons* se grisent lorsqu'ils sont actifs, ils permettent à l'utilisateur de savoir quels effets sont actifs.
\newline


\begin{figure}[htbp]
\centering
\includegraphics[scale=0.40]{../img/gui_settings.png}
\caption{Interface graphique avec les deux fenêtres de paramètres}
\end{figure}

Le bouton `Settings` permet d'ouvrir deux fenêtres :

- La fenêtre `Equalizer` avec les trois curseurs représentant les trois bandes de l'égaliseur.

- La fenêtre `Amplifier` avec le curseur `Gain` qui gère le gain et le curseur `Noise` qui permet de gérer le bruit.
\newline

Le bouton `Play` permet d'activer/désactiver le système de temps réel. Il renvoi le son perçu par le micro.
