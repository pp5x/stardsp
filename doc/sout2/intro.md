\chapter*{Introduction}

Précédemment, nous avions présenté les prémices de notre projet. Nous avions préparés les bases nécessaires à son développement comme le moteur *son*, la lecture des fichiers à des fins de test, un prototype de notre *surface*, ainsi qu’une interface graphique et un site internet très limités.

Nous rencontrions également quelques problèmes d’organisation et de travail. Cette seconde soutenance est l’occasion pour nous de développer plus en détails nos idées pour le rendu final.

Le travail effectué pour cette soutenance a été primordial car nous sommes passés d’un développement expérimental à un développement plus ordonné et concret. Le projet a été réorganisé, simplifié et nous n’avons pas manqué de rajouter des fonctionnalités. Les problèmes ont tous été résolus. Nous avons amélioré la lecture des fichiers de test, simplifié notre conception du traitement du signal, ajouté des nouveaux effets, revu en détails l’interface graphique et le site internet, miniaturisé la surface de contrôle. Notre objectif pour cette soutenance était de préparer le terrain pour la dernière ligne droite.

#### Répartition des tâches

* Pierre \textsc{Surply}
	* Surface de contrôle
	* Communication par *threads*
* Pierre \textsc{Pagnoux}
	* Lecture des fichiers de test
	* Gestion des effets & structure du programme
	* Amplification
	* Echo
	* Site internet
* Alexandra \textsc{Pizzini}
	* GUI
* Vincent \textsc{Molinié}
	* Réverbération