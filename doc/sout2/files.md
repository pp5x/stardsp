# Fichiers audio

## Problème
Lors de la précédente soutenance, nous avions présenté la possibilité de lire des fichiers `AIFF` afin de pouvoir effectuer des tests sur notre logiciel. Le problème de saturation que nous avions rencontré, qui a été source de conflits et de perte de temps est maintenant résolu.

## Changement de bibliothèque
Le problème était une amplification involontaire sur nos fichiers `AIFF`. Le son était complétement saturé à cause d’une perte d’information et des valeurs mal converties par la précédente bibliothèque. Pour palier ce problème, nous avons radicalement changé de bibliothèque pour lire les fichiers. Nous utilisons maintenant `libsndfile`. Cette nouvelle bibliothèque permet en outre de lire beaucoup plus de format de fichier. *StarDSP* peut maintenant être utilisé avec ces quelques formats de fichier :

#### Liste des formats de fichier pris en charge par *StarDSP*
* Microsoft `WAV`
* Apple `AIFF/AIFC`
* Format `RAW`
* Paris Audio File `PAF`
* Apple `CAF`
* Free Lossless Audio Codec `FLAC`
* Soundforge `W64`

Cela offre beaucoup plus de souplesse ! Cette nouvelle bibliothèque a été également l’occasion d’alléger notre code. Celle-ci s’occupe des différentes conversions à effectuer.