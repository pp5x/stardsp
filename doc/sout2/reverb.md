## Réverbération

### Explication

La réverbération est la persistance d'un son dans un lieu lorsque la source n'existe plus. C'est une addition de plusieurs échos successifs, avec des retards différents. Cet effet essaye de simuler l'ambiance d'une salle dans laquelle le signal sonore se repercute sur plusieurs obstacles. Par exemple dans une cathédrale, lorsqu'un son fort est produit il y aura une réverbération naturelle.
C'est l'un des effets musical les plus prisés, car il est très agréable à l'oreille et il permet de faire durer un son -- une note.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{../img/reverb_exp.jpg}
\caption{Schéma explicatif d’une réverbération}
\end{figure}

Le signal ressenti est semblable à ceci :

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/reverb_graph.jpg}
\caption{Signal audio lors d’une réverbération}
\end{figure}

### Implémentation

La réverbération est un effet très complexe. Il faut tout d'abord simuler des “filtres en peigne” — ou *Comb Filter* — et des “filtres déphaseurs” — ou *All-Pass Filter*.
Pour chacun de ces filtres, il faudra un *buffer* qui contiendra le signal d'entrée retardé et d'un gain d'amplification $g$. Ce gain est donné par la formule suivante:

\begin{figure}[htpb]
\centering
		  $g = 0.001 ^ {(D / RVT)} $
\end{figure}

Où $D$ est le *delay time* — ou *loop time* — qui correspont au retard du filtre et $RVT$ est le temps de réverbération désiré.


#### Comb Filter

Le *Comb Filter* est utilisé pour ajouter à un signal une version retardée de celui-ci.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/Comb_filter.png}
\caption{Comb Filter}
\end{figure}

 Il a pour équation:

\begin{figure}[htpb]
\centering
   	       	    $y(n) = x(n) + g * y(n - d)$
\end{figure}

où $n$ correspond au *sample* actuel, $x$ signal d'entrée, $y$ signal de sortie, $g$ le gain du filtre et $d$ le *delay* ou retard.

Son implémentation en C donne ceci:

\begin{verbatim}
  	float output = delayBuffer[n - d];
  	delayBuffer[n] = input + output * g;
 	return output;
\end{verbatim}

\newpage

#### All-Pass Filter

Le *All-Pass Filter* ne modifie pas le signal, mais la *phase* du signal quant à elle est décalée en sortie du filtre.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{../img/allpass.png}
\caption{All-Pass Filter}
\end{figure}

Il a pour équation :

\begin{figure}[htpb]
\centering
		 $y(n) = -g * x(n) + x(n - d) + g  *  y(n - d)$
\end{figure}

où $n$ correspond au *sample* actuel, $x$ signal d'entrée, $y$ signal de sortie, $g$ le gain du filtre et $d$ le *delay* ou retard.

Son implémentation en C donne ceci:
\begin{verbatim}
  float output = delayBuffer[n - d];
  delayBuffer[n] = input + output * g;
  output = output - input * g;
  return output;
\end{verbatim}

\newpage

#### Réverbération

Une fois que l'on a réussi à simuler ces filtres, il existe différentes méthodes pour simuler la réverbération en assemblant ces filtres en *série* ou *parallèle*.

J'ai opté pour une méthode similaire a celle présentée sur le schéma qui suit pour la réalisation de la réverbération — 6 *Comb Filters* en parallèles suivi de 4 *All-Pass Filters* en séries.

Chaque filtre a son propre *buffer de délais* et son propre gain $g$. Pour les filtres en parallèles, il suffit de diviser le signal par le nombre de filtre en parallèle et d'additionner les sorties de ces derniers. Pour les filtres en séries, il suffit simplement de passer en entrée du filtre la sortie du filtre précédent.

Cette méthode est basée sur les algorithmes de \textsc{Schroeder} et \textsc{Moorer}.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{../img/reverb.png}
\caption{Réverbération}
\end{figure}

