# StarDSP

Digital Sound Processor project for EPITA. Written in `C`, `OCaml` and `Mara`.

## Dependencies

### Core language & Libraries

* [OCaml 3.12](http://caml.inria.fr)
* [LablGTK2](http://lablgtk.forge.ocamlcore.org)
* [PulseAudio](http://www.freedesktop.org/wiki/Software/PulseAudio)
* [LibSndFile](http://www.mega-nerd.com/libsndfile/)
* [Mara >= 13](http://mara.psurply.com/)

### Documentation

* LaTeX
* [Pandoc](http://johnmacfarlane.net/pandoc/)

## Installation

First of all, you will need to install our *OCaml* binding for *PulseAudio Simple API*.

    $ cd path/to/stardsp/lib
    $ make
    # make install

## Contact

* Pierre Pagnoux — `pagnou_p@epita.fr`
* Vincent Molinié — `molini_v@epita.fr`
* Alexandra Pizzini — `pizzin_a@epita.fr`
* Pierre Surply — `surply_p@epita.fr`
