##
## Makefile for StarDSP
## 
## Made by Pierre Surply
## <surply_p@epita.fr>
## 
## Started on  Fri Jan  4 21:23:51 2013 Pierre Surply
## Last update Sat Apr  6 08:55:51 2013 Pierre Pagnoux
##

SUBDIRS = src lib doc website

all: $(SUBDIRS)

clean: COMMAND = clean
clean: $(SUBDIRS)
	@rm -rf *~ *#

$(SUBDIRS): FORCE
	@$(MAKE) -C $@ $(COMMAND)

FORCE:


