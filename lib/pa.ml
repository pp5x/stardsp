(*
** pa.ml for StarDSP
** 
** Made by Pierre Surply
** <pierre.surply@gmail.com>
** 
** Started on  Sun Jan 27 14:44:04 2013 Pierre Surply
** Last update Sat Feb  9 18:28:20 2013 Pierre Surply
*)

type t

type sample =
  {
    rate      : int;
    channels  : int
  }

type dir =
| Nodirection
| Playback
| Record
| Upload

type buffer_attr =
  {
    max_length          : int;
    target_length       : int;
    prebuffering        : int;
    min_request         : int;
    fragment_size       : int;
  }

type map

external create :
  string option         ->
  string                ->
  dir                   ->
  string option         ->
  string                ->
  sample                ->
  map option            ->
  buffer_attr option    ->
  t = "ocaml_pa_create_bytecode" "ocaml_pa_create_native"

let create
    ?server
    ~name
    ~direction
    ?dev
    ~stream_name
    ~ss
    ?map
    ?attr () =
  create
    server
    name
    direction
    dev
    stream_name
    ss
    map
    attr

external free : t -> unit = "ocaml_pa_simple_free"

external write :
  t                     ->
  (float, Bigarray.float32_elt, Bigarray.c_layout)
    Bigarray.Array1.t   -> 
  int                   ->
  unit = "ocaml_pa_simple_write"

external read :
  t                     ->
  (float, Bigarray.float32_elt, Bigarray.c_layout)
    Bigarray.Array1.t   -> 
  int                   ->
  unit = "ocaml_pa_simple_read"

external drain : t -> unit = "ocaml_pa_simple_drain"

external flush : t -> unit = "ocaml_pa_simple_flush"

external get_latency : t -> int = "ocaml_pa_simple_get_latency"
