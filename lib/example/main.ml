(*
** main.ml for StarDSP
** 
** Made by Pierre Surply
** <pierre.surply@gmail.com>
** 
** Started on  Sat Feb  9 15:37:15 2013 Pierre Surply
** Last update Sun Feb 10 18:27:03 2013 Pierre Surply
*)

open Bigarray

let main () =
  let ss =
    {
      Pa.rate = 44100;
      Pa.channels = 2;
    }
  in
  let size = 512 in
  let buffer = Array1.create
    Bigarray.float32
    Bigarray.c_layout
    size
  in
  let simple_rec =
    Pa.create
      ~name:"Echo"
      ~direction:Pa.Record
      ~stream_name:"read"
      ~ss:ss ()
  in
  let simple_play =
    Pa.create
      ~name:"Echo"
      ~direction:Pa.Playback
      ~stream_name:"play"
      ~ss:ss ()
  in
  while true do
    Pa.read simple_rec buffer size;
    Pa.write simple_play buffer size
  done;
  Pa.free simple_rec;
  Pa.free simple_play;
  exit 0

let _ = main ()
