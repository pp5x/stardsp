/*
** pa_stubs.c for StarDSP
** 
** Made by Pierre Surply
** <pierre.surply@gmail.com>
** 
** Started on  Sun Jan 27 14:43:50 2013 Pierre Surply
** Last update Sat Mar 23 13:57:36 2013 Pierre Pagnoux
*/

#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/misc.h>
#include <caml/mlvalues.h>
#include <caml/signals.h>
#include <caml/bigarray.h>

#include <pulse/pulseaudio.h>
#include <pulse/simple.h>

static char *string_opt_val(value v)
{
  if (Is_long(v))
    return NULL;
  else
    return String_val(Field(v, 0));
}

static pa_sample_spec* sample_spec_val(value spec)
{
  pa_sample_spec *ans = malloc(sizeof(pa_sample_spec));

  ans->format = PA_SAMPLE_FLOAT32NE;
  ans->rate = Int_val(Field(spec, 0));
  ans->channels = Int_val(Field(spec, 1));

  return ans;
}

static pa_stream_direction_t dir_val(value dir)
{
  switch (Int_val(dir))
    {
    case 0:
      return PA_STREAM_NODIRECTION;
    case 1:
      return PA_STREAM_PLAYBACK;
    case 2:
      return PA_STREAM_RECORD;
    case 3:
      return PA_STREAM_UPLOAD;
    default:
      assert(0);
    }
}

static inline
void check_err(int ret, int err)
{
  if (ret < 0)
    caml_raise_with_arg(*caml_named_value("pa_exn_error"),
                        Val_int(err));
}

CAMLprim
value ocaml_pa_create_native(value server,
                             value name,
                             value dir,
                             value dev,
                             value stream_name,
                             value sample,
                             value map,
                             value attr)
{
  CAMLparam5(server, name, dir, dev, stream_name);
  CAMLxparam3(sample, map, attr);

  pa_simple             *simple;
  pa_sample_spec        *ss;
  int                   err;
  pa_buffer_attr        *ba;
  value                 ans;

  ba = NULL;

  if (Is_block(attr))
    {
      ba = malloc(sizeof(pa_buffer_attr));
      attr = Field(attr, 0);
      ba->maxlength = Int_val(Field(attr, 0));
      ba->tlength = Int_val(Field(attr, 1));
      ba->prebuf = Int_val(Field(attr, 2));
      ba->minreq = Int_val(Field(attr, 3));
      ba->fragsize = Int_val(Field(attr, 4));
    }
  
  ss = sample_spec_val(sample);
  simple = pa_simple_new(string_opt_val(server),
                         String_val(name),
                         dir_val(dir),
                         string_opt_val(dev),
                         String_val(stream_name),
                         ss,
                         NULL,
                         ba,
                         &err);
  
  if(ba)
    free(ba);

  if(!simple)
    {
      caml_raise_with_arg(*caml_named_value("pa_exn_error"),
                          Val_int(err));
    }

  ans = caml_alloc_tuple(1);
  Store_field(ans, 0, (value) simple);
  
  CAMLreturn(ans);
}

CAMLprim
value ocaml_pa_create_bytecode(value *argv, int argc)
{
  return ocaml_pa_create_native(argv[0],
                                argv[1],
                                argv[2],
                                argv[3],
                                argv[4],
                                argv[5],
                                argv[6],
                                argv[7]);
}

CAMLprim
value ocaml_pa_simple_free(value simple)
{
  pa_simple_free((pa_simple *) Field(simple, 0));
  Store_field(simple, 0, (value) NULL);

  return Val_unit;
}

CAMLprim
value ocaml_pa_simple_write(value _simple,
                            value _buf,
                            value _len)
{
  CAMLparam2(_simple, _buf);
  pa_simple     *simple;
  int           len = Int_val(_len);
  int           err;
  int           ret;

  simple = (pa_simple *) Field(_simple, 0);

  caml_enter_blocking_section();
  ret = pa_simple_write(simple,
                        Data_bigarray_val(_buf),
                        len * sizeof(float),
                        &err);
  caml_leave_blocking_section();
  check_err(ret, err);

  CAMLreturn(Val_unit);
}

CAMLprim
value ocaml_pa_simple_drain(value _simple)
{
  CAMLparam1(_simple);
  pa_simple     *simple;
  int           ret;
  int           err;

  simple = (pa_simple *) Field(_simple, 0);

  caml_enter_blocking_section();
  ret = pa_simple_drain(simple, &err);
  caml_leave_blocking_section();
  check_err(ret, err);

  CAMLreturn(Val_unit);
}

CAMLprim
value ocaml_pa_simple_flush(value _simple)
{
  CAMLparam1(_simple);
  pa_simple     *simple;
  int           err;

  simple = (pa_simple *) Field(_simple, 0);

  caml_enter_blocking_section();
  pa_simple_flush(simple, &err);
  caml_leave_blocking_section();

  CAMLreturn(Val_unit);
}

CAMLprim
value ocaml_pa_simple_get_latency(value _simple)
{
  CAMLparam1(_simple);
  pa_simple     *simple;
  int           ret;
  int           err;

  simple = (pa_simple *) Field(_simple, 0);

  caml_enter_blocking_section();
  ret = pa_simple_get_latency(simple, &err);
  caml_leave_blocking_section();
  check_err(ret, err);

  CAMLreturn(Int_val(ret));
}

CAMLprim
value ocaml_pa_simple_read(value _simple,
                           value _buf,
                           value _len)
{
  CAMLparam2(_simple, _buf);
  pa_simple     *simple;
  int           len = Int_val(_len);
  int           err;
  int           ret;

  simple = (pa_simple *) Field(_simple, 0);

  caml_enter_blocking_section();
  ret = 0;
  ret = pa_simple_read(simple,
                       Data_bigarray_val(_buf),
                       len * sizeof(float),
                       &err);
  caml_leave_blocking_section();

  check_err(ret, err);

  CAMLreturn(Val_unit);
}
