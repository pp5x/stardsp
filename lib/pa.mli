(*
 ** pa.mli for StarDSP
 **
 ** Made by Pierre Surply
 ** <pierre.surply@gmail.com>
 **
 ** Started on  Sun Jan 27 14:44:13 2013 Pierre Surply
** Last update Sat Apr  6 18:46:10 2013 Pierre Pagnoux
 *)

type t

type sample =
    {
      rate      : int;
      channels  : int
    }

type dir =
  | Nodirection
  | Playback
  | Record
  | Upload

type buffer_attr =
    {
      max_length          : int;
      target_length       : int;
      prebuffering        : int;
      min_request         : int;
      fragment_size       : int;
    }

type map

val create :
  ?server       : string        ->
  name          : string        ->
  direction     : dir           ->
  ?dev          : string        ->
  stream_name   : string        ->
  ss            : sample        ->
  ?map          : map           ->
  ?attr         : buffer_attr   ->
  unit                          ->
  t

val free : t -> unit

val write :
  t                     ->
  (float, Bigarray.float32_elt, Bigarray.c_layout)
    Bigarray.Array1.t   ->
  int                   ->
  unit

val read :
  t                     ->
  (float, Bigarray.float32_elt, Bigarray.c_layout)
    Bigarray.Array1.t   ->
  int                   ->
  unit

val drain : t -> unit

val flush : t -> unit

val get_latency : t -> int
